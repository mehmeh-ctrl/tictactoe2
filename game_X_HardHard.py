# game_X_HardHard.py

from typing import Tuple, Dict

from tictactoe import TicTacToeRandom


class GameXHard:
    """
    The main game logic. With O playing first.
    The 3x3 classic automatic gameplay for the building and testing purposes.
    """

    X_wins: int = 0
    O_wins: int = 0
    draws: int = 0

    # The phrase variable set to empty string.

    phrase: str = ''

    def play(self) -> None:
        """
        The "empty" (the 9-element-long string composed of "_" elements)
        string is instantiated as the new_X_cells. The "empty" (the dictionary
        with the keys of coordinates and values of "_") dictionary is instantiated
        as the new_X_dict - to set it apart from the "O" that will be placed first
        and plug it into the loop. The loop uses the counter variable to direct
        the player activity to the relevant blocks of the game's method. There, as
        that version of the game was created for the automatic building purposes,
        the player activity (typing in the coordinates) is simulated by the
        pseudorandom drawing of the coordinates from the list. The even counter
        values direct the activity towards typing in the new "O" (the method
        O_field_without_checking(...)), the uneven counter values - towards typing
        in the new "X" (the method X_field_without_checking(...)). The values of
        the counter greater or equal to 4 are directed towards the block with
        the checking of the game results.

        :param self: An instance of the class.
        :type self: An instance of the class.
        :return: None. Example: None.
        :rtype: None

        """

        tic_tac_toe = TicTacToeRandom()

        # The helper coordinates printed:

        print("Coordinates of the cells:")
        print(tic_tac_toe.cell_coordinates())

        # The empty field printed:

        print(tic_tac_toe.empty_field()[0])

        # The counter variable set to 0:

        counter: int = 0

        new_X_dict: Dict[Tuple[int, int], str] = dict()
        new_X_cells: str = ""

        new_O_cells: str = tic_tac_toe.empty_field()[1]
        new_O_dict: Dict[Tuple[int, int], str] = tic_tac_toe.print_matrix_cells(new_O_cells)[1]

        while counter < 8:

            while counter < 4:

                if counter % 2 == 0:

                    new_O_field: str

                    new_X_field, new_X_dict, new_X_cells = \
                        TicTacToeRandom.X_field_without_checking_hard_level(
                            tic_tac_toe,
                            new_O_dict, new_O_cells)

                    print(new_X_field)

                    counter += 1

                elif counter % 2 == 1:

                    new_X_field: str

                    new_O_field, new_O_dict, new_O_cells = \
                        TicTacToeRandom.O_field_without_checking_hard_level(
                            tic_tac_toe,
                            new_X_dict, new_X_cells)

                    print(new_O_field)

                    counter += 1

            while 4 <= counter < 9:

                if counter % 2 == 0:

                    new_X_field, new_X_dict, new_X_cells = \
                        TicTacToeRandom.X_field_without_checking_hard_level(
                            tic_tac_toe,
                            new_O_dict, new_O_cells)

                    print(new_X_field)

                    phrase = TicTacToeRandom.checking_the_game_result(
                        tic_tac_toe, new_X_cells)

                    if phrase == "X wins":

                        print("X wins")
                        self.X_wins += 1
                        print("Game over.")
                        return

                    elif phrase == "O wins":

                        print("O wins")
                        self.O_wins += 1
                        print("Game over.")
                        return

                    elif phrase == "Draw":

                        print("Draw")
                        self.draws += 1
                        print("Game over.")
                        return

                    elif phrase == "Game not finished":

                        pass

                        counter += 1

                elif counter % 2 == 1:

                    new_O_field, new_O_dict, new_O_cells = \
                        TicTacToeRandom.O_field_without_checking_hard_level(
                            tic_tac_toe,
                            new_X_dict, new_X_cells)

                    print(new_O_field)

                    phrase = TicTacToeRandom.checking_the_game_result(
                        tic_tac_toe, new_O_cells)

                    if phrase == "X wins":

                        print("X wins")
                        self.X_wins += 1
                        print("Game over.")
                        return

                    elif phrase == "O wins":

                        print("O wins")
                        self.O_wins += 1
                        print("Game over.")
                        return

                    elif phrase == "Draw":

                        print("Draw")
                        self.draws += 1
                        print("Game over.")
                        return

                    elif phrase == "Game not finished":

                        pass

                    counter += 1
