# File run.py: The classic 3x3 TicTac-Toe 2-player game with the choice of
# the starting symbol: choose 0 - for starting with "X", choose 1 for
# starting with "O". The coordinates are shown after the choice.

from game_XO_choice import TheGame

if __name__ == '__main__':
    game = TheGame()
