# File to_tests.py

"""
Why is there a separate file for the testing purposes?

I've created many methods which returned many objects, which turned out
to be problematic during the tests. And a few of them had to be adjusted
for the lack of the player input. Lastly, for the purpose of consistency
I've decided to move all the remaining methods being tested to
the separate file.
"""

from typing import Dict, Tuple, Any, Union, List


def empty_field() -> str:
    """
    Returns an "empty" string. That is the 9-element-long string composed of
    "_".

    :returns: The new 9-element-long "empty" string composed of "_".
    Example: "_________".
    :rtype: str
    """

    c1: str
    c2: str
    c3: str
    c4: str
    c5: str
    c6: str
    c7: str
    c8: str
    c9: str

    c1 = c2 = c3 = c4 = c5 = c6 = c7 = c8 = c9 = '_'

    new_f_string: str = f'{c1}{c2}{c3}{c4}{c5}{c6}{c7}{c8}{c9}'

    return new_f_string


def checking_the_game_result(cells: str) -> str:
    """
    The function having received the string (the string here is the
    9-element-long combination of the 3 elements : "X", "_", and "O" in the
    varying degrees) analyzes it for the 5 possible results. 3 of those
    possible results - "X wins", "O wins" and "Draw" - if their "if/elif"
    conditional blocks are evaluated to True -  are printed out and that ends
    the game. The conditional evaluation to True in the "if/elif" block of
    "Game not finished" - only results in prompting the player for entering
    the another pair of the coordinates. There is also possibility of the
    conditional evaluation to "Impossible", which under the current game
    settings is rather unattainable (but was useful during the development
    and the testing stages).

    :param cells: The 9-element-long string composed of 3 symbols ("X", "O",
     "_") in the varying degrees. Examples: "_OXXXOOXO", "XOXXXOOXO".
    :type cells: str
    :returns: The evaluation of the actual game progress/result - "O wins",
     "X wins", "Draw", "Game not finished" or "Impossible" (the last one
     disabled in the current game settings). Example: "Draw".
    :rtype: str
    """

    c1: str = cells[0]
    c2: str = cells[1]
    c3: str = cells[2]
    c4: str = cells[3]
    c5: str = cells[4]
    c6: str = cells[5]
    c7: str = cells[6]
    c8: str = cells[7]
    c9: str = cells[8]

    # The who_wins_phrase variable instantiated to an empty string.

    who_wins_phrase: str = ''

    # X wins:

    if c1 == c2 == c3 == "X" or \
            c4 == c5 == c6 == "X" or \
            c7 == c8 == c9 == "X" or \
            c1 == c4 == c7 == "X" or \
            c2 == c5 == c8 == "X" or \
            c3 == c6 == c9 == "X" or \
            c1 == c5 == c9 == "X" or \
            c7 == c5 == c3 == "X":
        who_wins_phrase = "X wins"

    # O wins:

    elif c1 == c2 == c3 == "O" or \
            c4 == c5 == c6 == "O" or \
            c7 == c8 == c9 == "O" or \
            c1 == c4 == c7 == "O" or \
            c2 == c5 == c8 == "O" or \
            c3 == c6 == c9 == "O" or \
            c1 == c5 == c9 == "O" or \
            c7 == c5 == c3 == "O":
        who_wins_phrase = "O wins"

    # Game not finished:

    elif (c1 != c2 == c3 or
          c1 == c2 != c3 or
          c1 != c2 != c3 or
          c1 == c2 == c3 == '_') \
            and (c4 != c5 == c6 or
                 c4 == c5 != c6 or
                 c4 != c5 != c6 or
                 c4 == c5 == c6 == '_') \
            and (c7 != c8 == c9 or
                 c7 == c8 != c9 or
                 c7 != c8 != c9 or
                 c7 == c8 == c9 == '_') \
            and (c1 != c4 == c7 or
                 c1 == c4 != c7 or
                 c1 != c4 != c7 or
                 c1 == c4 == c7 == '_') \
            and (c2 != c5 == c8 or
                 c2 == c5 != c8 or
                 c2 != c5 != c8 or
                 c2 == c5 == c8 == '_') \
            and (c3 != c6 == c9 or
                 c3 == c6 != c9 or
                 c3 != c6 != c9 or
                 c3 == c6 == c9 == '_') \
            and (c1 != c5 == c9 or
                 c1 == c5 != c9 or
                 c1 != c5 != c9 or
                 c1 == c5 == c9 == '_') \
            and (c7 != c5 == c3 or
                 c7 != c5 != c3 or
                 c7 == c5 != c3 or
                 c7 == c5 == c3 == '_') \
            and ("_" in cells):
        who_wins_phrase = "Game not finished"

    # Draw:

    elif ((c1 == c3 != c2 or
           c1 == c2 != c3 or
           c1 != c2 == c3 or
           c1 != c2 != c3)
          and (c4 != c5 == c6 or
               c4 == c5 != c6 or
               c4 != c5 != c6 or
               c4 == c6 != c6)
          and (c7 != c8 == c9 or
               c7 == c8 != c9 or
               c7 == c9 != c8 or
               c7 != c8 != c9)
          and (c1 != c4 == c7 or
               c1 == c4 != c7 or
               c1 == c7 != c4 or
               c1 != c4 != c7)
          and (c2 != c5 == c8 or
               c2 == c5 != c8 or
               c2 == c8 != c5 or
               c2 != c5 != c8)
          and (c3 != c6 == c9 or
               c3 == c6 != c9 or
               c3 == c9 != c6 or
               c3 != c6 != c9)
          and (c1 != c5 == c9 or
               c1 == c5 != c9 or
               c1 == c9 != c5 or
               c1 != c5 != c9)
          and (c7 != c5 == c3 or
               c7 == c5 != c3 or
               c7 == c3 != c5 or
               c7 != c5 != c3)) \
            and (not "_" in cells):
        who_wins_phrase = "Draw"

    # Impossible:

    elif (
            (
                    c1 == c2 == c3 == "X" or
                    c4 == c5 == c6 == "X" or
                    c7 == c8 == c9 == "X" or
                    c1 == c4 == c7 == "X" or
                    c2 == c5 == c8 == "X" or
                    c3 == c6 == c9 == "X" or
                    c1 == c5 == c9 == "X" or
                    c7 == c5 == c3 == "X"
            )

            and
            (
                    c1 == c2 == c3 == "O" or
                    c4 == c5 == c6 == "O" or
                    c7 == c8 == c9 == "O" or
                    c1 == c4 == c7 == "O" or
                    c2 == c5 == c8 == "O" or
                    c3 == c6 == c9 == "O" or
                    c1 == c5 == c9 == "O" or
                    c7 == c5 == c3 == "O"
            )
    ):
        who_wins_phrase = "Impossible"

    return who_wins_phrase


def enter_the_coordinates(coordinates: str) -> Tuple[int, int]:
    """
    Takes in the string of the coordinates in the raw form ("0 0").
    Strips them off the trailing or/and the leading whitespaces and the
    accidental non-alphanumeric symbols.
    Splits them into a list.
    Eventually, strips the elements of the list of the whitespaces and
    the non-alphanumeric symbols, if that's necessary and
    feasible.
    Checks the elements of the list for them being a digit.
    If that's true, converts the previously checked
    element into an integer and appends it into a new list.
    Returns a tuple of 2 integers.

    :param coordinates: The pair of coordinates in the raw format "0 0".
    Example: "1 3".
    :type coordinates: str
    :returns: The tuple of 2 integers. Example: (1, 3).
    :rtype: Tuple(int, int)
    """

    coordinates: str = coordinates.strip()
    coordinates_splited: List[str] = coordinates.split(" ")
    new_coordinates: List[int] = []

    coordinate: str

    for coordinate in coordinates_splited:
        coordinate: str = coordinate.strip()
        if coordinate.isdigit():
            coordinate_int: int = int(coordinate)
            new_coordinates.append(coordinate_int)

    return new_coordinates[0], new_coordinates[1]


def O_fields_without_checking_cells(
        coordinates: Tuple[int, int], new_X_dict: Dict[Tuple[int, int], str],
        new_X_cells: str) -> str:
    """
    Receives the dictionary and the string with the new "X" previously
    entered. Receives the coordinates for the new "O". Checks if the chosen
    place is free and if that's the case, it returns the new string with the
    newly positioned "O".

    :param coordinates: A pair of the coordinates (a tuple of 2 integers) in
     the format (0, 0). Example : (1, 2).
    :type coordinates: Tuple[int, int]
    :param new_X_dict: The dictionary with the keys of the tuples of 2
    integers and the values of the cells of the matrix with the newest
    previous addition of "X". Example: new_X_dict = { (1, 1): "_",
    (1, 2): "O", (coordinates): "X", ...}.
    :type new_X_dict: Dict[Tuple[int, int], str]
    :param new_X_cells: The string composed of the 9-element variation of the
     values - "X", "O" and "_" -  with the newest previous addition of "X".
     Example: "_OXXXOOX_".
    :type new_X_cells: str
    :returns: The string with the newly positioned "O" according to the
     coordinates passed in. Example: "_OXXXOOXO".
    :rtype: str

    """

    if check_if_cell_occupied(new_X_dict, coordinates[0], coordinates[1]):
        new_O_cells: str = print_matrix_cells_new_O_string(
            new_X_cells, coordinates[0], coordinates[1])

        return new_O_cells


def O_fields_without_checking_dict(
        coordinates: Tuple[int, int], new_X_dict: Dict[Tuple[int, int], str],
        new_X_cells: str) -> Dict[Tuple[int, int], str]:
    """
    Receives the dictionary and the string with the new "X" previously
    entered. Receives the coordinates of the new "O" in the form of the tuple
    of 2 integers. Checks if the chosen place is free and if that's the case,
    it returns the new dictionary with the newly positioned "O".

    :param coordinates: The coordinates of the new "O" in the form of the
     tuple of 2 integers. Example: (1, 1).
    :type coordinates: Tuple[int, int]
    :param new_X_dict: The dictionary with the new "X" previously entered,
     keys: the pairs of integer tuples, values: "X","O" or "_".
     Example : new_X_dict = dict( (3,2) = "X", (3, 3) = "O", ...).
    :type new_X_dict: Dict[Tuple[int, int], str]
    :param new_X_cells: The string with the new "X" previously entered.
    Example: "_OXXXOOX_".
    :type new_X_cells: str
    :returns: The dictionary with the newly inserted "O" according to the
     coordinates passed in. new_X_dict = dict( (3,2) = "X", (3, 3) = "O",
     (3, 1) = "O", (2, 3) = "_", ... )
    :rtype: Dict[Tuple(int, int)], str]
    """

    if check_if_cell_occupied(new_X_dict, coordinates[0], coordinates[1]):
        new_O_dict: Dict[Tuple[int, int], str] = print_matrix_cells_new_O_dict(
            new_X_cells, coordinates[0],
            coordinates[1])

        return new_O_dict


def X_field_without_checking_cells(
        coordinates: Tuple[int, int], new_O_dict: Dict[Tuple[int, int], str],
        new_O_cells: str) -> str:
    """
    Receives the dictionary and the string with the new "O" previously
    entered. Receives the coordinates of the new "X". Checks if the chosen
    place is free and if that's the case, it returns the new string with the
    newly positioned "X".

    :param coordinates: The coordinates of the new "X" in the form of the
     tuple of 2 integers. Example: (2, 2).
    :type coordinates: Tuple[int, int]
    :param new_O_dict: The dictionary with the new "O" previously entered,
     keys: the pairs of the integer tuples (values from 1 to 3), values: "X",
     "O" or "_". Example : new_O_dict =  {(1, 2): "X", (1, 1): "_", ... }.
    :type new_O_dict: Dict[Tuple[int, int], str]
    :param new_O_cells: The string with the new "O" previously entered.
    Example: "_OX_XOOX_".
    :type new_O_cells: str
    :returns: The string with the newly inserted "X" according to the
     coordinates passed in. Example: "_OXXXOOX_".
    :rtype: str
    """

    if check_if_cell_occupied(new_O_dict, coordinates[0], coordinates[1]):
        new_X_cells: str = print_matrix_cells_new_X_string(
            new_O_cells, coordinates[0],
            coordinates[1])

        return new_X_cells


def X_field_without_checking_dict(
        coordinates: Tuple[int, int], new_O_dict: Dict[Tuple[int, int], str],
        new_O_cells: str) -> Dict[Tuple[int, int], str]:
    """
    Receives the dictionary and the string with the new "O" previously
    entered. Receives the coordinates of the new "X". Checks if the chosen
    place is free and if that's the case, it returns the new dictionary
    with the newly positioned "X".

    :param coordinates: A tuple of 2 integers being the pair of the coordinates
     for the new "X". Example: (3, 2).
    :type coordinates: Tuple[int, int]
    :param new_O_dict: The dictionary with the new "O" previously entered,
     keys: the pairs of the integer tuples (values from 1 to 3), values: "X",
     "O" or "_".
     Example: {(1, 1): "_", (1, 2): "_", (1, 3): "O", ...}.
    :type new_O_dict: Dict[Tuple[int, int], str]
    :param new_O_cells: The string with the new "O" previously entered.
    Example: "_OX__O_X_".
    :type new_O_cells: str
    :returns: The dictionary with the newly inserted "X" according to the
     coordinates passed in.
     Example: {(1, 1): "X", (1, 2): "_", (1, 3): "O", ...}.
    :rtype: Dict[Tuple[int, int], str]

    """

    if check_if_cell_occupied(new_O_dict, coordinates[0], coordinates[1]):
        new_X_dict: Dict[Tuple[int, int], str] = print_matrix_cells_new_X_dict(
            new_O_cells, coordinates[0],
            coordinates[1])

        return new_X_dict


def print_matrix_cells(cells: str) -> Dict[Tuple[int, int], str]:
    """
    Having the string passed in, assigns the coordinates (the tuple of 2
    integers) key to its every "X"/"O"/"_" value. Returns the dictionary
    of the key : value pairs.

    :param cells: The 9-element-long string of "X"/"O"/"_" values.
    Example: "__X__O___", "_________".
    :type cells: str
    :returns: The dictionary of the key ( the tuples of 2 integers - the
     coordinates) : value ("X", "O" or "_") pairs.
     Example: {(3, 1): "_", (3, 2): "O", (3, 3): "X", ...}.
    :rtype: Dict[Tuple[int, int], str]
    """

    new_dict: Dict[Tuple[int, int], str] = dict()

    new_dict[(1, 3)] = cells[0]
    new_dict[(2, 3)] = cells[1]
    new_dict[(3, 3)] = cells[2]
    new_dict[(1, 2)] = cells[3]
    new_dict[(2, 2)] = cells[4]
    new_dict[(3, 2)] = cells[5]
    new_dict[(1, 1)] = cells[6]
    new_dict[(2, 1)] = cells[7]
    new_dict[(3, 1)] = cells[8]

    return new_dict


def print_matrix_cells_new_X_string(
        cells: str, first_coordinate: int,
        second_coordinate: int) -> str:
    """
    Takes the string with the newly assigned "O" or the empty string
    with "X" to be assigned and the two coordinates, then assigns "X" to the
    given coordinates in the dictionary. Returns the new string.

    :param cells: The 9-element-long long string of  "X"/"O"/"_" values,
    or only "_" values. Examples: "_________", "___0_____".
    :type cells: str
    :param first_coordinate: The first coordinate of the pair. An integer - 1,
     2 or 3. Example: 2.
    :type first_coordinate: int
    :param second_coordinate: The second coordinate of the pair. An integer - 1,
     2 or 3. Example: 1.
    :type second_coordinate: int
    :returns: A string with the newly inserted "X" according to the
     coordinates passed in. Examples: "__X______", "__XO_____".
    :rtype: str
    """

    new_dict: Dict[Tuple[int, int], str] = dict()

    new_dict[(1, 3)] = cells[0]
    new_dict[(2, 3)] = cells[1]
    new_dict[(3, 3)] = cells[2]
    new_dict[(1, 2)] = cells[3]
    new_dict[(2, 2)] = cells[4]
    new_dict[(3, 2)] = cells[5]
    new_dict[(1, 1)] = cells[6]
    new_dict[(2, 1)] = cells[7]
    new_dict[(3, 1)] = cells[8]

    new_dict[(first_coordinate, second_coordinate)] = 'X'

    new_f_string: str = f"{new_dict[(1, 3)]}{new_dict[(2, 3)]}{new_dict[(3, 3)]}" \
                        f"{new_dict[(1, 2)]}{new_dict[(2, 2)]}{new_dict[(3, 2)]}{new_dict[(1, 1)]}" \
                        f"{new_dict[(2, 1)]}{new_dict[(3, 1)]}"

    return new_f_string


def print_matrix_cells_new_X_dict(
        cells: str, first_coordinate: int,
        second_coordinate: int) -> Dict[Tuple[int, int], str]:
    """
    Having the string with the newly assigned "O" or the "empty" string with
    "X" to be assigned passed in and the two coordinates in the form of
    integers, assigns the "X" to the given coordinates after preprocessing
    them into a key of a tuple of 2 integers in the dictionary. Returns the
    new dictionary.

    :param cells: The 9-element-long string of "X"/"O"/"_" values.
    Example: "__X_O_O__".
    :type cells: str
    :param first_coordinate: The first coordinate of the pair. An integer - 1,
     2 or 3. Example: 2.
    :type first_coordinate: int
    :param second_coordinate: The second coordinate of the pair. An integer
     - 1, 2 or 3. Example: 3.
    :type second_coordinate: int
    :returns: A dictionary with the newly inserted "X" according to the
     coordinates passed in.
     Example: {(2, 1): "X", (2, 2): "_", ...}.
    :rtype: Dict[Tuple[int, int], str]
    """

    new_dict: Dict[Tuple[int, int], str] = dict()

    new_dict[(1, 3)] = cells[0]
    new_dict[(2, 3)] = cells[1]
    new_dict[(3, 3)] = cells[2]
    new_dict[(1, 2)] = cells[3]
    new_dict[(2, 2)] = cells[4]
    new_dict[(3, 2)] = cells[5]
    new_dict[(1, 1)] = cells[6]
    new_dict[(2, 1)] = cells[7]
    new_dict[(3, 1)] = cells[8]

    new_dict[(first_coordinate, second_coordinate)] = 'X'

    return new_dict


def print_matrix_cells_new_O_dict(
        cells: str, first_coordinate: int,
        second_coordinate: int) -> Dict[Tuple[int, int], str]:
    """
    Having the string with the newly assigned "X" and the two coordinates
    in the form of 2 integers passed in, assigns the "O" to the given coordinates
    tuple key in the dictionary. Returns the new dictionary.

    :param cells: The 9-element-long string of the "X"/"O"/"_" values.
    Example: "__OX_O_X_".
    :type cells: str
    :param first_coordinate: The first coordinate of the pair. An integer -
    1, 2 or 3. Example: 1
    :type first_coordinate: int
    :param second_coordinate: The second coordinate of the pair. An integer -
    1, 2 or 3. Example: 3.
    :type second_coordinate: int
    :returns: A dictionary with the newly inserted "O" according to the
     coordinates passed in.
     Example: {(2, 1): "X", (2, 2): "O", ...}.
    :rtype: Dict[Tuple[int, int], str]

    """

    new_dict: Dict[Tuple[int, int], str] = dict()

    new_dict[(1, 3)] = cells[0]
    new_dict[(2, 3)] = cells[1]
    new_dict[(3, 3)] = cells[2]
    new_dict[(1, 2)] = cells[3]
    new_dict[(2, 2)] = cells[4]
    new_dict[(3, 2)] = cells[5]
    new_dict[(1, 1)] = cells[6]
    new_dict[(2, 1)] = cells[7]
    new_dict[(3, 1)] = cells[8]

    new_dict[(first_coordinate, second_coordinate)] = 'O'

    return new_dict


def print_matrix_cells_new_O_string(
        cells: str, first_coordinate: int,
        second_coordinate: int) -> str:
    """
    Having the string with the newly assigned "X" and the two coordinates
    in the form of 2 integers passed in, assigns the "O" to the given pair of
    coordinates in the dictionary. Returns the new string with a newly
    assigned "O".

    :param cells: The 9-letters-long string of the "X"/"O"/"_" values.
    Example: "_XOX_O_X_".
    :type cells: str
    :param first_coordinate: The first coordinate of the pair. An integer - 1,
     2 or 3. Example: 1.
    :type first_coordinate: int
    :param second_coordinate: The second coordinate of the pair. An integer
     - 1, 2 or 3. Example: 2.
    :type second_coordinate: int
    :returns: A string with the newly inserted "O" according to the coordinates
     passed in. Example: "_XOX_O_XO".
    :rtype: str
    """

    new_dict: Dict[Tuple[int, int], str] = dict()

    new_dict[(1, 3)] = cells[0]
    new_dict[(2, 3)] = cells[1]
    new_dict[(3, 3)] = cells[2]
    new_dict[(1, 2)] = cells[3]
    new_dict[(2, 2)] = cells[4]
    new_dict[(3, 2)] = cells[5]
    new_dict[(1, 1)] = cells[6]
    new_dict[(2, 1)] = cells[7]
    new_dict[(3, 1)] = cells[8]

    new_dict[(first_coordinate, second_coordinate)] = 'O'

    new_f_string: str = f'{new_dict[(1, 3)]}{new_dict[(2, 3)]}{new_dict[(3, 3)]}' \
                        f'{new_dict[(1, 2)]}{new_dict[(2, 2)]}{new_dict[(3, 2)]}{new_dict[(1, 1)]}' \
                        f'{new_dict[(2, 1)]}{new_dict[(3, 1)]}'

    return new_f_string


def check_if_coordinates_int(
        first_coordinate: Union[int, Any, float, str],
        second_coordinate: Union[int, Any, float, str]) -> bool:
    """
    Returns False if the first or the second coordinate is not an instance of
    an integer. Otherwise returns True.

    :param first_coordinate: The first coordinate of the pair.
    Expected: An integer - 1, 2 or 3. Example: 2.
    :type first_coordinate: int
    :param second_coordinate: The second coordinate of the pair.
    Expected: An integer - 1, 2 or 3. Example: 3.
    :type second_coordinate: int
    :returns: False, if the first or the second coordinate is not an instance
     of an integer. Otherwise returns True. Example: False.
    :rtype: bool
    """

    if not isinstance(first_coordinate, int) or \
            not isinstance(second_coordinate, int):
        return False

    return True


def check_if_coordinates_in_range(
        first_coordinate: int,
        second_coordinate: int) -> bool:
    """
    Returns False if the first or the second coordinate ∉ <1, 3>.
    Otherwise returns True.

    :param first_coordinate: The first coordinate of the pair.
    An integer - 1, 2 or 3. Example: 3.
    :type first_coordinate: int
    :param second_coordinate: The second coordinate of the pair.
    An integer - 1, 2 or 3. Example: 1.
    :type second_coordinate: int
    :returns: False if the first or the second coordinate ∉ <1, 3>.
    Otherwise returns True. Example: True.
    :rtype: bool
    """

    if first_coordinate not in [1, 2, 3] or \
            second_coordinate not in [1, 2, 3]:
        return False

    return True


def check_if_cell_occupied(
        new_dict: Dict[Tuple[int, int], str], first_coordinate: int,
        second_coordinate: int) -> bool:
    """
    Returns False if the dictionary value under the key (first_coordinate,
    second_coordinate) is not empty (not equals "_").
    Otherwise returns True.

    :param new_dict: The dictionary passed in.
    Example: new_dict : {(1, 1): "_", (1, 2): "O", (1, 3): "X", (2, 1): "_",
     ...}.
    :type new_dict: Dict[Tuple[int, int], str]
    :param first_coordinate: The first coordinate of the pair (first part of
     the key). An integer - 1, 2 or 3. Example: 2.
    :type first_coordinate: int
    :param second_coordinate: The second coordinate of the pair (second part
     of the key). An integer - 1, 2 or 3. Example: 1.
    :type second_coordinate: int
    :returns: False, if dictionary value under the key (first_coordinate,
    second_coordinate) is not empty (not equals "_"). Otherwise returns True.
    Example: False.
    :rtype: bool
    """

    if new_dict[(first_coordinate, second_coordinate)] != '_':
        return False

    return True


def check_if_choice_int(choice: Union[int, Any, float, str]) -> bool:
    """
    Checks if the choice is an instance of an integer.

    :param choice: An integer - 0 or 1. Expected: 0 or 1.
     Example: 0.
    :type choice: int (Union[int, Any, float, str])
    :returns: False if choice is not an instance of an integer.
    Otherwise returns True. Example: True.
    :rtype: bool
    """

    if not isinstance(choice, int):
        return False

    return True



def checking_the_game_result_partial_win_put_X(cells: str) -> Tuple[int, int]:
    """
    The method, having received the string (the combination of 9 symbols:
    "X", "_", and "O" in varying degree) analyzes it for the 5 possible
     results. The conditional evaluation to True in the 3 of "if/elif" blocks
    may result in termination of the game with the printout of the final
    result, which may be - "X wins", "O wins" or "Draw" - when there is no
    win. When the conditional evaluation evaluates to True in the "Game not
    finished" block - the player is only prompted to enter the another
    pair of the coordinates. The conditional evaluation of the passed-in
    string to "Impossible" is also feasible. However, under the current game
    setting rather unreachable.

    :param cells: The 9-elements-long string composed of 3 symbols ("X", "O",
    "_") in varying degrees.
    Example: "OXXOOXXOX".
    :type cells: str
    :returns: The evaluation of the actual game progress/result - "O wins",
    "X wins", "Draw", "Game not finished" or "Impossible" (disabled in the
    current game settings). Example: "Game not finished".
    :rtype: str
    """

    c1: str = cells[0]
    c2: str = cells[1]
    c3: str = cells[2]
    c4: str = cells[3]
    c5: str = cells[4]
    c6: str = cells[5]
    c7: str = cells[6]
    c8: str = cells[7]
    c9: str = cells[8]

    # X wins:

    if c1 == c2 == "X" and c3 == "_":
        return (3, 3)
    elif c1 == c3 == "X" and c2 == "_":
        return (2, 3)
    elif c2 == c3 == "X" and c1 == "_":
        return (1, 3)
    elif c4 == c5 == "X" and c6 == "_":
        return (3, 2)
    elif c4 == c6 == "X" and c5 == "_":
        return (2, 2)
    elif c5 == c6 == "X" and c4 == "_":
        return (1, 2)
    elif c7 == c8 == "X" and c9 == "_":
        return (3, 1)
    elif c7 == c9 == "X" and c8 == "_":
        return (2, 1)
    elif c8 == c9 == "X" and c7 == "_":
        return (1, 1)
    elif c1 == c4 == "X" and c7 == "_":
        return (1, 1)
    elif c1 == c7 == "X" and c4 == "_":
        return (1, 2)
    elif c4 == c7 == "X" and c1 == "_":
        return (1, 3)
    elif c2 == c5 == "X" and c8 == "_":
        return (2, 1)
    elif c2 == c8 == "X" and c5 == "_":
        return (2, 2)
    elif c5 == c8 == "X" and c2 == "_":
        return (2, 3)
    elif c3 == c6 == "X" and c9 == "_":
        return (3, 1)
    elif c3 == c9 == "X" and c6 == "_":
        return (3, 2)
    elif c6 == c9 == "X" and c3 == "_":
        return (3, 3)
    elif c1 == c5 == "X" and c9 == "_":
        return (3, 1)
    elif c1 == c9 == "X" and c5 == "_":
        return (2, 2)
    elif c5 == c9 == "X" and c1 == "_":
        return (1, 3)
    elif c7 == c5 == "X" and c3 == "_":
        return (3, 3)
    elif c7 == c3 == "X" and c5 == "_":
        return (2, 2)
    elif c5 == c3 == "X" and c7 == "_":
        return (1, 1)
    elif c1 == c2 == "O" and c3 == "_":
        return (3, 3)
    elif c1 == c3 == "O" and c2 == "_":
        return (2, 3)
    elif c2 == c3 == "O" and c1 == "_":
        return (1, 3)
    elif c4 == c5 == "O" and c6 == "_":
        return (3, 2)
    elif c4 == c6 == "O" and c5 == "_":
        return (2, 2)
    elif c5 == c6 == "O" and c4 == "_":
        return (1, 2)
    elif c7 == c8 == "O" and c9 == "_":
        return (3, 1)
    elif c7 == c9 == "O" and c8 == "_":
        return (2, 1)
    elif c8 == c9 == "O" and c7 == "_":
        return (1, 1)
    elif c1 == c4 == "O" and c7 == "_":
        return (1, 1)
    elif c1 == c7 == "O" and c4 == "_":
        return (1, 2)
    elif c4 == c7 == "O" and c1 == "_":
        return (1, 3)
    elif c2 == c5 == "O" and c8 == "_":
        return (2, 1)
    elif c2 == c8 == "O" and c5 == "_":
        return (2, 2)
    elif c5 == c8 == "O" and c2 == "_":
        return (2, 3)
    elif c3 == c6 == "O" and c9 == "_":
        return (3, 1)
    elif c3 == c9 == "O" and c6 == "_":
        return (3, 2)
    elif c6 == c9 == "O" and c3 == "_":
        return (3, 3)
    elif c1 == c5 == "O" and c9 == "_":
        return (3, 1)
    elif c1 == c9 == "O" and c5 == "_":
        return (2, 2)
    elif c5 == c9 == "O" and c1 == "_":
        return (1, 3)
    elif c7 == c5 == "O" and c3 == "_":
        return (3, 3)
    elif c7 == c3 == "O" and c5 == "_":
        return (2, 2)
    elif c5 == c3 == "O" and c7 == "_":
        return (1, 1)


def checking_the_game_result_partial_win_put_O(cells: str) -> Tuple[int, int]:
    """
        The method, having received the string (the combination of 9 symbols:
        "X", "_", and "O" in varying degree) analyzes it for the 5 possible
        results. The conditional evaluation to True in the 3 of "if/elif" blocks
        may result in termination of the game with the printout of the final
        result, which may be - "X wins", "O wins" or "Draw" - when there is no
        win. When the conditional evaluation evaluates to True in the "Game not
        finished" block - the player is only prompted to enter the another
        pair of the coordinates. The conditional evaluation of the passed-in
        string to "Impossible" is also feasible. However, under the current game
        setting rather unreachable.

        :param cells: The 9-elements-long string composed of 3 symbols ("X", "O",
         "_") in varying degrees.
        Example: "OXXOOXXOX".
        :type cells: str
        :returns: The evaluation of the actual game progress/result - "O wins",
        "X wins", "Draw", "Game not finished" or "Impossible" (disabled in the
        current game settings). Example: "Game not finished".
        :rtype: str
    """

    c1: str = cells[0]
    c2: str = cells[1]
    c3: str = cells[2]
    c4: str = cells[3]
    c5: str = cells[4]
    c6: str = cells[5]
    c7: str = cells[6]
    c8: str = cells[7]
    c9: str = cells[8]

    # X wins:

    if c1 == c2 == "O" and c3 == "_":
        return (3, 3)
    elif c1 == c3 == "O" and c2 == "_":
        return (2, 3)
    elif c2 == c3 == "O" and c1 == "_":
        return (1, 3)
    elif c4 == c5 == "O" and c6 == "_":
        return (3, 2)
    elif c4 == c6 == "O" and c5 == "_":
        return (2, 2)
    elif c5 == c6 == "O" and c4 == "_":
        return (1, 2)
    elif c7 == c8 == "O" and c9 == "_":
        return (3, 1)
    elif c7 == c9 == "O" and c8 == "_":
        return (2, 1)
    elif c8 == c9 == "O" and c7 == "_":
        return (1, 1)
    elif c1 == c4 == "O" and c7 == "_":
        return (1, 1)
    elif c1 == c7 == "O" and c4 == "_":
        return (1, 2)
    elif c4 == c7 == "O" and c1 == "_":
        return (1, 3)
    elif c2 == c5 == "O" and c8 == "_":
        return (2, 1)
    elif c2 == c8 == "O" and c5 == "_":
        return (2, 2)
    elif c5 == c8 == "O" and c2 == "_":
        return (2, 3)
    elif c3 == c6 == "O" and c9 == "_":
        return (3, 1)
    elif c3 == c9 == "O" and c6 == "_":
        return (3, 2)
    elif c6 == c9 == "O" and c3 == "_":
        return (3, 3)
    elif c1 == c5 == "O" and c9 == "_":
        return (3, 1)
    elif c1 == c9 == "O" and c5 == "_":
        return (2, 2)
    elif c5 == c9 == "O" and c1 == "_":
        return (1, 3)
    elif c7 == c5 == "O" and c3 == "_":
        return (3, 3)
    elif c7 == c3 == "O" and c5 == "_":
        return (2, 2)
    elif c5 == c3 == "O" and c7 == "_":
        return (1, 1)
    elif c1 == c2 == "X" and c3 == "_":
        return (3, 3)
    elif c1 == c3 == "X" and c2 == "_":
        return (2, 3)
    elif c2 == c3 == "X" and c1 == "_":
        return (1, 3)
    elif c4 == c5 == "X" and c6 == "_":
        return (3, 2)
    elif c4 == c6 == "X" and c5 == "_":
        return (2, 2)
    elif c5 == c6 == "X" and c4 == "_":
        return (1, 2)
    elif c7 == c8 == "X" and c9 == "_":
        return (3, 1)
    elif c7 == c9 == "X" and c8 == "_":
        return (2, 1)
    elif c8 == c9 == "X" and c7 == "_":
        return (1, 1)
    elif c1 == c4 == "X" and c7 == "_":
        return (1, 1)
    elif c1 == c7 == "X" and c4 == "_":
        return (1, 2)
    elif c4 == c7 == "X" and c1 == "_":
        return (1, 3)
    elif c2 == c5 == "X" and c8 == "_":
        return (2, 1)
    elif c2 == c8 == "X" and c5 == "_":
        return (2, 2)
    elif c5 == c8 == "X" and c2 == "_":
        return (2, 3)
    elif c3 == c6 == "X" and c9 == "_":
        return (3, 1)
    elif c3 == c9 == "X" and c6 == "_":
        return (3, 2)
    elif c6 == c9 == "X" and c3 == "_":
        return (3, 3)
    elif c1 == c5 == "X" and c9 == "_":
        return (3, 1)
    elif c1 == c9 == "X" and c5 == "_":
        return (2, 2)
    elif c5 == c9 == "X" and c1 == "_":
        return (1, 3)
    elif c7 == c5 == "X" and c3 == "_":
        return (3, 3)
    elif c7 == c3 == "X" and c5 == "_":
        return (2, 2)
    elif c5 == c3 == "X" and c7 == "_":
        return (1, 1)

def check_if_choice_in_range(choice: int) -> bool:
    """
    Checks if the choice is 0 or 1.
    Otherwise returns False.

    :param choice: An integer - 0 or 1. Example: 0.
    :type choice: int
    :returns: False if the choice variable is not 0 or 1.
    Otherwise returns True. Example: False.
     :rtype: bool
    """

    if choice not in [0, 1, 2, 3, 4, 5, 6, 7, 8]:
        return False

    return True