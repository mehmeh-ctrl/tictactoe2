# File run_testing_automatic.py: The number of the classic 3x3 tic-tac-toe
# games to play in the automatic build mode drawn pseudorandomly from the
# range of integers, where integer ∈ <1 000, 10 000 000> and the symbol to
# play with first chosen pseudorandomly. The summaries are
# printed at the end and saved to a text file. Text files have
# descriptive names: "testing_{starting_symbol}_game_
# {dimensions_of_the_matrix}_
# {number_of_the_games_played}_{result of calling of the
# time.time() function trimmed to a integer format}.txt".


from choice_testing_automatic import TheGame

if __name__ == '__main__':
    game = TheGame()
