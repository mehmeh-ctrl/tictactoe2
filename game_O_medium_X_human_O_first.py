# game_O_medium_X_human_O_first.py

from typing import Dict, Tuple

from tictactoe import TicTacToeHalfRandomXHumanOComputer


class GameXHumanOComputerOFirstMidLevel:
    """
    The main game logic. With "O" playing first.
    The medium-level agents plays with "O"
    """

    # The phrase variable instantiated to an empty string.

    phrase: str = ''

    def play(self) -> None:

        """
        The "empty" string (an 9-element-long composed of "_") is instantiated as
        the new_O_cells. The "empty" dictionary is instantiated as the new_O_dict
        - to set it apart from the "O" that will be placed first and to plug it
        into the loop. The loop uses the counter variable to direct the player
        activity to the relevant blocks of the game. The even counter values
        direct the player activity towards typing in the new "X" coordinates (the
        X_field_without_checking method), the uneven counter values - towards
        pseudorandomly drawing in the new "O" coordinates (the
        O_field_without_checking method). The values of the counter greater or
        equal to 4 are directed towards the block with the checking of the game
        results. There, the string plugged into the method
        checking_the_game_result may resolve to 4 outcomes - 3 of them end in the
        resolution of the game with the printout of the result - "X wins",
        "O wins" (when player with "X" or "O" wins, respectively) and "Draw"
        - when there is no win. The string may also resolve to "Game not finished"
        - the game then continues.

        :param self: An instance of the class.
        :type self: An instance of the class.
        :return: None. Example: None.
        :rtype: None
        """
        tic_tac_toe = TicTacToeHalfRandomXHumanOComputer()

        # The helper coordinates printed:

        print("Coordinates of the cells:")
        print(tic_tac_toe.cell_coordinates())

        # The empty field printed:

        print(tic_tac_toe.empty_field()[0])

        # The counter variable set to 0.

        counter: int = 0

        new_O_cells: str = ''
        new_O_dict: Dict[Tuple[int, int], str] = dict()

        new_X_cells: str = tic_tac_toe.empty_field()[1]
        new_X_dict: Dict[Tuple[int, int], str] = tic_tac_toe.print_matrix_cells(new_X_cells)[1]

        while counter < 8:

            while counter < 4:

                if counter % 2 == 1:

                    new_O_field: str

                    new_X_field, new_X_dict, new_X_cells = \
                        TicTacToeHalfRandomXHumanOComputer.X_field_without_checking(
                            tic_tac_toe,
                            new_O_dict, new_O_cells)

                    print(new_X_field)

                    counter += 1

                elif counter % 2 == 0:

                    new_X_field: str

                    new_O_field, new_O_dict, new_O_cells = \
                        TicTacToeHalfRandomXHumanOComputer.O_field_without_checking_mid_level(
                            tic_tac_toe,
                            new_X_dict, new_X_cells)

                    print(new_O_field)

                    counter += 1

            while 4 <= counter < 9:

                if counter % 2 == 1:

                    new_X_field, new_X_dict, new_X_cells = \
                        TicTacToeHalfRandomXHumanOComputer.X_field_without_checking(
                            tic_tac_toe,
                            new_O_dict, new_O_cells)

                    print(new_X_field)

                    phrase = TicTacToeHalfRandomXHumanOComputer.checking_the_game_result(
                        tic_tac_toe, new_X_cells)

                    if phrase == "X wins":

                        print("X wins")
                        return

                    elif phrase == "O wins":

                        print("O wins")
                        return

                    elif phrase == "Draw":

                        print("Draw")
                        return

                    elif phrase == "Game not finished":
                        pass

                    counter += 1

                elif counter % 2 == 0:

                    new_O_field, new_O_dict, new_O_cells = \
                        TicTacToeHalfRandomXHumanOComputer.O_field_without_checking_mid_level(
                            tic_tac_toe,
                            new_X_dict, new_X_cells)

                    print(new_O_field)

                    phrase = TicTacToeHalfRandomXHumanOComputer.checking_the_game_result(
                        tic_tac_toe, new_O_cells)

                    if phrase == "X wins":

                        print("X wins")
                        return

                    elif phrase == "O wins":

                        print("O wins")
                        return

                    elif phrase == "Draw":

                        print("Draw")
                        return

                    elif phrase == "Game not finished":

                        pass

                    counter += 1
