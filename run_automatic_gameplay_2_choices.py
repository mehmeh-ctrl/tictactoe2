# File run_automatic_gameplay_2_choices.py: The classic automatic 3x3
# Tic-Tac-Toe gameplay with 2 kinds of choices drawn pseudorandomly -
# for the automatic building & testing purposes of run_half_random.py
# gameplay.

from game_choice_half_random_gameplay_automatic import TheGame

if __name__ == '__main__':
    game = TheGame()
