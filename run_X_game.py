# run_X_game.py

from typing import List


def main():
    input_commands: str = ''
    while input_commands != "exit":
        input_commands = input("Input command: > ")
        if len(input_commands.split()) == 4:
            input_commands_list: List[str] = input_commands.split(" ")
            user_1: str = input_commands_list[1]
            user_2: str = input_commands_list[2]
            starting_symbol: str = input_commands_list[3]

            if input_commands_list[0] == "start":

                if user_1 in ["medium", "mid"] and user_2 == "hard" and starting_symbol == "O":
                    from gameOrandom_O_MidLevel_X_HardLevel import GameORandom

                    game = GameORandom()
                    game.play()

                elif user_1 == user_2 == "easy" and starting_symbol == "O":
                    from gameO_automatic import GameORandom

                    game = GameORandom()
                    game.play()

                elif user_1 in ["user", "human"] and user_2 == "easy" and starting_symbol == "O":
                    from half_random_gameplayOhumanOfirst import GameO

                    game = GameO()
                    game.play()

                elif user_1 == "easy" and user_2 in ["user", "human"] and starting_symbol == "O":
                    from half_random_gameplayXhumanOfirst import GameO

                    game = GameO()
                    game.play()

                elif user_1 in ["user", "human"] and user_2 in ["user", "human"] and starting_symbol == "O":
                    from gameO import GameO

                    game = GameO()
                    game.play()

                elif user_1 in ["user", "human"] and user_2 in ["medium", "mid"] and starting_symbol == "O":
                    from game_X_Human_O_Computer_OFirst_MidLevel import GameO

                    game = GameO()
                    game.play()

                elif user_1 == "easy" and user_2 in ["medium", "mid"] and starting_symbol == "O":
                    from gameX_easy_O_mid_Ofirst import GameORandom

                    game = GameORandom()
                    game.play()

                elif user_1 == "hard" and user_2 == "easy" and starting_symbol == "O":
                    from gameX_hard_O_easy_Ofirst import GameORandom

                    game = GameORandom()
                    game.play()

                elif user_1 == "hard" and user_2 == "easy" and starting_symbol == "O":
                    from game_O_Computer_X_Computer_OFirst_X_Hard_O_Easy import GameO

                    game = GameO()
                    game.play()

                elif user_1 == user_2 == "easy" and starting_symbol == "X":
                    from gameX_automatic import GameXRandom

                    game = GameXRandom()
                    game.play()

                elif user_1 == "hard" and user_2 in ["medium", "mid"] and starting_symbol == "X":
                    from gameXrandom_OMidLevel_XHard import GameXRandomMidLevel

                    game = GameXRandomMidLevel()
                    game.play()

                elif user_1 in ["user", "human"] and user_2 in ["medium", "mid"] and starting_symbol == "X":
                    from game_X_Human_O_Computer_XFirst_MidLevel import GameXHumanOComputerXFirstMidLevel
                    game = GameXHumanOComputerXFirstMidLevel()
                    game.play()

                elif user_1 == "hard" and user_2 in ["user", "human"] and starting_symbol == "X":
                    from game_XComputer_OHuman_XFirst_X_HardLevel import GameXComputerOHumanXFirstMidLevel

                    game = GameXComputerOHumanXFirstMidLevel()
                    game.play()

                elif user_1 == "easy" and user_2 in ["user", "human"] and starting_symbol == "X":
                    from half_random_gameplayOhumanXfirst import GameX

                    game = GameX()
                    game.play()

                elif user_1 in ["user", "human"] and user_2 == "easy" and starting_symbol == "X":
                    from half_random_gameplayXhumanXfirst import GameX

                    game = GameX()
                    game.play()

                elif user_1 in ["user", "human"] and user_2 in ["user", "human"] and starting_symbol == "X":
                    from gameX import GameX

                    game = GameX()
                    game.play()

                elif user_1 == "easy" and user_2 in ["medium", "mid"] and starting_symbol == "X":
                    from gameX_easy_Omid_XFirst import GameXRandomMidLevel

                    game = GameXRandomMidLevel()
                    game.play()

                elif user_1 == "hard" and user_2 == "easy" and starting_symbol == "X":
                    from gameO_easy_X_Hard_XFirst import GameXRandomMidLevel

                    game = GameXRandomMidLevel()
                    game.play()

                elif user_1 in ["medium", "mid"] and user_2 in ["user", "human"] and starting_symbol == "O":
                    from game_O_medium_X_human_O_first import GameXHumanOComputerOFirstMidLevel

                    game = GameXHumanOComputerOFirstMidLevel()
                    game.play()

                elif user_1 in ["medium", "mid"] and user_2 in ["user", "human"] and starting_symbol == "X":
                    from game_X_medium_O_human_X_first import GameOHumanXComputerXFirstMidLevel

                    game = GameOHumanXComputerXFirstMidLevel()
                    game.play()

                elif user_1 in ["human", "user"] and user_2 == "hard" and starting_symbol == "X":
                    from game_X_human_O_hard_X_first import GameXHumanOComputerXFirstHardLevel

                    game = GameXHumanOComputerXFirstHardLevel()
                    game.play()

                elif user_1 in ["human", "user"] and user_2 == "hard" and starting_symbol == "O":
                    from game_O_human_X_hard_O_first import GameOHumanXComputerOFirstHardLevel

                    game = GameOHumanXComputerOFirstHardLevel()
                    game.play()

                elif user_1 == "hard" and user_2 in ["human", "user"] and starting_symbol == "O":
                    from game_X_human_O_hard_O_first import GameXHumanOComputerOFirstHardLevel

                    game = GameXHumanOComputerOFirstHardLevel()
                    game.play()

                elif user_1 in ["mid", "medium"] and user_2 == "easy" and starting_symbol == "X":
                    from game_X_mid_O_easy_X_first import GameXMid

                    game = GameXMid()
                    game.play()

                elif user_1 in ["mid", "medium"] and user_2 == "easy" and starting_symbol == "O":
                    from game_O_mid_X_easy_O_first import GameOMid

                    game = GameOMid()
                    game.play()

                elif user_1 in ["mid", "medium"] and user_2 in ["mid", "medium"] and starting_symbol == "X":
                    from game_X_MidMid import GameXMid

                    game = GameXMid()
                    game.play()

                elif user_1 in ["mid", "medium"] and user_2 in ["mid", "medium"] and starting_symbol == "O":
                    from game_O_MidMid import GameOMid

                    game = GameOMid()
                    game.play()

                elif user_1 == "easy" and user_2 == "hard" and starting_symbol == "O":
                    from game_O_easy_X_hard_O_first import GameXHardOEasy

                    game = GameXHardOEasy()
                    game.play()

                elif user_1 == "easy" and user_2 == "hard" and starting_symbol == "X":
                    from game_X_easy_O_hard_X_first import GameOHardXEasy

                    game = GameOHardXEasy()
                    game.play()

                elif user_1 in ["medium", "mid"] and user_2 == "hard" and starting_symbol == "X":
                    from game_X_mid_O_hard_X_first import GameXMidOHard

                    game = GameXMidOHard()
                    game.play()

                elif user_1 == "hard" and user_2 in ["medium", "mid"] and starting_symbol == "O":
                    from game_O_hard_X_mid_O_first import GameOHardXMid

                    game = GameOHardXMid()
                    game.play()

                elif user_1 == "hard" and user_2 == "hard" and starting_symbol == "X":
                    from game_X_HardHard import GameXHard

                    game = GameXHard()
                    game.play()

                elif user_1 == "hard" and user_2 == "hard" and starting_symbol == "O":
                    from game_O_HardHard import GameOHard

                    game = GameOHard()
                    game.play()



        elif input_commands == "exit":
            return

        elif len(input_commands.split()) != 4 or input_commands != "exit":

            print("Bad arguments!")

            while input_commands != "exit":

                input_commands = input("Input command: > ")
                if len(input_commands.split()) == 4:
                    input_commands_list = input_commands.split(" ")
                    user_1 = input_commands_list[1]
                    user_2 = input_commands_list[2]
                    starting_symbol: str = input_commands_list[3]

                    if input_commands_list[0] == "start":

                        if user_1 in ["medium", "mid"] and user_2 == "hard" and starting_symbol == "O":
                            from gameOrandom_O_MidLevel_X_HardLevel import GameORandom

                            game = GameORandom()
                            game.play()

                        elif user_1 == user_2 == "easy" and starting_symbol == "O":
                            from gameO_automatic import GameORandom

                            game = GameORandom()
                            game.play()

                        elif user_1 in ["user", "human"] and user_2 == "easy" and starting_symbol == "O":
                            from half_random_gameplayOhumanOfirst import GameO

                            game = GameO()
                            game.play()

                        elif user_1 == "easy" and user_2 in ["user", "human"] and starting_symbol == "O":
                            from half_random_gameplayXhumanOfirst import GameO

                            game = GameO()
                            game.play()

                        elif user_1 in ["user", "human"] and user_2 in ["user", "human"] and starting_symbol == "O":
                            from gameO import GameO

                            game = GameO()
                            game.play()

                        elif user_1 in ["user", "human"] and user_2 in ["medium", "mid"] and starting_symbol == "O":
                            from game_X_Human_O_Computer_OFirst_MidLevel import GameO

                            game = GameO()
                            game.play()

                        elif user_1 == "easy" and user_2 in ["medium", "mid"] and starting_symbol == "O":
                            from game_O_easy_X_mid_O_first import GameORandom

                            game = GameORandom()
                            game.play()

                        elif user_1 == "hard" and user_2 == "easy" and starting_symbol == "O":
                            from game_O_hard_X_easy_O_first import GameORandom

                            game = GameORandom()
                            game.play()

                        elif user_1 == "hard" and user_2 == "easy" and starting_symbol == "O":
                            from game_O_Computer_X_Computer_OFirst_X_Hard_O_Easy import GameO

                            game = GameO()
                            game.play()

                        elif user_1 == user_2 == "easy" and starting_symbol == "X":
                            from gameX_automatic import GameXRandom

                            game = GameXRandom()
                            game.play()

                        elif user_1 == "hard" and user_2 in ["medium", "mid"] and starting_symbol == "X":
                            from gameXrandom_OMidLevel_XHard import GameXRandomMidLevel

                            game = GameXRandomMidLevel()
                            game.play()

                        elif user_1 in ["user", "human"] and user_2 in ["medium", "mid"] and starting_symbol == "X":
                            from game_X_Human_O_Computer_XFirst_MidLevel import GameXHumanOComputerXFirstMidLevel
                            game = GameXHumanOComputerXFirstMidLevel()
                            game.play()

                        elif user_1 == "hard" and user_2 in ["user", "human"] and starting_symbol == "X":
                            from game_XComputer_OHuman_XFirst_X_HardLevel import GameXComputerOHumanXFirstMidLevel

                            game = GameXComputerOHumanXFirstMidLevel()
                            game.play()

                        elif user_1 == "easy" and user_2 in ["user", "human"] and starting_symbol == "X":
                            from half_random_gameplayOhumanXfirst import GameX

                            game = GameX()
                            game.play()

                        elif user_1 in ["user", "human"] and user_2 == "easy" and starting_symbol == "X":
                            from half_random_gameplayXhumanXfirst import GameX

                            game = GameX()
                            game.play()

                        elif user_1 in ["user", "human"] and user_2 in ["user", "human"] and starting_symbol == "X":
                            from gameX import GameX

                            game = GameX()
                            game.play()

                        elif user_1 == "easy" and user_2 in ["medium", "mid"] and starting_symbol == "X":
                            from gameX_easy_Omid_XFirst import GameXRandomMidLevel

                            game = GameXRandomMidLevel()
                            game.play()

                        elif user_1 == "hard" and user_2 == "easy" and starting_symbol == "X":
                            from gameO_easy_X_Hard_XFirst import GameXRandomMidLevel

                            game = GameXRandomMidLevel()
                            game.play()

                        elif user_1 in ["medium", "mid"] and user_2 in ["user", "human"] and starting_symbol == "O":
                            from game_O_medium_X_human_O_first import GameXHumanOComputerOFirstMidLevel

                            game = GameXHumanOComputerOFirstMidLevel()
                            game.play()

                        elif user_1 in ["medium", "mid"] and user_2 in ["user", "human"] and starting_symbol == "X":
                            from game_X_medium_O_human_X_first import GameOHumanXComputerXFirstMidLevel

                            game = GameOHumanXComputerXFirstMidLevel()
                            game.play()

                        elif user_1 in ["human", "user"] and user_2 == "hard" and starting_symbol == "X":
                            from game_X_human_O_hard_X_first import GameXHumanOComputerXFirstHardLevel

                            game = GameXHumanOComputerXFirstHardLevel()
                            game.play()

                        elif user_1 in ["human", "user"] and user_2 == "hard" and starting_symbol == "O":
                            from game_O_human_X_hard_O_first import GameOHumanXComputerOFirstHardLevel

                            game = GameOHumanXComputerOFirstHardLevel()
                            game.play()

                        elif user_1 == "hard" and user_2 in ["human", "user"] and starting_symbol == "O":
                            from game_X_human_O_hard_O_first import GameXHumanOComputerOFirstHardLevel

                            game = GameXHumanOComputerOFirstHardLevel()
                            game.play()

                        elif user_1 in ["mid", "medium"] and user_2 == "easy" and starting_symbol == "X":
                            from game_X_mid_O_easy_X_first import GameXMid

                            game = GameXMid()
                            game.play()

                        elif user_1 in ["mid", "medium"] and user_2 == "easy" and starting_symbol == "O":
                            from game_O_mid_X_easy_O_first import GameOMid

                            game = GameOMid()
                            game.play()

                        elif user_1 in ["mid", "medium"] and user_2 in ["mid", "medium"] and starting_symbol == "X":
                            from game_X_MidMid import GameXMid

                            game = GameXMid()
                            game.play()

                        elif user_1 in ["mid", "medium"] and user_2 in ["mid", "medium"] and starting_symbol == "O":
                            from game_O_MidMid import GameOMid

                            game = GameOMid()
                            game.play()

                        elif user_1 == "easy" and user_2 == "hard" and starting_symbol == "O":
                            from game_O_easy_X_hard_O_first import GameXHardOEasy

                            game = GameXHardOEasy()
                            game.play()

                        elif user_1 == "easy" and user_2 == "hard" and starting_symbol == "X":
                            from game_X_easy_O_hard_X_first import GameOHardXEasy

                            game = GameOHardXEasy()
                            game.play()

                        elif user_1 in ["medium", "mid"] and user_2 == "hard" and starting_symbol == "X":
                            from game_X_mid_O_hard_X_first import GameXMidOHard

                            game = GameXMidOHard()
                            game.play()

                        elif user_1 == "hard" and user_2 in ["medium", "mid"] and starting_symbol == "O":
                            from game_O_hard_X_mid_O_first import GameOHardXMid

                            game = GameOHardXMid()
                            game.play()

                        elif user_1 == "hard" and user_2 == "hard" and starting_symbol == "X":
                            from game_X_HardHard import GameXHard

                            game = GameXHard()
                            game.play()

                        elif user_1 == "hard" and user_2 == "hard" and starting_symbol == "O":
                            from game_O_HardHard import GameOHard

                            game = GameOHard()
                            game.play()

                elif input_commands == "exit":
                    return

        elif input_commands == "exit":
            return


if __name__ == '__main__':
    main()
