# game_XComputer_OHuman_XFirst_X_HardLevel.py

from typing import Dict, Tuple

from tictactoe import TicTacToeHalfRandomOHumanXComputer


class GameXComputerOHumanXFirstMidLevel:
    """
    The main game logic. With "X" playing first.
    The human plays with "O" and the coordinates are drawn pseudorandomly for
    "X".
    """

    # The phrase variable instantiated to an empty string.

    phrase: str = ''

    def play(self) -> None:
        """
        The "empty" string (an 9-element-long string consisting of "_") is
        instantiated as the new_O_cells. The "empty" dictionary is instantiated as
        the new_O_dict - to set it apart from the "O" that will be placed first
        and to plug it into the loop. The loop uses the counter variable to direct
        the player activity to the relevant blocks of the game. The even counter
        values the program activity towards pseudorandomly drawing in the
        coordinates for the new "X" (the X_field_without_checking method), the
        uneven counter values - directs the player's activity towards typing in
        the new "O" coordinates (the O_field_without_checking method). The values
        of the counter greater or equal to 4 are directed towards the block with
        the checking of the game results. There, the string plugged into the
        method checking_the_game_result may resolve to 4 outcomes - 3 of them end
        in the resolution of the game with the printout of the result - "X wins",
        "O wins" (when player with "X" or "O" wins, respectively) and "Draw"
        - when there is no win. The string may also resolve to "Game not finished"
        - the game then continues.

        :param self: An instance of the class.
        :type self: An instance of the class.
        :return: None. Example: None.
        :rtype: None
        """

        # Let's make available what was made beforehand and
        # instantiate an instance of TicTacToe class.

        tic_tac_toe = TicTacToeHalfRandomOHumanXComputer()

        # The helper coordinates printed:

        print("Coordinates of the cells:")
        print(tic_tac_toe.cell_coordinates())

        # The empty field printed:

        print(tic_tac_toe.empty_field()[0])

        # The counter variable set to 0.

        counter: int = 0

        new_X_dict: Dict[Tuple[int, int], str] = dict()
        new_X_cells: str = ''

        new_O_cells: str = tic_tac_toe.empty_field()[1]
        new_O_dict: Dict[Tuple[int, int], str] = tic_tac_toe.print_matrix_cells(new_O_cells)[1]

        while counter < 8:

            while counter < 4:

                if counter % 2 == 1:

                    new_O_field: str

                    new_O_field, new_O_dict, new_O_cells = \
                        TicTacToeHalfRandomOHumanXComputer.O_field_without_checking(
                            tic_tac_toe,
                            new_X_dict, new_X_cells)

                    print(new_O_field)

                    counter += 1

                elif counter % 2 == 0:

                    new_X_field: str

                    new_X_field, new_X_dict, new_X_cells = \
                        TicTacToeHalfRandomOHumanXComputer.X_field_without_checking_hard_level(
                            tic_tac_toe,
                            new_O_dict, new_O_cells)

                    print(new_X_field)
                    print('Making move level "medium"')

                    counter += 1

            while 4 <= counter < 9:

                if counter % 2 == 1:

                    new_O_field, new_O_dict, new_O_cells = \
                        TicTacToeHalfRandomOHumanXComputer.O_field_without_checking(
                            tic_tac_toe,
                            new_X_dict, new_X_cells)

                    print(new_O_field)

                    phrase = TicTacToeHalfRandomOHumanXComputer.checking_the_game_result(
                        tic_tac_toe, new_O_cells)

                    if phrase == "X wins":

                        print("X wins")
                        return

                    elif phrase == "O wins":

                        print("O wins")
                        return

                    elif phrase == "Draw":

                        print("Draw")
                        return

                    elif phrase == "Game not finished":

                        pass

                    counter += 1

                elif counter % 2 == 0:

                    new_X_field, new_X_dict, new_X_cells = \
                        TicTacToeHalfRandomOHumanXComputer.X_field_without_checking_hard_level(
                            tic_tac_toe,
                            new_O_dict, new_O_cells)

                    print(new_X_field)

                    phrase = TicTacToeHalfRandomOHumanXComputer.checking_the_game_result(
                        tic_tac_toe, new_X_cells)

                    if phrase == "X wins":

                        print("X wins")
                        return

                    elif phrase == "O wins":

                        print("O wins")
                        return

                    elif phrase == "Draw":

                        print("Draw")
                        return

                    elif phrase == "Game not finished":

                        pass

                    counter += 1
