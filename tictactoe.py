# File tictactoe.py:

from typing import Dict, Tuple, List, Any
from random import randint, choice


class TicTacToe:
    """
    The class TicTacToe contains the main game logic
    subdivided into the methods. The current game settings
    allow to play the 3x3 classing Tic-Tac-Toe console game
    by 2 players.
    """

    def cell_coordinates(self) -> str:
        """
        The helper function which returns the matrix view of
        the game coordinates.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: The matrix view of the coordinates.
        Example:
        "------------------------\n| {(1, 3)} {(2, 3)} {(3, 3)} |\n| {(1, 2)}
         "{(2, 2)} {(3, 2)} |\n| {(1, 1)} {(2, 1)}
         "{(3, 1)} |\n------------------------"

        :rtype: str
        """

        c1: Tuple[int, int] = (1, 3)
        c2: Tuple[int, int] = (2, 3)
        c3: Tuple[int, int] = (3, 3)
        c4: Tuple[int, int] = (1, 2)
        c5: Tuple[int, int] = (2, 2)
        c6: Tuple[int, int] = (3, 2)
        c7: Tuple[int, int] = (1, 1)
        c8: Tuple[int, int] = (2, 1)
        c9: Tuple[int, int] = (3, 1)
        return f"------------------------\n| {c1} {c2} {c3} |\n| {c4} {c5} {c6} |\n| {c7} {c8} {c9} |\n------------------------"

    def empty_field(self) -> Tuple[str, str]:
        """
        Returns the view of an "empty" 3x3 matrix , and the accessory,
        "empty" (composed of 9 "_" elements) string.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: The tuple of 2 strings: string[0] is the view of the
        empty matrix, string[1] is the 9-element-long, accessory, "empty" string.
        Example: ("---------\n| _ _ _ |\n| _ _ _ |\n| _ _ _ |\n---------",
         "_________")
        :rtype: Tuple[str, str]
        """

        c1: str
        c2: str
        c3: str
        c4: str
        c5: str
        c6: str
        c7: str
        c8: str
        c9: str

        c1 = c2 = c3 = c4 = c5 = c6 = c7 = c8 = c9 = '_'
        new_f_string: str = f'{c1}{c2}{c3}{c4}{c5}{c6}{c7}{c8}{c9}'

        return f"---------\n| {c1} {c2} {c3} |\n| {c4} {c5} {c6} |\n| {c7} {c8} {c9} |\n---------", new_f_string

    def checking_the_game_result(self, cells: str) -> str:
        """
        The method, having received the string (the combination of 9 symbols:
        "X", "_", and "O" in varying degrees) analyzes it for the 5 possible
        results. The conditional evaluation to True in the 3 of "if/elif" blocks
        may result in termination of the game with the printout of the final
        result, which may be - "X wins", "O wins" or "Draw" (when there is no
        win). When the conditional evaluation evaluates to True in the "Game not
        finished" block - the player is only prompted to enter the another
        pair of the coordinates. The conditional evaluation of the passed-in
        string to "Impossible" is also feasible. However, under the current game
        setting rather unreachable.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param cells: The 9-elements-long string composed of 3 symbols ("X", "O",
         "_") in varying degrees.
        Example: "OXXOOXXOX".
        :type cells: str
        :returns: The evaluation of the actual game progress/result - "O wins",
        "X wins", "Draw", "Game not finished" or "Impossible" (disabled in the
        current game settings). Example: "Game not finished".
        :rtype: str
        """

        c1: str = cells[0]
        c2: str = cells[1]
        c3: str = cells[2]
        c4: str = cells[3]
        c5: str = cells[4]
        c6: str = cells[5]
        c7: str = cells[6]
        c8: str = cells[7]
        c9: str = cells[8]

        # The who_wins_phrase string instantiated to an empty string.

        who_wins_phrase: str = ''

        # X wins:

        if c1 == c2 == c3 == "X" or \
                c4 == c5 == c6 == "X" or \
                c7 == c8 == c9 == "X" or \
                c1 == c4 == c7 == "X" or \
                c2 == c5 == c8 == "X" or \
                c3 == c6 == c9 == "X" or \
                c1 == c5 == c9 == "X" or \
                c7 == c5 == c3 == "X":
            who_wins_phrase = "X wins"

        # O wins:

        elif c1 == c2 == c3 == "O" or \
                c4 == c5 == c6 == "O" or \
                c7 == c8 == c9 == "O" or \
                c1 == c4 == c7 == "O" or \
                c2 == c5 == c8 == "O" or \
                c3 == c6 == c9 == "O" or \
                c1 == c5 == c9 == "O" or \
                c7 == c5 == c3 == "O":
            who_wins_phrase = "O wins"

        # Game not finished:

        elif (c1 != c2 == c3 or
              c1 == c2 != c3 or
              c1 != c2 != c3 or
              c1 == c2 == c3 == '_') \
                and (c4 != c5 == c6 or
                     c4 == c5 != c6 or
                     c4 != c5 != c6 or
                     c4 == c5 == c6 == '_') \
                and (c7 != c8 == c9 or
                     c7 == c8 != c9 or
                     c7 != c8 != c9 or
                     c7 == c8 == c9 == '_') \
                and (c1 != c4 == c7 or
                     c1 == c4 != c7 or
                     c1 != c4 != c7 or
                     c1 == c4 == c7 == '_') \
                and (c2 != c5 == c8 or
                     c2 == c5 != c8 or
                     c2 != c5 != c8 or
                     c2 == c5 == c8 == '_') \
                and (c3 != c6 == c9 or
                     c3 == c6 != c9 or
                     c3 != c6 != c9 or
                     c3 == c6 == c9 == '_') \
                and (c1 != c5 == c9 or
                     c1 == c5 != c9 or
                     c1 != c5 != c9 or
                     c1 == c5 == c9 == '_') \
                and (c7 != c5 == c3 or
                     c7 != c5 != c3 or
                     c7 == c5 != c3 or
                     c7 == c5 == c3 == '_') \
                and ("_" in cells):
            who_wins_phrase = "Game not finished"

        # Draw:

        elif (
                not ((not (c1 == c3 != c2 or
                           c1 == c2 != c3 or
                           c1 != c2 == c3 or
                           c1 != c2 != c3) or
                      not (c4 != c5 == c6 or
                           c4 == c5 != c6 or
                           c4 != c5 != c6 or
                           c4 == c6 != c6)) or
                     not (c7 != c8 == c9 or
                          c7 == c8 != c9 or
                          c7 == c9 != c8 or
                          c7 != c8 != c9))
                and (c1 != c4 == c7 or
                     c1 == c4 != c7 or
                     c1 == c7 != c4 or
                     c1 != c4 != c7)
                and (not (not (c2 != c5 == c8)
                          and not (c2 == c5 != c8)
                          and not (c2 == c8 != c5)) or
                     c2 != c5 != c8)
                and (c3 != c6 == c9 or
                     c3 == c6 != c9 or
                     c3 == c9 != c6 or
                     c3 != c6 != c9)
                and (c1 != c5 == c9 or
                     c1 == c5 != c9 or
                     c1 == c9 != c5 or
                     c1 != c5 != c9)
                and (c7 != c5 == c3 or
                     c7 == c5 != c3 or
                     c7 == c3 != c5 or
                     c7 != c5 != c3)) \
                and (not "_" in cells):
            who_wins_phrase = "Draw"

        # Impossible:

        elif (
                (
                        c1 == c2 == c3 == "X" or
                        c4 == c5 == c6 == "X" or
                        c7 == c8 == c9 == "X" or
                        c1 == c4 == c7 == "X" or
                        c2 == c5 == c8 == "X" or
                        c3 == c6 == c9 == "X" or
                        c1 == c5 == c9 == "X" or
                        c7 == c5 == c3 == "X"
                )
                and (
                        c1 == c2 == c3 == "O" or
                        c4 == c5 == c6 == "O" or
                        c7 == c8 == c9 == "O" or
                        c1 == c4 == c7 == "O" or
                        c2 == c5 == c8 == "O" or
                        c3 == c6 == c9 == "O" or
                        c1 == c5 == c9 == "O" or
                        c7 == c5 == c3 == "O"
                )
        ):
            who_wins_phrase = "Impossible"
        return who_wins_phrase

    def O_field_without_checking(
            self, new_X_dict: Dict[Tuple[int, int], str],
            new_X_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "X" previously
        positioned. Asks the player for the new coordinates for the "O" placement.
        Checks, if the player entered numbers. If not, it prints the information
        and asks for the correct coordinates. Checks, if the both coordinates ∈
         <1, 3>. If not, it prints the information and asks for the correct
        coordinates. Checks, if the chosen place is already occupied (uses the
        provided dictionary). If that's the case, prints the information and asks
        for the other coordinates. Checks the last time if the chosen place is
        free and if that's the case, it returns the new dictionary, the new
        printable matrix and the new string with the newly positioned "O".
        In the further iteration of the class the player's behaviour is overridden
        by the pseudorandom drawing of the coordinates from the prepopulated
        list.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_X_dict: The dictionary with the keys of the 2-integer tuples
         and the values of the cells of the matrix with the newest
         previous addition of "X". Example: {(2, 1): "O", (2, 2): "O", ...}.
        :type new_X_dict: Dict[Tuple[int, int], str]
        :param new_X_cells: The 9-element-long string composed of "X", "O" and "_"
         with the newest previous addition of "X". Example: "OX_OOXXOX".
        :type new_X_cells: str
        :returns: The tuple of 2 strings and a dictionary: (0) the new printable
         matrix with the newly positioned "O" according to the coordinates typed
         in, (1) the new dictionary with the newly positioned "O" according to the
         coordinates typed in, (2) the new string with a newly positioned "O".
         Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.enter_the_coordinates()

        while not self.check_if_coordinates_int(
                coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.enter_the_coordinates()

        while not self.check_if_coordinates_in_range(
                coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.enter_the_coordinates()

        while not self.check_if_cell_occupied(
                new_X_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.enter_the_coordinates()

        if self.check_if_cell_occupied(
                new_X_dict, coordinates[0], coordinates[1]):
            new_O_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0],
                coordinates[1])[1]

            new_O_field: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0],
                coordinates[1])[0]

            new_O_cells: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0],
                coordinates[1])[2]

            return new_O_field, new_O_dict, new_O_cells

    def X_field_without_checking(
            self, new_O_dict: Dict[Tuple[int, int], str],
            new_O_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "O" previously
        positioned. Asks the player for the coordinates for the "X" placement.
        Checks if the player entered numbers. If not, it prints the information
        and asks for the correct coordinates. Checks if the both coordinates ∈
        <1, 3>. If not, it prints the information and asks for the other
        coordinates. Checks, if the chosen place is already occupied (uses the
        provided dictionary). If that's the case, it prints the information and
        asks for the other coordinates. Checks the last time if the chosen place
        is free and if that's the case, it returns the new dictionary, the new
        printable matrix and the new string with the newly positioned "X". In
        the further iteration of the class the player's behaviour is overridden
        by the pseudorandom drawing of the coordinates from the prepopulated
        list.


        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_O_dict: The dictionary with the keys of the 2-integer tuples
         and the values of the cells of the matrix with the newest
         previous addition of "O". Example: new_O_dict = {(3,1) : "O", (2,1): "_",
         ...}.
        :type new_O_dict: Dict[Tuple[int, int], str]
        :param new_O_cells: The string composed of "X", "O" and "_" with newest
        previous addition of "O". Example: "OX_OOXXOX".
        :type new_O_cells: str
        :returns: The tuple of 2 string and 1 dictionary: (0) the new printable
        matrix with a newly positioned "X" according to the coordinates typed in,
        (1) the new dictionary with a newly positioned "X" according to the
        coordinates typed in, (2) the new string with a newly positioned "X".
        Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
        {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
        "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.enter_the_coordinates()

        while not self.check_if_coordinates_int(coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.enter_the_coordinates()

        while not self.check_if_coordinates_in_range(coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.enter_the_coordinates()

        while not self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.enter_the_coordinates()

        if self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            new_X_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[1]

            new_X_field: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[0]

            new_X_cells: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[2]

            return new_X_field, new_X_dict, new_X_cells

    def print_matrix_cells(
            self, cells: str) -> Tuple[str, Dict[Tuple[int, int], str]]:
        """
        Having the string passed in, the method returns the printable
        matrix and the corresponding dictionary.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param cells: The 9-element-long string of alternating "X"/"O"/"_" values.
        Examples: "_________", "_X__X__O_".
        :type cells: str
        :returns: The tuple of 2 strings and a dictionary: (0) the printable
         matrix view of the string passed in and (1) the dictionary of the key (
         the tuple of 2 integers = the coordinates) : value ("X", "O" or "_")
         pairs. Example: ("---------\n| _ _ _ |\n| _ _ _ |\n| _ _ _ |\n---------",
         {(1, 3): "_", (2, 1): "_", (3, 3): "_", ...},
         "_________").
        :rtype: Tuple[str, Dict[Tuple[int, int]]
        """

        new_dict: Dict[Tuple[int, int], str] = dict()

        c1: str
        c2: str
        c3: str
        c4: str
        c5: str
        c6: str
        c7: str
        c8: str
        c9: str

        new_dict[(1, 3)] = c1 = cells[0]
        new_dict[(2, 3)] = c2 = cells[1]
        new_dict[(3, 3)] = c3 = cells[2]
        new_dict[(1, 2)] = c4 = cells[3]
        new_dict[(2, 2)] = c5 = cells[4]
        new_dict[(3, 2)] = c6 = cells[5]
        new_dict[(1, 1)] = c7 = cells[6]
        new_dict[(2, 1)] = c8 = cells[7]
        new_dict[(3, 1)] = c9 = cells[8]

        return f"---------\n| {c1} {c2} {c3} |\n| {c4} {c5} {c6} |\n| {c7} {c8} {c9} |\n---------", new_dict

    def print_matrix_cells_new_X(
            self, cells: str, first_coordinate: int,
            second_coordinate: int) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Having the string and a pair of coordinates passed in, the method
        returns a new matrix[0], a new string[2] and a new corresponding
        dictionary[1] with the "X" newly positioned according to the
        coordinates passed in.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param cells: The 9-element-long string of alternating "X"/"O"/"_"
        values. Example "OX_OOXXOX".
        :type cells: str
        :param first_coordinate: The first coordinate of the pair of integers.
        An integer - 1, 2, or 3. Example: 1.
        :type first_coordinate: int
        :param second_coordinate: The second coordinate of the pair of in integers.
        An integer - 1, 2 or 3. Example: 2.
        :type second_coordinate: int
        :returns: The tuple of 2 strings and a dictionary: (0) the printable
         matrix view of the new string with the "X" inserted according to the
         coordinates passed in, (1) the dictionary of the key ( the tuple of
         2 integers - the coordinates) : value ("X", "O" or "_") pairs and the new
         string (2) with the "X" inserted acc. to the coordinates provided.
         Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str]
        """

        new_dict: Dict[Tuple[int, int], str] = dict()

        new_dict[(1, 3)] = cells[0]
        new_dict[(2, 3)] = cells[1]
        new_dict[(3, 3)] = cells[2]
        new_dict[(1, 2)] = cells[3]
        new_dict[(2, 2)] = cells[4]
        new_dict[(3, 2)] = cells[5]
        new_dict[(1, 1)] = cells[6]
        new_dict[(2, 1)] = cells[7]
        new_dict[(3, 1)] = cells[8]

        new_dict[(first_coordinate, second_coordinate)] = 'X'

        new_f_string: str = f'{new_dict[(1, 3)]}{new_dict[(2, 3)]}{new_dict[(3, 3)]}' \
                            f'{new_dict[(1, 2)]}{new_dict[(2, 2)]}{new_dict[(3, 2)]}{new_dict[(1, 1)]}' \
                            f'{new_dict[(2, 1)]}{new_dict[(3, 1)]}'

        return f"---------\n| {new_dict[(1, 3)]} {new_dict[(2, 3)]} {new_dict[(3, 3)]} |\n| {new_dict[(1, 2)]} {new_dict[(2, 2)]} {new_dict[(3, 2)]} |\n| {new_dict[(1, 1)]} {new_dict[(2, 1)]} {new_dict[(3, 1)]} |\n---------", new_dict, new_f_string

    def print_matrix_cells_new_O(
            self, cells: str, first_coordinate: int,
            second_coordinate: int) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Having the string and a pair of coordinates passed in,
        the method returns a new matrix[0], a new string[2]
        and a new dictionary[1] with the newly positioned
        "O" according to the coordinates passed in.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param cells: The 9-element-long string of alternating "X"/"O"/"_" values.
        Example: "OX__OXXOX".
        :type cells: str
        :param first_coordinate: The first coordinate of the pair of integers.
        An integer - 1, 2 or 3. Example: 3.
        :type first_coordinate: int
        :param second_coordinate: The second coordinate of the pair of integers.
        An integer - 1, 2 or 3. Example: 2.
        :returns: The tuple of 2 strings and a dictionary: (0) the printable
         matrix view of the new string with the "O" inserted according to the
         coordinates passed in, (1) the dictionary of the key ( the tuple of 2
         integers - the coordinates) : value ("X", "O" or "_") pairs and the new
         string (2) with the "O" inserted acc. to the coordinates provided.
         Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str]
        """

        new_dict: Dict[Tuple[int, int], str] = dict()

        new_dict[(1, 3)] = cells[0]
        new_dict[(2, 3)] = cells[1]
        new_dict[(3, 3)] = cells[2]
        new_dict[(1, 2)] = cells[3]
        new_dict[(2, 2)] = cells[4]
        new_dict[(3, 2)] = cells[5]
        new_dict[(1, 1)] = cells[6]
        new_dict[(2, 1)] = cells[7]
        new_dict[(3, 1)] = cells[8]

        new_dict[(first_coordinate, second_coordinate)] = 'O'

        new_f_string: str = f'{new_dict[(1, 3)]}{new_dict[(2, 3)]}{new_dict[(3, 3)]}' \
                            f'{new_dict[(1, 2)]}{new_dict[(2, 2)]}{new_dict[(3, 2)]}{new_dict[(1, 1)]}' \
                            f'{new_dict[(2, 1)]}{new_dict[(3, 1)]}'

        return f"---------\n| {new_dict[(1, 3)]} {new_dict[(2, 3)]} {new_dict[(3, 3)]} |\n| {new_dict[(1, 2)]} {new_dict[(2, 2)]} {new_dict[(3, 2)]} |\n| {new_dict[(1, 1)]} {new_dict[(2, 1)]} {new_dict[(3, 1)]} |\n---------", new_dict, new_f_string

    def enter_the_coordinates(self) -> Tuple[int, int]:
        """
        Prompts entering the coordinates by the player in the form of the string
        of the 2 integers separated by a whitespace ("0 0"). If the player will be
        unfortunate to spoil the coordinates at the beginning or/and at the end of
        the string with the accidental whitespaces and symbols an attempt will be
        made to unclutter the string. The string will be splitted into the list
        and the numerical identity of the symbol will be checked. Those positively
        tested will be returned as a tuple of 2 integers. In the further iterations
        of the class the player's behaviour in the method is overridden by the
        pseudorandom drawing of the coordinates from the prepopulated list.


        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: A tuple of 2 integers processed from the user input.
        An integer in a pair may be 1, 2, 3. Example: (1, 1).
        rtype: Tuple[int, int]

        """

        try:

            coordinates: str = input("Enter the coordinates: '0 0' > ")
            coordinates: str = coordinates.strip()
            coordinates_splited: List[str] = coordinates.split(" ")
            new_coordinates: List[int] = []

            coordinate: str

            for coordinate in coordinates_splited:
                coordinate: str = coordinate.strip()
                if coordinate.isdigit():
                    coordinate_int: int = int(coordinate)
                    new_coordinates.append(coordinate_int)
            return new_coordinates[0], new_coordinates[1]

        except IndexError:

            print("You had IndexError. Try again.")
            coordinates = input("Enter the coordinates: '0 0' > ")
            coordinates = coordinates.strip()
            coordinates_splited = coordinates.split(" ")
            new_coordinates = []
            for coordinate in coordinates_splited:
                coordinate = coordinate.strip()
                if coordinate.isdigit():
                    coordinate_int = int(coordinate)
                    new_coordinates.append(coordinate_int)
            return new_coordinates[0], new_coordinates[1]

    def check_if_coordinates_int(self, first_coordinate: int,
                                 second_coordinate: int) -> bool:
        """
        Returns False if the first or the second coordinate is not an instance of
        an integer.
        Otherwise returns True.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param first_coordinate: The first coordinate of the pair.
        An integer - 1, 2 or 3. Example: 1.
        :type first_coordinate: int
        :param second_coordinate: The second coordinate of the pair.
        An integer - 1, 2 or 3. Example: 2.
        :type second_coordinate: int
        :returns: False if the first or the second coordinate is not an instance
         of an integer. Otherwise returns True. Example: False.
        :rtype: bool
        """

        if not isinstance(first_coordinate, int) or \
                not isinstance(second_coordinate, int):
            return False

        return True

    def check_if_coordinates_in_range(self, first_coordinate: int,
                                      second_coordinate: int) -> bool:
        """
        Returns False if the first or the second coordinate ∉ <1, 3>
        Otherwise returns True.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param first_coordinate: The first coordinate of the pair.
        An integer - 1, 2 or 3. Example: 2.
        :type first_coordinate: int
        :param second_coordinate: The second coordinate of the pair.
        An integer - 1, 2 or 3. Example: 3.
        :type second_coordinate: int
        :returns: False if the first or the second coordinate ∉ <1, 3>.
        Otherwise returns True. Example: True.
        :rtype: bool
        """

        if first_coordinate not in [1, 2, 3] \
                or second_coordinate not in [1, 2, 3]:
            return False

        return True

    def check_if_cell_occupied(
            self, new_dict: Dict[Tuple[int, int], str], first_coordinate: int,
            second_coordinate: int) -> bool:
        """
        Returns False if dictionary under the (first_coordinate,
        second_coordinate) key is not empty (has other value than "_").
        Otherwise returns True.


        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_dict: The dictionary passed in.
        Example: new_dict: {(1,1): "X", (1,2): "_", ...}.
        :type new_dict: Dict[Tuple[int, int], str]
        :param first_coordinate: The first coordinate of the pair
        (first part of the key) An integer - 1, 2 or 3. Example: 3.
        :type first_coordinate: int
        :param second_coordinate: The second coordinate of the pair
        (second part of the key). An integer - 1, 2 or 3. Example: 2.
        :type second_coordinate: int
        :returns: False if dictionary value under the key (first_coordinate,
        second_coordinate) is not empty (not a "_" value). Otherwise returns True.
        Example: False.
        :rtype: bool
        """
        if new_dict[(first_coordinate, second_coordinate)] != '_':
            return False

        return True


class TicTacToeRandom(TicTacToe):
    """
    The class TicTacRandom contains the main game logic
    subdivided into the methods. The current game settings
    allow to play the 3x3 classing Tic-Tac-Toe console
    game automatically for the building and testing purposes
    using the "X" and "O" symbols.
    """

    # The empty list for the coordinates, which already had been drawn from
    # the list.

    def __init__(self):
        self.coordinates_already_drawn: List[str] = []

    def checking_the_game_result_partial_win_put_X_hard_level(self, cells: str) -> Tuple[int, int]:
        """
        The method, having received the string (the combination of 9 symbols:
        "X", "_", and "O" in varying degree) analyzes it for the 5 possible
        results. The conditional evaluation to True in the 3 of "if/elif" blocks
        may result in termination of the game with the printout of the final
        result, which may be - "X wins", "O wins" or "Draw" - when there is no
        win. When the conditional evaluation evaluates to True in the "Game not
        finished" block - the player is only prompted to enter the another
        pair of the coordinates. The conditional evaluation of the passed-in
        string to "Impossible" is also feasible. However, under the current game
        setting rather unreachable.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param cells: The 9-elements-long string composed of 3 symbols ("X", "O",
         "_") in varying degrees.
        Example: "OXXOOXXOX".
        :type cells: str
        :returns: The evaluation of the actual game progress/result - "O wins",
        "X wins", "Draw", "Game not finished" or "Impossible" (disabled in the
        current game settings). Example: "Game not finished".
        :rtype: str
        """

        c1: str = cells[0]
        c2: str = cells[1]
        c3: str = cells[2]
        c4: str = cells[3]
        c5: str = cells[4]
        c6: str = cells[5]
        c7: str = cells[6]
        c8: str = cells[7]
        c9: str = cells[8]

        # X wins:

        count_empty_cells: int = cells.count("_")
        if count_empty_cells == 9:
            random_choice: Tuple[int, int] = choice([(1, 1), (3, 3), (1, 3), (2, 2), (3, 1)])
            self.coordinates_already_drawn.append(f"{random_choice[0]} {random_choice[1]}")
            return random_choice
        if count_empty_cells == 8:
            if c1 == "O" or c3 == "O" or c2 == "O" or c4 == "O" or c6 == "O" or c7 == "O" or c8 == "O" or c9 == "O":
                self.coordinates_already_drawn.append("2 2")
                return (2, 2)
            elif c5 == "O":
                random_choice: Tuple[int, int] = choice([(1, 1), (3, 3), (1, 3), (3, 1)])
                self.coordinates_already_drawn.append(f"{random_choice[0]} {random_choice[1]}")
                return random_choice
        if count_empty_cells == 7:
            if c1 == "X" and c9 == "_":
                self.coordinates_already_drawn.append("3 1")
                return (3, 1)
            elif c3 == "X" and c7 == "_":
                self.coordinates_already_drawn.append("1 1")
                return (1, 1)
            elif c7 == "X" and c3 == "_":
                self.coordinates_already_drawn.append("3 3")
                return (3, 3)
            elif c9 == "X" and c1 == "_":
                self.coordinates_already_drawn.append("1 3")
                return (1, 3)
            elif c1 == "O" and c5 == "_":
                self.coordinates_already_drawn.append("2 2")
                return (2, 2)
            elif c3 == "O" and c5 == "_":
                self.coordinates_already_drawn.append("2 2")
                return (2, 2)
            elif c7 == "O" and c5 == "_":
                self.coordinates_already_drawn.append("2 2")
                return (2, 2)
            elif c9 == "O" and c5 == "_":
                self.coordinates_already_drawn.append("2 2")
                return (2, 2)
            elif c5 != "_":
                if c1 == "_":
                    self.coordinates_already_drawn.append("1 3")
                    return (1, 3)
                elif c3 == "_":
                    self.coordinates_already_drawn.append("3 3")
                    return (3, 3)
                elif c7 == "_":
                    self.coordinates_already_drawn.append("1 1")
                    return (1, 1)
                elif c9 == "_":
                    self.coordinates_already_drawn.append("3 1")
                    return (3, 1)
        if c1 == c2 == "X" and c3 == "_":
            return (3, 3)
        elif c1 == c3 == "X" and c2 == "_":
            return (2, 3)
        elif c2 == c3 == "X" and c1 == "_":
            return (1, 3)
        elif c4 == c5 == "X" and c6 == "_":
            return (3, 2)
        elif c4 == c6 == "X" and c5 == "_":
            return (2, 2)
        elif c5 == c6 == "X" and c4 == "_":
            return (1, 2)
        elif c7 == c8 == "X" and c9 == "_":
            return (3, 1)
        elif c7 == c9 == "X" and c8 == "_":
            return (2, 1)
        elif c8 == c9 == "X" and c7 == "_":
            return (1, 1)
        elif c1 == c4 == "X" and c7 == "_":
            return (1, 1)
        elif c1 == c7 == "X" and c4 == "_":
            return (1, 2)
        elif c4 == c7 == "X" and c1 == "_":
            return (1, 3)
        elif c2 == c5 == "X" and c8 == "_":
            return (2, 1)
        elif c2 == c8 == "X" and c5 == "_":
            return (2, 2)
        elif c5 == c8 == "X" and c2 == "_":
            return (2, 3)
        elif c3 == c6 == "X" and c9 == "_":
            return (3, 1)
        elif c3 == c9 == "X" and c6 == "_":
            return (3, 2)
        elif c6 == c9 == "X" and c3 == "_":
            return (3, 3)
        elif c1 == c5 == "X" and c9 == "_":
            return (3, 1)
        elif c1 == c9 == "X" and c5 == "_":
            return (2, 2)
        elif c5 == c9 == "X" and c1 == "_":
            return (1, 3)
        elif c7 == c5 == "X" and c3 == "_":
            return (3, 3)
        elif c7 == c3 == "X" and c5 == "_":
            return (2, 2)
        elif c5 == c3 == "X" and c7 == "_":
            return (1, 1)
        elif c1 == c2 == "O" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c1 == c3 == "O" and c2 == "_":
            self.coordinates_already_drawn.append("2 3")
            return (2, 3)
        elif c2 == c3 == "O" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c4 == c5 == "O" and c6 == "_":
            self.coordinates_already_drawn.append("3 2")
            return (3, 2)
        elif c4 == c6 == "O" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c6 == "O" and c4 == "_":
            self.coordinates_already_drawn.append("1 2")
            return (1, 2)
        elif c7 == c8 == "O" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c7 == c9 == "O" and c8 == "_":
            self.coordinates_already_drawn.append("2 1")
            return (2, 1)
        elif c8 == c9 == "O" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        elif c1 == c4 == "O" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        elif c1 == c7 == "O" and c4 == "_":
            self.coordinates_already_drawn.append("1 2")
            return (1, 2)
        elif c4 == c7 == "O" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c2 == c5 == "O" and c8 == "_":
            self.coordinates_already_drawn.append("2 1")
            return (2, 1)
        elif c2 == c8 == "O" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c8 == "O" and c2 == "_":
            self.coordinates_already_drawn.append("2 3")
            return (2, 3)
        elif c3 == c6 == "O" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c3 == c9 == "O" and c6 == "_":
            self.coordinates_already_drawn.append("3 2")
            return (3, 2)
        elif c6 == c9 == "O" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c1 == c5 == "O" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c1 == c9 == "O" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c9 == "O" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c7 == c5 == "O" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c7 == c3 == "O" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c3 == "O" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        else:
            return self.enter_the_coordinates()

    def checking_the_game_result_partial_win_put_O_hard_level(self, cells: str) -> Tuple[int, int]:
        """
        The method, having received the string (the combination of 9 symbols:
        "X", "_", and "O" in varying degree) analyzes it for the 5 possible
        results. The conditional evaluation to True in the 3 of "if/elif" blocks
        may result in termination of the game with the printout of the final
        result, which may be - "X wins", "O wins" or "Draw" - when there is no
        win. When the conditional evaluation evaluates to True in the "Game not
        finished" block - the player is only prompted to enter the another
        pair of the coordinates. The conditional evaluation of the passed-in
        string to "Impossible" is also feasible. However, under the current game
        setting rather unreachable.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param cells: The 9-elements-long string composed of 3 symbols ("X", "O",
         "_") in varying degrees.
        Example: "OXXOOXXOX".
        :type cells: str
        :returns: The evaluation of the actual game progress/result - "O wins",
        "X wins", "Draw", "Game not finished" or "Impossible" (disabled in the
        current game settings). Example: "Game not finished".
        :rtype: str
        """

        c1: str = cells[0]
        c2: str = cells[1]
        c3: str = cells[2]
        c4: str = cells[3]
        c5: str = cells[4]
        c6: str = cells[5]
        c7: str = cells[6]
        c8: str = cells[7]
        c9: str = cells[8]

        # O wins:

        count_empty_cells: int = cells.count("_")
        if count_empty_cells == 9:
            random_choice: Tuple[int, int] = choice([(1, 1), (3, 3), (1, 3), (2, 2), (3, 1)])
            self.coordinates_already_drawn.append(f"{random_choice[0]} {random_choice[1]}")
            return random_choice
        if count_empty_cells == 8:
            if c1 == "X" or c3 == "X" or c2 == "X" or c4 == "X" or c6 == "X" or c7 == "X" or c8 == "X" or c9 == "X":
                self.coordinates_already_drawn.append("2 2")
                return (2, 2)
            elif c5 == "X":
                random_choice: Tuple[int, int] = choice([(1, 1), (3, 3), (1, 3), (3, 1)])
                self.coordinates_already_drawn.append(f"{random_choice[0]} {random_choice[1]}")
                return random_choice
        if count_empty_cells == 7:
            if c1 == "O" and c9 == "_":
                self.coordinates_already_drawn.append("3 1")
                return (3, 1)
            elif c3 == "O" and c7 == "_":
                self.coordinates_already_drawn.append("1 1")
                return (1, 1)
            elif c7 == "O" and c3 == "_":
                self.coordinates_already_drawn.append("3 3")
                return (3, 3)
            elif c9 == "O" and c1 == "_":
                self.coordinates_already_drawn.append("1 3")
                return (1, 3)
            elif c1 == "X" and c5 == "_":
                self.coordinates_already_drawn.append("2 2")
                return (2, 2)
            elif c3 == "X" and c5 == "_":
                self.coordinates_already_drawn.append("2 2")
                return (2, 2)
            elif c7 == "X" and c5 == "_":
                self.coordinates_already_drawn.append("2 2")
                return (2, 2)
            elif c9 == "X" and c5 == "_":
                self.coordinates_already_drawn.append("2 2")
                return (2, 2)
            elif c5 != "_":
                if c1 == "_":
                    self.coordinates_already_drawn.append("1 3")
                    return (1, 3)
                elif c3 == "_":
                    self.coordinates_already_drawn.append("3 3")
                    return (3, 3)
                elif c7 == "_":
                    self.coordinates_already_drawn.append("1 1")
                    return (1, 1)
                elif c9 == "_":
                    self.coordinates_already_drawn.append("3 1")
                    return (3, 1)
        if c1 == c2 == "O" and c3 == "_":
            return (3, 3)
        elif c1 == c3 == "O" and c2 == "_":
            return (2, 3)
        elif c2 == c3 == "O" and c1 == "_":
            return (1, 3)
        elif c4 == c5 == "O" and c6 == "_":
            return (3, 2)
        elif c4 == c6 == "O" and c5 == "_":
            return (2, 2)
        elif c5 == c6 == "O" and c4 == "_":
            return (1, 2)
        elif c7 == c8 == "O" and c9 == "_":
            return (3, 1)
        elif c7 == c9 == "O" and c8 == "_":
            return (2, 1)
        elif c8 == c9 == "O" and c7 == "_":
            return (1, 1)
        elif c1 == c4 == "O" and c7 == "_":
            return (1, 1)
        elif c1 == c7 == "O" and c4 == "_":
            return (1, 2)
        elif c4 == c7 == "O" and c1 == "_":
            return (1, 3)
        elif c2 == c5 == "O" and c8 == "_":
            return (2, 1)
        elif c2 == c8 == "O" and c5 == "_":
            return (2, 2)
        elif c5 == c8 == "O" and c2 == "_":
            return (2, 3)
        elif c3 == c6 == "O" and c9 == "_":
            return (3, 1)
        elif c3 == c9 == "O" and c6 == "_":
            return (3, 2)
        elif c6 == c9 == "O" and c3 == "_":
            return (3, 3)
        elif c1 == c5 == "O" and c9 == "_":
            return (3, 1)
        elif c1 == c9 == "o" and c5 == "_":
            return (2, 2)
        elif c5 == c9 == "O" and c1 == "_":
            return (1, 3)
        elif c7 == c5 == "O" and c3 == "_":
            return (3, 3)
        elif c7 == c3 == "O" and c5 == "_":
            return (2, 2)
        elif c5 == c3 == "O" and c7 == "_":
            return (1, 1)
        elif c1 == c2 == "X" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c1 == c3 == "X" and c2 == "_":
            self.coordinates_already_drawn.append("2 3")
            return (2, 3)
        elif c2 == c3 == "X" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c4 == c5 == "X" and c6 == "_":
            self.coordinates_already_drawn.append("3 2")
            return (3, 2)
        elif c4 == c6 == "X" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c6 == "X" and c4 == "_":
            self.coordinates_already_drawn.append("1 2")
            return (1, 2)
        elif c7 == c8 == "X" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c7 == c9 == "X" and c8 == "_":
            self.coordinates_already_drawn.append("2 1")
            return (2, 1)
        elif c8 == c9 == "X" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        elif c1 == c4 == "X" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        elif c1 == c7 == "X" and c4 == "_":
            self.coordinates_already_drawn.append("1 2")
            return (1, 2)
        elif c4 == c7 == "X" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c2 == c5 == "X" and c8 == "_":
            self.coordinates_already_drawn.append("2 1")
            return (2, 1)
        elif c2 == c8 == "X" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c8 == "X" and c2 == "_":
            self.coordinates_already_drawn.append("2 3")
            return (2, 3)
        elif c3 == c6 == "X" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c3 == c9 == "X" and c6 == "_":
            self.coordinates_already_drawn.append("3 2")
            return (3, 2)
        elif c6 == c9 == "X" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c1 == c5 == "X" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c1 == c9 == "X" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c9 == "X" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c7 == c5 == "X" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c7 == c3 == "O" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c3 == "X" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        else:
            return self.enter_the_coordinates()

    def checking_the_game_result_partial_win_put_O_mid_level(self, cells: str) -> Tuple[int, int]:
        """
        The method, having received the string (the combination of 9 symbols:
        "X", "_", and "O" in varying degree) analyzes it for the 5 possible
        results. The conditional evaluation to True in the 3 of "if/elif" blocks
        may result in termination of the game with the printout of the final
        result, which may be - "X wins", "O wins" or "Draw" - when there is no
        win. When the conditional evaluation evaluates to True in the "Game not
        finished" block - the player is only prompted to enter the another
        pair of the coordinates. The conditional evaluation of the passed-in
        string to "Impossible" is also feasible. However, under the current game
        setting rather unreachable.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param cells: The 9-elements-long string composed of 3 symbols ("X", "O",
         "_") in varying degrees.
        Example: "OXXOOXXOX".
        :type cells: str
        :returns: The evaluation of the actual game progress/result - "O wins",
        "X wins", "Draw", "Game not finished" or "Impossible" (disabled in the
        current game settings). Example: "Game not finished".
        :rtype: str
        """

        c1: str = cells[0]
        c2: str = cells[1]
        c3: str = cells[2]
        c4: str = cells[3]
        c5: str = cells[4]
        c6: str = cells[5]
        c7: str = cells[6]
        c8: str = cells[7]
        c9: str = cells[8]

        # O wins:

        if c1 == c2 == "O" and c3 == "_":
            return (3, 3)
        elif c1 == c3 == "O" and c2 == "_":
            return (2, 3)
        elif c2 == c3 == "O" and c1 == "_":
            return (1, 3)
        elif c4 == c5 == "O" and c6 == "_":
            return (3, 2)
        elif c4 == c6 == "O" and c5 == "_":
            return (2, 2)
        elif c5 == c6 == "O" and c4 == "_":
            return (1, 2)
        elif c7 == c8 == "O" and c9 == "_":
            return (3, 1)
        elif c7 == c9 == "O" and c8 == "_":
            return (2, 1)
        elif c8 == c9 == "O" and c7 == "_":
            return (1, 1)
        elif c1 == c4 == "O" and c7 == "_":
            return (1, 1)
        elif c1 == c7 == "O" and c4 == "_":
            return (1, 2)
        elif c4 == c7 == "O" and c1 == "_":
            return (1, 3)
        elif c2 == c5 == "O" and c8 == "_":
            return (2, 1)
        elif c2 == c8 == "O" and c5 == "_":
            return (2, 2)
        elif c5 == c8 == "O" and c2 == "_":
            return (2, 3)
        elif c3 == c6 == "O" and c9 == "_":
            return (3, 1)
        elif c3 == c9 == "O" and c6 == "_":
            return (3, 2)
        elif c6 == c9 == "O" and c3 == "_":
            return (3, 3)
        elif c1 == c5 == "O" and c9 == "_":
            return (3, 1)
        elif c1 == c9 == "O" and c5 == "_":
            return (2, 2)
        elif c5 == c9 == "O" and c1 == "_":
            return (1, 3)
        elif c7 == c5 == "O" and c3 == "_":
            return (3, 3)
        elif c7 == c3 == "O" and c5 == "_":
            return (2, 2)
        elif c5 == c3 == "O" and c7 == "_":
            return (1, 1)
        elif c1 == c2 == "X" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c1 == c3 == "X" and c2 == "_":
            self.coordinates_already_drawn.append("2 3")
            return (2, 3)
        elif c2 == c3 == "X" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c4 == c5 == "X" and c6 == "_":
            self.coordinates_already_drawn.append("3 2")
            return (3, 2)
        elif c4 == c6 == "X" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c6 == "X" and c4 == "_":
            self.coordinates_already_drawn.append("1 2")
            return (1, 2)
        elif c7 == c8 == "X" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c7 == c9 == "X" and c8 == "_":
            self.coordinates_already_drawn.append("2 1")
            return (2, 1)
        elif c8 == c9 == "X" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        elif c1 == c4 == "X" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        elif c1 == c7 == "X" and c4 == "_":
            self.coordinates_already_drawn.append("1 2")
            return (1, 2)
        elif c4 == c7 == "X" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c2 == c5 == "X" and c8 == "_":
            self.coordinates_already_drawn.append("2 1")
            return (2, 1)
        elif c2 == c8 == "X" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c8 == "X" and c2 == "_":
            self.coordinates_already_drawn.append("2 3")
            return (2, 3)
        elif c3 == c6 == "X" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c3 == c9 == "X" and c6 == "_":
            self.coordinates_already_drawn.append("3 2")
            return (3, 2)
        elif c6 == c9 == "X" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c1 == c5 == "X" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c1 == c9 == "X" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c9 == "X" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c7 == c5 == "X" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c7 == c3 == "X" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c3 == "X" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        else:
            return self.enter_the_coordinates()

    def checking_the_game_result_partial_win_put_X_mid_level(self, cells: str) -> Tuple[int, int]:
        """
        The method, having received the string (the combination of 9 symbols:
        "X", "_", and "O" in varying degree) analyzes it for the 5 possible
        results. The conditional evaluation to True in the 3 of "if/elif" blocks
        may result in termination of the game with the printout of the final
        result, which may be - "X wins", "O wins" or "Draw" - when there is no
        win. When the conditional evaluation evaluates to True in the "Game not
        finished" block - the player is only prompted to enter the another
        pair of the coordinates. The conditional evaluation of the passed-in
        string to "Impossible" is also feasible. However, under the current game
        setting rather unreachable.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param cells: The 9-elements-long string composed of 3 symbols ("X", "O",
         "_") in varying degrees.
        Example: "OXXOOXXOX".
        :type cells: str
        :returns: The evaluation of the actual game progress/result - "O wins",
        "X wins", "Draw", "Game not finished" or "Impossible" (disabled in the
        current game settings). Example: "Game not finished".
        :rtype: str
        """

        c1: str = cells[0]
        c2: str = cells[1]
        c3: str = cells[2]
        c4: str = cells[3]
        c5: str = cells[4]
        c6: str = cells[5]
        c7: str = cells[6]
        c8: str = cells[7]
        c9: str = cells[8]

        # X wins:

        if c1 == c2 == "X" and c3 == "_":
            return (3, 3)
        elif c1 == c3 == "X" and c2 == "_":
            return (2, 3)
        elif c2 == c3 == "X" and c1 == "_":
            return (1, 3)
        elif c4 == c5 == "X" and c6 == "_":
            return (3, 2)
        elif c4 == c6 == "X" and c5 == "_":
            return (2, 2)
        elif c5 == c6 == "X" and c4 == "_":
            return (1, 2)
        elif c7 == c8 == "X" and c9 == "_":
            return (3, 1)
        elif c7 == c9 == "X" and c8 == "_":
            return (2, 1)
        elif c8 == c9 == "X" and c7 == "_":
            return (1, 1)
        elif c1 == c4 == "X" and c7 == "_":
            return (1, 1)
        elif c1 == c7 == "X" and c4 == "_":
            return (1, 2)
        elif c4 == c7 == "X" and c1 == "_":
            return (1, 3)
        elif c2 == c5 == "X" and c8 == "_":
            return (2, 1)
        elif c2 == c8 == "X" and c5 == "_":
            return (2, 2)
        elif c5 == c8 == "X" and c2 == "_":
            return (2, 3)
        elif c3 == c6 == "X" and c9 == "_":
            return (3, 1)
        elif c3 == c9 == "X" and c6 == "_":
            return (3, 2)
        elif c6 == c9 == "X" and c3 == "_":
            return (3, 3)
        elif c1 == c5 == "X" and c9 == "_":
            return (3, 1)
        elif c1 == c9 == "X" and c5 == "_":
            return (2, 2)
        elif c5 == c9 == "X" and c1 == "_":
            return (1, 3)
        elif c7 == c5 == "X" and c3 == "_":
            return (3, 3)
        elif c7 == c3 == "X" and c5 == "_":
            return (2, 2)
        elif c5 == c3 == "X" and c7 == "_":
            return (1, 1)
        elif c1 == c2 == "O" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c1 == c3 == "O" and c2 == "_":
            self.coordinates_already_drawn.append("2 3")
            return (2, 3)
        elif c2 == c3 == "O" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c4 == c5 == "O" and c6 == "_":
            self.coordinates_already_drawn.append("3 2")
            return (3, 2)
        elif c4 == c6 == "O" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c6 == "O" and c4 == "_":
            self.coordinates_already_drawn.append("1 2")
            return (1, 2)
        elif c7 == c8 == "O" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c7 == c9 == "O" and c8 == "_":
            self.coordinates_already_drawn.append("2 1")
            return (2, 1)
        elif c8 == c9 == "O" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        elif c1 == c4 == "O" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        elif c1 == c7 == "O" and c4 == "_":
            self.coordinates_already_drawn.append("1 2")
            return (1, 2)
        elif c4 == c7 == "O" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c2 == c5 == "O" and c8 == "_":
            self.coordinates_already_drawn.append("2 1")
            return (2, 1)
        elif c2 == c8 == "O" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c8 == "O" and c2 == "_":
            self.coordinates_already_drawn.append("2 3")
            return (2, 3)
        elif c3 == c6 == "O" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c3 == c9 == "O" and c6 == "_":
            self.coordinates_already_drawn.append("3 2")
            return (3, 2)
        elif c6 == c9 == "O" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c1 == c5 == "O" and c9 == "_":
            self.coordinates_already_drawn.append("3 1")
            return (3, 1)
        elif c1 == c9 == "O" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c9 == "O" and c1 == "_":
            self.coordinates_already_drawn.append("1 3")
            return (1, 3)
        elif c7 == c5 == "O" and c3 == "_":
            self.coordinates_already_drawn.append("3 3")
            return (3, 3)
        elif c7 == c3 == "O" and c5 == "_":
            self.coordinates_already_drawn.append("2 2")
            return (2, 2)
        elif c5 == c3 == "O" and c7 == "_":
            self.coordinates_already_drawn.append("1 1")
            return (1, 1)
        else:
            return self.enter_the_coordinates()

    def O_field_without_checking_mid_level(
            self, new_X_dict: Dict[Tuple[int, int], str],
            new_X_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "X" previously
        positioned. Asks for the pseudorandomly chosen coordinates for the "O"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks if the both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-in dictionary). If that's the case, prints the information and
        asks for the other coordinates. Checks the last time if the chosen place
        is free and if that's the case, it returns the new dictionary, the new
        printable matrix and the new string with the newly positioned "O".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_X_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "X".
        Example: new_X_dict ={(1, 1): "X", (1, 2): "_", ...}.
        :type new_X_dict: Dict[Tuple[int, int], str]
        :param new_X_cells: The string composed of "X", "O" and "_" with the
        newest previous addition of "X". Example: "_OX_X_XOO".
        :type new_X_cells: str
        :returns: The tuple of 2 strings and 1 dictionary: (0) the new printable
        matrix with the newly positioned "O" according to the coordinates passed
        in, (1) the new dictionary with the newly positioned "O" according to the
        coordinates passed in, (2) the new string with the newly positioned "O".
        Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.checking_the_game_result_partial_win_put_O_mid_level(new_X_cells)
        print("Making the move level \"mid-hard\"")

        while not self.check_if_coordinates_int(coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.checking_the_game_result_partial_win_put_O_mid_level(new_X_cells)

        while not self.check_if_coordinates_in_range(coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.checking_the_game_result_partial_win_put_O_mid_level(new_X_cells)

        while not self.check_if_cell_occupied(
                new_X_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.checking_the_game_result_partial_win_put_O_mid_level(new_X_cells)

        if self.check_if_cell_occupied(new_X_dict, coordinates[0], coordinates[1]):
            new_O_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[1]

            new_O_field: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[0]

            new_O_cells: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[2]

            return new_O_field, new_O_dict, new_O_cells

    def X_field_without_checking_mid_level(
            self, new_O_dict: Dict[Tuple[int, int], str],
            new_O_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "X" previously
        positioned. Asks for the pseudorandomly chosen coordinates for the "O"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks if the both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-in dictionary). If that's the case, prints the information and
        asks for the other coordinates. Checks the last time if the chosen place
        is free and if that's the case, it returns the new dictionary, the new
        printable matrix and the new string with the newly positioned "O".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_X_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "X".
        Example: new_X_dict ={(1, 1): "X", (1, 2): "_", ...}.
        :type new_X_dict: Dict[Tuple[int, int], str]
        :param new_X_cells: The string composed of "X", "O" and "_" with the
        newest previous addition of "X". Example: "_OX_X_XOO".
        :type new_X_cells: str
        :returns: The tuple of 2 strings and 1 dictionary: (0) the new printable
        matrix with the newly positioned "O" according to the coordinates passed
        in, (1) the new dictionary with the newly positioned "O" according to the
        coordinates passed in, (2) the new string with the newly positioned "O".
        Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.checking_the_game_result_partial_win_put_X_mid_level(new_O_cells)
        print("Making the move level \"mid-hard\"")

        while not self.check_if_coordinates_int(coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.checking_the_game_result_partial_win_put_X_mid_level(new_O_cells)

        while not self.check_if_coordinates_in_range(coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.checking_the_game_result_partial_win_put_X_mid_level(new_O_cells)

        while not self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.checking_the_game_result_partial_win_put_X_mid_level(new_O_cells)

        if self.check_if_cell_occupied(new_O_dict, coordinates[0], coordinates[1]):
            new_X_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[1]

            new_X_field: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[0]

            new_X_cells: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[2]

            return new_X_field, new_X_dict, new_X_cells

    def X_field_without_checking_hard_level(
            self, new_O_dict: Dict[Tuple[int, int], str],
            new_O_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "O" previously
        positioned. Asks for the (pseudorandomly chosen) coordinates for the "X"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks, if both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-im dictionary). If that's the case, it prints the information
        and asks for the other coordinates. Checks the last time if the chosen
        place is free and if that's the case, it returns the new dictionary, the
        new printable matrix and the new string with the newly positioned "X".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_O_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "O". Example: new_O_dict = {(3,1) : "O", (2,1): "_",
         ...}.
        :type new_O_dict: Dict[Tuple[int, int], str]
        :param new_O_cells: The string composed of "X", "O" and "_" with the
         newest previous addition of "O". Example: "_O__X_XOO".
        :type new_O_cells: str
        :returns: The tuple of objects: (0) the new printable matrix with the
         newly positioned "X" according to the coordinates passed in, (1) the new
         dictionary with the newly positioned "X" according to the coordinates
         passed in, (2) the new string with a newly positioned "X".
         Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.checking_the_game_result_partial_win_put_X_hard_level(new_O_cells)
        print("Making the move level \"hard\"")

        while not self.check_if_coordinates_int(
                coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.checking_the_game_result_partial_win_put_X_hard_level(new_O_cells)

        while not self.check_if_coordinates_in_range(
                coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.checking_the_game_result_partial_win_put_X_hard_level(new_O_cells)

        while not self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.checking_the_game_result_partial_win_put_X_hard_level(new_O_cells)

        if self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            new_X_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[1]

            new_X_field: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[0]

            new_X_cells: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[2]

            return new_X_field, new_X_dict, new_X_cells

    def O_field_without_checking_hard_level(
            self, new_X_dict: Dict[Tuple[int, int], str],
            new_X_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "O" previously
        positioned. Asks for the (pseudorandomly chosen) coordinates for the "X"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks, if both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-im dictionary). If that's the case, it prints the information
        and asks for the other coordinates. Checks the last time if the chosen
        place is free and if that's the case, it returns the new dictionary, the
        new printable matrix and the new string with the newly positioned "X".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_O_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "O". Example: new_O_dict = {(3,1) : "O", (2,1): "_",
         ...}.
        :type new_O_dict: Dict[Tuple[int, int], str]
        :param new_O_cells: The string composed of "X", "O" and "_" with the
         newest previous addition of "O". Example: "_O__X_XOO".
        :type new_O_cells: str
        :returns: The tuple of objects: (0) the new printable matrix with the
         newly positioned "X" according to the coordinates passed in, (1) the new
         dictionary with the newly positioned "X" according to the coordinates
         passed in, (2) the new string with a newly positioned "X".
         Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.checking_the_game_result_partial_win_put_O_hard_level(new_X_cells)
        print("Making the move level \"hard\"")

        while not self.check_if_coordinates_int(
                coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.checking_the_game_result_partial_win_put_O_hard_level(new_X_cells)

        while not self.check_if_coordinates_in_range(
                coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.checking_the_game_result_partial_win_put_O_hard_level(new_X_cells)

        while not self.check_if_cell_occupied(
                new_X_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.checking_the_game_result_partial_win_put_O_hard_level(new_X_cells)

        if self.check_if_cell_occupied(
                new_X_dict, coordinates[0], coordinates[1]):
            new_O_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[1]

            new_O_field: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[0]

            new_O_cells: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[2]

            return new_O_field, new_O_dict, new_O_cells

    def enter_the_coordinates_easy(self) -> Tuple[int, int]:
        """
        Chooses pseudorandomly a pair of coordinates
        in a form of a string of 2 integers separated by a
        whitespace. If the string will be spoiled at the
        beginning or/and at the end of the string with the
        accidental whitespaces and/or other symbols an attempt
        will be made to unclutter the string. The string will
        be splitted into the list of presumed digits at the
        whitespace and the numerical identity of the symbol will
        be evaluated. Those which will positively pass the test
        will be returned as a tuple of 2 integers.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: A tuple of integers processed from the pseudorandomly
        chosen string input. Example: (3, 3).
        rtype: Tuple[int, int]

        """

        coordinates: str = self.choose_the_random_coordinates()
        coordinates: str = coordinates.strip()
        coordinates_splited: List[str] = coordinates.split(" ")

        new_coordinates: List[int] = []

        coordinate: str

        for coordinate in coordinates_splited:

            coordinate: str = coordinate.strip()

            if coordinate.isdigit():
                coordinate_int: int = int(coordinate)
                new_coordinates.append(coordinate_int)

        return new_coordinates[0], new_coordinates[1]

    def O_field_without_checking(
            self, new_X_dict: Dict[Tuple[int, int], str],
            new_X_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "X" previously
        positioned. Asks for the pseudorandomly chosen coordinates for the "O"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks if the both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-in dictionary). If that's the case, prints the information and
        asks for the other coordinates. Checks the last time if the chosen place
        is free and if that's the case, it returns the new dictionary, the new
        printable matrix and the new string with the newly positioned "O".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_X_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "X".
        Example: new_X_dict ={(1, 1): "X", (1, 2): "_", ...}.
        :type new_X_dict: Dict[Tuple[int, int], str]
        :param new_X_cells: The string composed of "X", "O" and "_" with the
        newest previous addition of "X". Example: "_OX_X_XOO".
        :type new_X_cells: str
        :returns: The tuple of 2 strings and 1 dictionary: (0) the new printable
        matrix with the newly positioned "O" according to the coordinates passed
        in, (1) the new dictionary with the newly positioned "O" according to the
        coordinates passed in, (2) the new string with the newly positioned "O".
        Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.enter_the_coordinates_easy()
        print("Making the move level \"easy\"")

        while not self.check_if_coordinates_int(coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.enter_the_coordinates_easy()

        while not self.check_if_coordinates_in_range(coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.enter_the_coordinates_easy()

        while not self.check_if_cell_occupied(
                new_X_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.enter_the_coordinates_easy()

        if self.check_if_cell_occupied(new_X_dict, coordinates[0], coordinates[1]):
            new_O_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[1]

            new_O_field: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[0]

            new_O_cells: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[2]

            return new_O_field, new_O_dict, new_O_cells

    def X_field_without_checking(
            self, new_O_dict: Dict[Tuple[int, int], str],
            new_O_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "O" previously
        positioned. Asks for the (pseudorandomly chosen) coordinates for the "X"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks, if both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-im dictionary). If that's the case, it prints the information
        and asks for the other coordinates. Checks the last time if the chosen
        place is free and if that's the case, it returns the new dictionary, the
        new printable matrix and the new string with the newly positioned "X".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_O_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "O". Example: new_O_dict = {(3,1) : "O", (2,1): "_",
         ...}.
        :type new_O_dict: Dict[Tuple[int, int], str]
        :param new_O_cells: The string composed of "X", "O" and "_" with the
         newest previous addition of "O". Example: "_O__X_XOO".
        :type new_O_cells: str
        :returns: The tuple of objects: (0) the new printable matrix with the
         newly positioned "X" according to the coordinates passed in, (1) the new
         dictionary with the newly positioned "X" according to the coordinates
         passed in, (2) the new string with a newly positioned "X".
         Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.enter_the_coordinates_easy()
        print("Making the move level \"easy\"")

        while not self.check_if_coordinates_int(
                coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.enter_the_coordinates_easy()

        while not self.check_if_coordinates_in_range(
                coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.enter_the_coordinates_easy()

        while not self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.enter_the_coordinates_easy()

        if self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            new_X_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[1]

            new_X_field: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[0]

            new_X_cells: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[2]

            return new_X_field, new_X_dict, new_X_cells

    def enter_the_coordinates(self) -> Tuple[int, int]:
        """
        Chooses pseudorandomly a pair of coordinates
        in a form of a string of 2 integers separated by a
        whitespace. If the string will be spoiled at the
        beginning or/and at the end of the string with the
        accidental whitespaces and/or other symbols an attempt
        will be made to unclutter the string. The string will
        be splitted into the list of presumed digits at the
        whitespace and the numerical identity of the symbol will
        be evaluated. Those which will positively pass the test
        will be returned as a tuple of 2 integers.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: A tuple of integers processed from the pseudorandomly
        chosen string input. Example: (3, 3).
        rtype: Tuple[int, int]

        """

        coordinates: str = self.choose_the_random_coordinates()
        coordinates: str = coordinates.strip()
        coordinates_splited: List[str] = coordinates.split(" ")

        new_coordinates: List[int] = []

        coordinate: str

        for coordinate in coordinates_splited:

            coordinate: str = coordinate.strip()

            if coordinate.isdigit():
                coordinate_int: int = int(coordinate)
                new_coordinates.append(coordinate_int)

        return new_coordinates[0], new_coordinates[1]

    def choose_the_random_coordinates(self) -> str:
        """
        Chooses pseudorandomly a pair of coordinates from the list.
        A pair of coordinates can be chosen only once from the list,
        as the chosen coordinate is appended to the all-class
        coordinates-already-chosen list and then checked if they
        accidentally weren't chosen before - if that was the case the
        coordinates are drawn till the unique (those that hadn't  will be chosen
        before) will be chosen.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: A string of coordinates in the format "0 0".
        Example: "2 3".
        :rtype: str
        """

        coordinates_list: List[str] = [
            "1 1", "1 2",
            "1 3", "2 1",
            "2 2", "2 2",
            "2 3", "3 3",
            "3 2", "3 1"
        ]

        random_coordinate: str = coordinates_list[randint(0, len(coordinates_list) - 1)]

        while random_coordinate in self.coordinates_already_drawn:
            random_coordinate = coordinates_list[randint(0, len(coordinates_list) - 1)]

        if random_coordinate not in self.coordinates_already_drawn:
            self.coordinates_already_drawn.append(random_coordinate)

        return random_coordinate


class TicTacToeHalfRandomOHumanXComputer(TicTacToeRandom):
    """
    The class TicTacToeHalfRandomOHumanXComputer contains the main game
    logic subdivided into the methods. The current game settings
    allow to play the 3x3 classic Tic-Tac-Toe console
    game with human playing "O" and the coordinates being pseudorandomly
    drawn fo "X".
    """

    # The empty list for the coordinates, which already had been drawn from
    # the list or typed in by the player.

    def __init__(self):
        self.coordinates_already_drawn: List[str] = []

    def O_field_without_checking(
            self, new_X_dict: Dict[Tuple[int, int], str],
            new_X_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "X" previously
        positioned. Asks for the new coordinates for the "O" placement. Checks if
        the passed-in data are numbers. If not, it prints the information and asks
        for the coordinates again. Checks if the both coordinates ∈ <1, 3>. If
        not, it prints the information and asks for the correct coordinates.
        Checks if the chosen place is already occupied (uses the passed-in
        dictionary). If that's the case, prints the information and asks for the
        other coordinates. Checks for the last time if the chosen place
        is free and if that's the case, it returns the new dictionary, the new
        printable matrix and the new string with the newly positioned "O".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_X_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "X".
        Example: new_X_dict ={(1, 1): "X", (1, 2): "_", ...}.
        :type new_X_dict: Dict[Tuple[int, int], str]
        :param new_X_cells: The string composed of "X", "O" and "_" with the
        newest previous addition of "X". Example: "_OX_X_XOO".
        :type new_X_cells: str
        :returns: The tuple of 2 strings and a dictionary: (0) the new printable
        matrix with the newly positioned "O" according to the coordinates passed
        in, (1) the new dictionary with the newly positioned "O" according to the
        coordinates passed in, (2) the new string with the newly positioned "O".
        Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.enter_the_coordinates_O()
        print("Making the move level \"human\"")

        while not self.check_if_coordinates_int(coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.enter_the_coordinates_O()

        while not self.check_if_coordinates_in_range(coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.enter_the_coordinates_O()

        while not self.check_if_cell_occupied(
                new_X_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.enter_the_coordinates_O()

        if self.check_if_cell_occupied(new_X_dict, coordinates[0], coordinates[1]):
            new_O_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[1]

            new_O_field: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[0]

            new_O_cells: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[2]

            return new_O_field, new_O_dict, new_O_cells

    def X_field_without_checking(
            self, new_O_dict: Dict[Tuple[int, int], str],
            new_O_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "O" previously
        positioned. Asks for the (pseudorandomly chosen) coordinates for the "X"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks, if both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-im dictionary). If that's the case, it prints the information
        and asks for the other coordinates. Checks for the last time if the chosen
        place is free and if that's the case, it returns the new dictionary, the
        new printable matrix and the new string with the newly positioned "X".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_O_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "O". Example: new_O_dict = {(3,1) : "O", (2,1): "_",
         ...}.
        :type new_O_dict: Dict[Tuple[int, int], str]
        :param new_O_cells: The string composed of "X", "O" and "_" with the
         newest previous addition of "O". Example: "_O__X_XOO".
        :type new_O_cells: str
        :returns: The tuple of objects: (0) the new printable matrix with the
         newly positioned "X" according to the coordinates passed in, (1) the new
         dictionary with the newly positioned "X" according to the coordinates
         passed in, (2) the new string with the newly positioned "X".
         Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.enter_the_coordinates_X()
        print("Making the move level \"easy\"")

        while not self.check_if_coordinates_int(
                coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.enter_the_coordinates_X()

        while not self.check_if_coordinates_in_range(
                coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.enter_the_coordinates_X()

        while not self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.enter_the_coordinates_X()

        if self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            new_X_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[1]

            new_X_field: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[0]

            new_X_cells: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[2]

            return new_X_field, new_X_dict, new_X_cells

    def X_field_without_checking_hard_level(
            self, new_O_dict: Dict[Tuple[int, int], str],
            new_O_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "O" previously
        positioned. Asks for the (pseudorandomly chosen) coordinates for the "X"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks, if both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-im dictionary). If that's the case, it prints the information
        and asks for the other coordinates. Checks for the last time if the chosen
        place is free and if that's the case, it returns the new dictionary, the
        new printable matrix and the new string with the newly positioned "X".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_O_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "O". Example: new_O_dict = {(3,1) : "O", (2,1): "_",
         ...}.
        :type new_O_dict: Dict[Tuple[int, int], str]
        :param new_O_cells: The string composed of "X", "O" and "_" with the
         newest previous addition of "O". Example: "_O__X_XOO".
        :type new_O_cells: str
        :returns: The tuple of objects: (0) the new printable matrix with the
         newly positioned "X" according to the coordinates passed in, (1) the new
         dictionary with the newly positioned "X" according to the coordinates
         passed in, (2) the new string with the newly positioned "X".
         Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.checking_the_game_result_partial_win_put_X_hard_level(new_O_cells)
        print("Making the move level \"hard\"")

        while not self.check_if_coordinates_int(
                coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.checking_the_game_result_partial_win_put_X_hard_level(new_O_cells)

        while not self.check_if_coordinates_in_range(
                coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.checking_the_game_result_partial_win_put_X_hard_level(new_O_cells)

        while not self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.checking_the_game_result_partial_win_put_X_hard_level(new_O_cells)

        if self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            new_X_dict: Dict[Tuple[int, int], str] = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[1]

            new_X_field: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[0]

            new_X_cells: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[2]

            return new_X_field, new_X_dict, new_X_cells

    def enter_the_coordinates_O(self) -> Tuple[int, int]:
        """
        The player is prompted to enter a pair of the coordinates
        in a form of the string of 2 integers separated by a
        whitespace. If the string will be spoiled at the
        beginning or/and at the end of the string with the
        accidental whitespaces and/or other symbols an attempt
        will be made to unclutter the string. The string will
        be splitted into the list of presumed digits at the
        whitespace and the numerical identity of the symbol will
        be evaluated. Those which will positively pass the test
        will be returned as a tuple of 2 integers.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: A tuple of 2 integers processed from the pseudorandomly
        chosen string input. Example: (3, 3).
        rtype: Tuple[int, int]

        """

        try:

            coordinates: str = input("Enter the coordinates: > ")

            coordinates: str = coordinates.strip()
            self.coordinates_already_drawn.append(coordinates)

            coordinates_splited: List[str] = coordinates.split(" ")

            new_coordinates: List[int] = []

            coordinate: str

            for coordinate in coordinates_splited:

                coordinate: str = coordinate.strip()

                if coordinate.isdigit():
                    coordinate_int: int = int(coordinate)
                    new_coordinates.append(coordinate_int)

            return new_coordinates[0], new_coordinates[1]

        except IndexError:

            print("You had IndexError. Try again.")

            coordinates = input("Enter the coordinates: > ")

            coordinates = coordinates.strip()
            self.coordinates_already_drawn.append(coordinates)

            coordinates_splited = coordinates.split(" ")

            new_coordinates = []

            for coordinate in coordinates_splited:

                coordinate = coordinate.strip()

                if coordinate.isdigit():
                    coordinate_int = int(coordinate)
                    new_coordinates.append(coordinate_int)

            return new_coordinates[0], new_coordinates[1]

    def enter_the_coordinates_X(self) -> Tuple[int, int]:
        """
        Chooses pseudorandomly a pair of the coordinates
        in a form of a string of 2 integers separated by a
        whitespace. If the string will be spoiled at the
        beginning or/and at the end of the string with the
        accidental whitespaces and/or other symbols an attempt
        will be made to unclutter the string. The string will
        be splitted into the list of presumed digits at the
        whitespace and the numerical identity of the symbol will
        be evaluated. Those which will positively pass the test
        will be returned as a tuple of 2 integers.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: A tuple of 2 integers processed from the pseudorandomly
        chosen string input. Example: (3, 3).
        rtype: Tuple[int, int]

        """

        coordinates: str = self.choose_the_random_coordinates()
        coordinates: str = coordinates.strip()
        coordinates_splited: List[str] = coordinates.split(" ")

        new_coordinates: List[int] = []

        coordinate: str

        for coordinate in coordinates_splited:

            coordinate = coordinate.strip()

            if coordinate.isdigit():
                coordinate_int: int = int(coordinate)
                new_coordinates.append(coordinate_int)

        return new_coordinates[0], new_coordinates[1]


class TicTacToeHalfRandomXHumanOComputer(TicTacToeRandom):
    """
    The class TicTacToeHalfRandomXHumanOComputer contains the main game
    logic subdivided into the methods. The current game settings
    allow to play the 3x3 classic Tic-Tac-Toe console
    game to human player playing "X" and the computer pseudorandomly
    drawing the coordinates for "O".
    """

    # The empty list for the coordinates, which already had been drawn from
    # the list or chosen by the player.

    def __init__(self):
        self.coordinates_already_drawn: List[str] = []

    def O_field_without_checking_mid_level(
            self, new_X_dict: Dict[Tuple[int, int], str],
            new_X_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "X" previously
        positioned. Asks for the pseudorandomly chosen coordinates for the "O"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks if the both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-in dictionary). If that's the case, prints the information and
        asks for the other coordinates. Checks the last time if the chosen place
        is free and if that's the case, it returns the new dictionary, the new
        printable matrix and the new string with the newly positioned "O".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_X_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "X".
        Example: new_X_dict ={(1, 1): "X", (1, 2): "_", ...}.
        :type new_X_dict: Dict[Tuple[int, int], str]
        :param new_X_cells: The string composed of "X", "O" and "_" with the
        newest previous addition of "X". Example: "_OX_X_XOO".
        :type new_X_cells: str
        :returns: The tuple of 2 strings and a 1 dictionary: (0) the new printable
        matrix with the newly positioned "O" according to the coordinates passed
        in, (1) the new dictionary with the newly positioned "O" according to the
        coordinates passed in, (2) the new string with the newly positioned "O".
        Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.checking_the_game_result_partial_win_put_O_mid_level(new_X_cells)
        print("Making the move level \"mid-hard\"")

        while not self.check_if_coordinates_int(coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.checking_the_game_result_partial_win_put_O_mid_level(new_X_cells)

        while not self.check_if_coordinates_in_range(coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.checking_the_game_result_partial_win_put_O_mid_level(new_X_cells)

        while not self.check_if_cell_occupied(
                new_X_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.checking_the_game_result_partial_win_put_O_mid_level(new_X_cells)

        if self.check_if_cell_occupied(new_X_dict, coordinates[0], coordinates[1]):
            new_O_dict: Dict[Tuple[int, int]] = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[1]

            new_O_field: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[0]

            new_O_cells: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[2]

            return new_O_field, new_O_dict, new_O_cells

    def O_field_without_checking(
            self, new_X_dict: Dict[Tuple[int, int], str],
            new_X_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "X" previously
        positioned. Asks for the pseudorandomly chosen coordinates for the "O"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks if the both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-in dictionary). If that's the case, prints the information and
        asks for the other coordinates. Checks the last time if the chosen place
        is free and if that's the case, it returns the new dictionary, the new
        printable matrix and the new string with the newly positioned "O".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_X_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "X".
        Example: new_X_dict ={(1, 1): "X", (1, 2): "_", ...}.
        :type new_X_dict: Dict[Tuple[int, int], str]
        :param new_X_cells: The string composed of "X", "O" and "_" with the
        newest previous addition of "X". Example: "_OX_X_XOO".
        :type new_X_cells: str
        :returns: The tuple of 2 strings and a 1 dictionary: (0) the new printable
        matrix with the newly positioned "O" according to the coordinates passed
        in, (1) the new dictionary with the newly positioned "O" according to the
        coordinates passed in, (2) the new string with the newly positioned "O".
        Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.enter_the_coordinates_O()
        print("Making the move level \"easy\"")

        while not self.check_if_coordinates_int(coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.enter_the_coordinates_O()

        while not self.check_if_coordinates_in_range(coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.enter_the_coordinates_O()

        while not self.check_if_cell_occupied(
                new_X_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.enter_the_coordinates_O()

        if self.check_if_cell_occupied(new_X_dict, coordinates[0], coordinates[1]):
            new_O_dict: Dict[Tuple[int, int]] = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[1]

            new_O_field: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[0]

            new_O_cells: str = self.print_matrix_cells_new_O(
                new_X_cells, coordinates[0], coordinates[1])[2]

            return new_O_field, new_O_dict, new_O_cells

    def X_field_without_checking(
            self, new_O_dict: Dict[Tuple[int, int], str],
            new_O_cells: str) -> Tuple[str, Dict[Tuple[int, int], str], str]:
        """
        Receives the dictionary and the string with the new "O" previously
        positioned. Asks the player for the new coordinates for the "X"
        placement. Checks if the passed-in data are numbers. If not, it prints the
        information and asks for the coordinates again. Checks, if both
        coordinates ∈ <1, 3>. If not, it prints the information and asks for the
        correct coordinates. Checks if the chosen place is already occupied (uses
        the passed-im dictionary). If that's the case, it prints the information
        and asks for the other coordinates. Checks the last time if the chosen
        place is free and if that's the case, it returns the new dictionary, the
        new printable matrix and the new string with the newly positioned "X".

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param new_O_dict: The dictionary with the keys of the tuples of 2
        integers and the values of the cells of the matrix with the newest
        previous addition of "O". Example: new_O_dict = {(3,1) : "O", (2,1): "_",
         ...}.
        :type new_O_dict: Dict[Tuple[int, int], str]
        :param new_O_cells: The string composed of "X", "O" and "_" with the
         newest previous addition of "O". Example: "_O__X_XOO".
        :type new_O_cells: str
        :returns: The tuple of 2 strings and a dictionary: (0) the new printable
         matrix with the newly positioned "X" according to the coordinates passed
         in, (1) the new dictionary with the newly positioned "X" according to the
         coordinates passed in, (2) the new string with a newly positioned "X".
         Example: ("---------\n| _ X _ |\n| _ O _ |\n| X O X |\n---------",
          {(1, 3): "_", (2, 1): "X", (3, 3): "_", ...},
         "_X__O_XOX").
        :rtype: Tuple[str, Dict[Tuple[int, int], str], str]
        """

        coordinates: Tuple[int, int] = self.enter_the_coordinates_X()
        print("Making the move level \"human\"")

        while not self.check_if_coordinates_int(
                coordinates[0], coordinates[1]):
            print("You should enter numbers!")
            coordinates = self.enter_the_coordinates_X()

        while not self.check_if_coordinates_in_range(
                coordinates[0], coordinates[1]):
            print("Coordinates should be from 1 to 3!")
            coordinates = self.enter_the_coordinates_X()

        while not self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            print("This cell is occupied! Choose another one!")
            coordinates = self.enter_the_coordinates_X()

        if self.check_if_cell_occupied(
                new_O_dict, coordinates[0], coordinates[1]):
            new_X_dict: Dict[Tuple[int, int]] = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[1]

            new_X_field: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[0]

            new_X_cells: str = self.print_matrix_cells_new_X(
                new_O_cells, coordinates[0], coordinates[1])[2]

            return new_X_field, new_X_dict, new_X_cells

    def enter_the_coordinates_X(self) -> Tuple[int, int]:
        """
        The player is prompted to enter a pair of coordinates
        in a form of a string of 2 integers separated by a
        whitespace. If the string will be spoiled at the
        beginning or/and at the end of the string with the
        accidental whitespaces and/or other symbols an attempt
        will be made to unclutter the string. The string will
        be splitted into the list of presumed digits at the
        whitespace and the numerical identity of the symbol will
        be evaluated. Those which will positively pass the test
        will be returned as a tuple of 2 integers.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: A tuple of 2 integers processed from the player's input.
        Example: (3, 3).
        rtype: Tuple[int, int]

        """
        try:

            coordinates: str = input("Enter the coordinates: > ")

            coordinates: str = coordinates.strip()
            self.coordinates_already_drawn.append(coordinates)

            coordinates_splited: List[str] = coordinates.split(" ")

            new_coordinates: List[int] = []

            coordinate: str

            for coordinate in coordinates_splited:

                coordinate: str = coordinate.strip()

                if coordinate.isdigit():
                    coordinate_int: int = int(coordinate)
                    new_coordinates.append(coordinate_int)

            return new_coordinates[0], new_coordinates[1]

        except IndexError:

            print("You had IndexError. Try again.")

            coordinates = input("Enter the coordinates: > ")

            coordinates = coordinates.strip()
            self.coordinates_already_drawn.append(coordinates)

            coordinates_splited = coordinates.split(" ")
            new_coordinates = []

            for coordinate in coordinates_splited:

                coordinate = coordinate.strip()

                if coordinate.isdigit():
                    coordinate_int = int(coordinate)
                    new_coordinates.append(coordinate_int)

            return new_coordinates[0], new_coordinates[1]

    def enter_the_coordinates_O(self) -> Tuple[int, int]:
        """
        Chooses pseudorandomly a pair of coordinates
        in a form of a string of 2 integers separated by a
        whitespace. If the string will be spoiled at the
        beginning or/and at the end of the string with the
        accidental whitespaces and/or other symbols an attempt
        will be made to unclutter the string. The string will
        be splitted into the list of presumed digits at the
        whitespace and the numerical identity of the symbol will
        be evaluated. Those which will positively pass the test
        will be returned as a tuple of 2 integers.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: A tuple of 2 integers processed from the pseudorandomly
        chosen string input. Example: (3, 3).
        rtype: Tuple[int, int]

        """

        coordinates: str = self.choose_the_random_coordinates()
        coordinates: str = coordinates.strip()

        coordinates_splited: List[str] = coordinates.split(" ")
        new_coordinates: List[int] = []

        coordinate: str

        for coordinate in coordinates_splited:

            coordinate: str = coordinate.strip()

            if coordinate.isdigit():
                coordinate_int: int = int(coordinate)
                new_coordinates.append(coordinate_int)

        return new_coordinates[0], new_coordinates[1]


class TicTacToeRandom100(TicTacToeRandom):
    """
    The class TicTacToeRandom100 contains the main game logic
    subdivided into the methods. The current game settings
    allow to play the 3x3 classing Tic-Tac-Toe console
    game automatically for the building and testing purposes
    using the "X" and "O" symbols. The overridden
    choose_the_random_coordinates method allows for playing the game how
    many times you like automatically and
    summarizes the results at the end.
    """

    # coordinates_already_drawn: List[str] = []
    def __init__(self):
        self.coordinates_already_drawn: List[str] = []

    def choose_the_random_coordinates(self) -> str:
        """
        Chooses pseudorandomly a pair of coordinates from the list.
        A certain pair of coordinates may be chosen more than 1
        time. Overrides the parent method.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :returns: A string of coordinates in the format "0 0".
        Example: "2 3".
        :rtype: str
        """

        coordinates_list: List[str] = [
            "1 1", "1 2",
            "1 3", "2 1",
            "2 2", "2 2",
            "2 3", "3 3",
            "3 2", "3 1"
        ]

        random_coordinate: str = \
            coordinates_list[randint(0, len(coordinates_list) - 1)]

        while random_coordinate in self.coordinates_already_drawn:
            random_coordinate = coordinates_list[randint(0, len(coordinates_list) - 1)]

        if random_coordinate not in self.coordinates_already_drawn:
            self.coordinates_already_drawn.append(random_coordinate)

        return random_coordinate


class TicTacToeXEasyOMedium(TicTacToeRandom):
    # coordinates_already_drawn: List[str] = []
    def __init__(self):
        self.coordinates_already_drawn: List[str] = []


class Check:
    """
    Class for checking, if the data entered during the choosing the
    "O" or "X" are at least to some degree correct.
    """

    def check_if_choice_int(self, choice: [int, Any]) -> bool:
        """
        Checks if the choice is an instance of an integer.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param choice: An integer - 0 or 1. Example: 1.
        :type choice: int
        :returns: False if choice is not an instance of an integer.
        Otherwise returns True. Example: True.
        :rtype: bool
        """

        if not isinstance(choice, int):
            return False

        return True

    def check_if_choice_in_range(self, choice: int) -> bool:
        """
        Checks if the choice is 0 or 1.
        Otherwise returns False.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param choice: An integer - 0 or 1. Example: 0.
        :type choice: int
        :returns: False if the choice variable is not 0 or 1.
        Otherwise returns True. Example: False.
        :rtype: bool
        """

        if choice not in [0, 1]:
            return False

        return True


class Check2(Check):
    """
    Class for checking, if the data entered during the choosing the
    "O" or "X" are at least to some degree correct.
    """

    def check_if_choice_in_range(self, choice: int) -> bool:
        """
        Checks if the choice is 0 or 1.
        Otherwise returns False.

        :param self: An instance of the class.
        :type self: An instance of the class, an object.
        :param choice: An integer - 0 or 1. Example: 0.
        :type choice: int
        :returns: False if the choice variable is not 0 or 1.
        Otherwise returns True. Example: False.
        :rtype: bool
        """

        if choice not in [0, 1, 2, 3, 4, 5, 6, 7, 8]:
            return False

        return True
