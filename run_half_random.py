# File run_half_random.py: The file allows to play the classic 3x3
# TicTac-Toe game against the computer (pseudorandomly). The coordinates
# are shown after the choice.

from game_choice_half_random_gameplay import TheGame

if __name__ == '__main__':
    game = TheGame()
