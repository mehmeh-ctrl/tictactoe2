# File run_testing.py: you can choose how many games you want to play with
# automatic build and a starting symbol. The outcomes are summarized at
# the end of the game and saved to a text file.
# Textfiles have descriptive names:
# testing_{starting_symbol}_game_{dimensions_of_the_matrix}_
# {number_of_the_games_played}_{result of calling of the
# time.time() function trimmed to a integer format}.txt

from choice_testing import TheGame

if __name__ == '__main__':
    game = TheGame()
