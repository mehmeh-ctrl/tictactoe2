# File game_choice_half_random_gameplay.py

from tictactoe import Check


class TheGame:
    """
    The game logic of choice between starting with "X" or starting with "O"
    and being the first or the second player (playing with the computer,
    pseudorandomly - so-called "easy player").
    """

    # Let's instantiate a Check class.

    check: Check = Check()

    # The player is prompted to choose with which symbol they want to play.
    # Choose 0 to play with "X" first, choose 1 to play with "O" first.

    try:

        choice_1: int = int(input("Do you want to play with \"X\" (choose 0) or with "
                                  "\"O\" (choose 1)? "))

    except ValueError:

        print("You had ValueError. Try again.")
        choice_1: int = int(input("Do you want to play with \"X\" (choose 0) or with "
                                  "\"O\" (choose 1)? "))

    # The player is required to choose an integer. Otherwise the player is
    # informed that they hadn't chosen an instance of an integer and they are
    # prompted to choose once again.

    while not Check.check_if_choice_int(check, choice_1):
        print("Choose an integer !!!")
        choice_1: int = int(input("Do you want to play with \"X\" (choose 0) or with "
                                  "\"O\" (choose 1)? "))

    # If the player didn't choose an integer which is 0 or 1 they are informed
    # of it and prompted to choose once again.

    while not Check.check_if_choice_in_range(check, choice_1):
        print("Choose 0 or 1 !!!")
        choice_1: int = int(input("Do you want to play with \"X\" (choose 0) or with "
                                  "\"O\" (choose 1)? "))

    # The player is prompted to choose if they want to play first or the
    # second against the computer.

    try:

        choice_2: int = int(input("Do you want to play first (choose 0) or "
                                  " the second (choose 1)? "))

    except ValueError:

        print("You had ValueError. Try again.")
        choice_2 = int(input("Do you want to play first (choose 0) or "
                             " the second (choose 1)? "))

    # The player is required to choose an integer. Otherwise the player is
    # informed that they hadn't chosen an instance of an integer and they are
    # prompted to choose once again.

    while not Check.check_if_choice_int(check, choice_2):
        print("Choose an integer !!!")
        choice_2 = int(input("Do you want to play first (choose 0) or"
                             " the second (choose 1)? "))

    # If the player didn't chose an integer which is 0 or 1 they are informed
    # of it and prompted to choose once again.

    while not Check.check_if_choice_in_range(check, choice_2):
        print("Choose 0 or 1 !!! ")
        choice_2 = int(input("Do you want to play first (choose 0) or"
                             " the second (choose 1)? "))

    # If 0 is chosen the game with the logic with starting "X" is imported,
    # instantiated, and played.

    if choice_1 == 0:

        # If 0 is chosen at choice_2 the game with the logic with the human playing
        # first is imported, instantiated, and played.

        # playing X and playing first

        if choice_2 == 0:

            from half_random_gameplayXhumanXfirst import GameX
            game = GameX()
            game.play()

        # If 1 is chosen at choice_2 the game with the logic with the human playing as
        # the second player is imported, instantiated, and played.

        # playing with X and second

        elif choice_2 == 1:

            from half_random_gameplayXhumanOfirst import GameO
            game = GameO()
            game.play()

    # If 1 is chosen the game with the logic with starting "O" is imported,
    # instantiated, and played.

    elif choice_1 == 1:

        # If 0 is chosen the game with the logic with the human playing first is
        # imported, instantiated, and played.

        # with O and first

        if choice_2 == 0:

            from half_random_gameplayOhumanOfirst import GameO
            game = GameO()
            game.play()

        # If 1 is chosen the game with the logic with the human playing as the
        # second player is imported, instantiated, and played.

        # with O and second

        elif choice_2 == 1:

            from half_random_gameplayOhumanXfirst import GameX
            game = GameX()
            game.play()
