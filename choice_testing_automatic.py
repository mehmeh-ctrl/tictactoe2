# File choice_testing_automatic.py: The gameplay version for the automatic
# testing and building purposes. The number of games to play in the automatic
# build mode are chosen pseudorandomly from a range of integers,
# where integer ∈ <1 000, 5 000 001>. The symbol to play with first is
# also chosen pseudorandomly. Filenames are constructed according to the
# following scheme:
# f"testing_{starting_symbol}_game_3D_{number_of_games_played}
# _{current_time}.txt"

from random import randint
import time
from typing import List, TextIO

from tictactoe import Check, Check2


class TheGame:

    check: Check = Check()
    check2: Check2 = Check2()

    try:
        number_of_the_games: int = randint(1000, 5000001)

    except ValueError:

        number_of_the_games  = randint(1000, 5000001)

    while not Check.check_if_choice_int(check, number_of_the_games):
        print("Choose an integer !!!")
        number_of_the_games = randint(1000, 5000001)

    try:
        choice: int = randint(0, 1)

    except ValueError:
        print("You had ValueError. Try again.")
        choice: int = randint(0, 1)

    while not Check.check_if_choice_int(check, choice):
        print("Choose an integer !!!")
        choice: int = randint(0, 1)
    while not Check.check_if_choice_in_range(check, choice):

        print("Choose 0 or 1 !!! ")
        choice: int = randint(0, 1)


    try:

        choice_O: int = randint(0, 8)

    except ValueError:


        print("You had ValueError. Try again.")
        choice_O = randint(0, 8)


    while not Check2.check_if_choice_int(check2, choice_O):
        print("Choose an integer !!!")
        choice_O = randint(0, 8)


    while not Check2.check_if_choice_in_range(check2, choice_O):
        print("Choose from 0 to 8  ")
        choice_O = randint(0, 8)


    if choice == 0 and choice_O == 0:

        from game_X_testing import GameXRandom
        game = GameXRandom()

        i: int

        for i in range(0, number_of_the_games):
            game.play()

        print("---------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played easy games with X first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won {game.X_wins} times. That is {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won {game.O_wins} times. That is {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played {number_of_games_played} games."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_X_easy_game_3D_{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    if choice == 0 and choice_O == 2:

        from gameXeasyOmidXfirst_testing import GameXRandom
        game = GameXRandom()

        i: int

        for i in range(0, number_of_the_games):
            game.play()

        print("---------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played easy-medium games with X first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X (easy player) won {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O (medium player) won {game.O_wins} times." \
                            f" That is {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played {number_of_games_played} games."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_X_easy_medium_game_3D_{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    if choice == 0 and choice_O == 5:

        from gameXhardOeasyXfirst_testing import GameXHard
        game = GameXHard()

        i: int

        for i in range(0, number_of_the_games):
            game.play()

        print("---------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played hard-easy games with X first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X (hard player) won {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O (easy player) won {game.O_wins} times." \
                            f" That is {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played {number_of_games_played} games."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_X_hard_easy_game_3D_{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 1 and choice_O == 0:

        from game_O_testing import GameORandom
        game = GameORandom()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played easy games with O first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won {game.X_wins} times. That is {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won {game.O_wins} times. That is {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played {number_of_games_played} easy (pseudorandom) games."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_O_easy_games_3D_{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 0 and choice_O == 1:

        from game_X_MidMid import GameXMid
        game = GameXMid()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played medium-hard games with X first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won {game.X_wins} times. That is {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won {game.O_wins} times. That is {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played {number_of_games_played}" \
                                             f" medium-hard games (both agents used the same" \
                                             f" strategy)."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_X_medium_games_3D_{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 1 and choice_O == 1:

        from game_O_MidMid import GameOMid
        game = GameOMid()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played medium-hard games with O first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won {game.X_wins} times. That is {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won {game.O_wins} times. That is {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played {number_of_games_played}" \
                                             f" medium-hard games (both agents used the same" \
                                             f" strategy."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_O_medium_games_3D_{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 1 and choice_O == 4:

        from gameOeasyXhard_Ofirst_testing import GameORandom
        game = GameORandom()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played easy-hard games with O first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (hard player) {game.X_wins} times." \
                            f" That is {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (easy player) {game.O_wins} times." \
                            f" That is {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played {number_of_games_played} easy-medium-hard games ."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_O_easy_hard_games_3D_{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 1 and choice_O == 3:

        from gameXeasyOmid_Ofirst_testing import GameORandom
        game = GameORandom()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played easy-medium games with O first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (easy player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (medium player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} easy-medium games ."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_O_medium_easy_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)



    elif choice == 1 and choice_O == 2:

        from game_O_easy_X_mid_O_first import GameORandom
        game = GameORandom()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played easy-medium games with O first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (easy player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (medium player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} easy-medium games ."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_O_medium_easy_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 0 and choice_O == 3:
        # First X, medium first

        from game_X_mid_O_easy_X_first import GameXMid
        game = GameXMid()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played medium-easy games with X first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (medium player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (easy player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} medium-easy games ."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_X_medium_easy_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 0 and choice_O == 4:
        # First X, easy first 0, 4

        from game_X_easy_O_hard_X_first import GameOHardXEasy
        game = GameOHardXEasy()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played easy-hard games with X playing first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (easy player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} easy-hard games ."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_X_easy_hard_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 1 and choice_O == 5:
        # First O, hard first

        from game_O_hard_X_easy_O_first import GameORandom
        game = GameORandom()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played hard-easy games with O playing first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (easy player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} hard-easy games ."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_O_hard_easy_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 0 and choice_O == 6:
        # First X, medium first

        from game_X_mid_O_hard_X_first import GameXMidOHard
        game = GameXMidOHard()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played medium-hard games with X playing first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (medium player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} medium-hard games ."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_X_medium_hard_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 1 and choice_O == 6:
        # First O, medium first

        from game_Orandom_testing_O_mid_X_hard import GameORandom
        game = GameORandom()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played medium-hard games with O playing first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (hard player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (medium player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} medium-hard games ."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_O_medium_hard_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 0 and choice_O == 7:
        # First X, hard first

        from game_Xrandom_testing_Omid_Xhard import GameXRandom
        game = GameXRandom()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played hard-medium games with X playing first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (hard player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (medium player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} hard-medium games ."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_X_hard_medium_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 1 and choice_O == 7:
        # First O, hard first

        from game_O_hard_X_mid_O_first import GameOHardXMid
        game = GameOHardXMid()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played hard-medium games with O playing first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (medium player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} hard-medium games ."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_O_hard_medium_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 0 and choice_O == 8:
        # X

        from game_X_HardHard import GameXHard
        game = GameXHard()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played hard games with X playing first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (hard player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} hard games (both" \
                                             f" agents used the same strategy)."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_X_hard_hard_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)


    elif choice == 1 and choice_O == 8:
        # X

        from game_O_HardHard import GameOHard
        game = GameOHard()
        for i in range(number_of_the_games):
            game.play()

        print("-------------------------------------------")
        print(f"Choice: {choice}")
        print(f"Choice 2: {choice_O}")
        symbol_string: str = "You have played hard games with O playing first."
        print(symbol_string)
        number_of_games_played: int = game.X_wins + game.O_wins + game.draws
        percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
        percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
        percent_of_draws: float = (game.draws / number_of_the_games) * 100
        X_won_string: str = f"X won (hard player) {game.X_wins} times. That is" \
                            f" {round(percent_of_X_won, 3)} % of the time."
        print(X_won_string)
        O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                            f" {round(percent_of_O_won, 3)} % of the time."
        print(O_won_string)
        draws_string: str = f"There were {game.draws} draws." \
                            f" That is {round(percent_of_draws, 3)} % of the time."
        print(draws_string)
        number_of_games_played_string: str = f"You have played" \
                                             f" {number_of_games_played} hard games (both" \
                                             f" agents used the same strategy)."
        print(number_of_games_played_string)
        current_time: int = int(time.time())
        file_name: str = f"testing_O_hard_hard_games_3D_" \
                         f"{number_of_games_played}_{current_time}.txt"
        string_list: List[str] = [symbol_string, "\n",
                                  X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                  number_of_games_played_string, "\n"]

        f: TextIO

        with open(file_name, "a+") as f:
            f.writelines(string_list)

