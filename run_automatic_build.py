# File run_automatic_build.py: The classic 3x3 gameplay of TicTac-Toe.
# The automatic build of the game with the pseudorandom choice of 0 -
# the game starts with "X" or 1 - the game starts with "O".

from game_XO_choice_automatic import TheRandomGameWithChoice

if __name__ == '__main__':
    game = TheRandomGameWithChoice()
