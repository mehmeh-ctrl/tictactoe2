# File gameXO_choice_automatic.py

from random import randint

from tictactoe import Check


class TheRandomGameWithChoice:
    """
    The game logic of the choice between starting with "X" or starting with
    "O". Chosen pseudorandomly for the automatic building purposes.
    """

    # Let's instantiate a Check class.

    check: Check = Check()

    # The 0 or 1 is pseudorandomly assigned to a choice variable.
    # 0 - the game setting are set to play "X" first, 1 - to play "O" first.

    choice: int = randint(0, 1)

    while not Check.check_if_choice_int(check, choice):
        print("Choose an integer !!!")
        choice = randint(0, 1)

    # If the player didn't chose the integer which is 0 or 1 they are informed
    # of it and prompted to choose once again. In that case the replacement
    # with the pseudorandom choice of 2 integers is provided.

    while not Check.check_if_choice_in_range(check, choice):
        print("Choose 0 or 1 !!! ")
        choice = randint(0, 1)

    # If 0 is chosen the game with the logic with starting "X" is imported and
    # instantiated, and played.

    if choice == 0:

        print("Choice:", choice)

        from gameX_automatic import GameXRandom
        game = GameXRandom()
        game.play()

    # If 1 is chosen the game with the logic with starting "O" is imported and
    # instantiated, and played.

    elif choice == 1:

        print("Choice:", choice)

        from gameO_automatic import GameORandom
        game = GameORandom()
        game.play()
