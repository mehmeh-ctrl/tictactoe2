# File gameXO_choice.py

from tictactoe import Check


class TheGame:
    """
    The game logic of choice between starting with "X" or starting with "O".
    """

    # Let's instantiate a Check class.

    check: Check = Check()

    # The player is prompted to choose with which symbol they want to play.
    # Choose "0" to play with "X" first, choose "1" to play with "O" first.

    try:

        choice: int = int(input("Do you want to play with \"X\" first (choose 0) or "
                                "with \"O\" first (choose 1) ? >"))

    except ValueError:

        print("You had ValueError. Try again.")
        choice = int(input("Do you want to play with \"X\" first (choose 0) or "
                           "with \"O\" first (choose 1) ? >"))

    # The player is required to choose an integer. Otherwise the player is
    # informed that they hadn't chosen an instance of an integer and they are
    # prompted to choose another time.

    while not Check.check_if_choice_int(check, choice):
        print("Choose an integer !!!")
        choice = int(input("Do you want to play with \"X\" first (choose 0) or "
                           "with \"O\" first (choose 1) ? >"))

    # If the player didn't chose an integer which is 0 or 1 they are informed
    # of it and prompted to choose once again.

    while not Check.check_if_choice_in_range(check, choice):
        print("Choose 0 or 1 !!! ")
        choice = int(input("Do you want to play with \"X\" first (choose 0) or "
                           "with \"O\" first (choose 1) ? >"))

    # If 0 is chosen the game with the logic with starting "X" is imported and
    # instantiated, and played.

    if choice == 0:

        from gameX import GameX
        game = GameX()
        game.play()

    # If 1 is chosen the game with the logic with starting "O" is imported and
    # instantiated, and played.

    elif choice == 1:

        from gameO import GameO
        game = GameO()
        game.play()
