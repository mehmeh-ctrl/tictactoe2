# gameX_easy_Omid_XFirst.py

from typing import Dict, Tuple

from tictactoe import TicTacToeRandom


class GameXRandomMidLevel:
    """
    The main game logic. With X playing first.
    The classical 3 x 3 gameplay played automatically for the
    automatic building and testing purposes.
    """

    # The phrase variable initialized to the empty string.

    phrase: str = ''

    def play(self) -> None:
        """
        The "empty" string (the 9-element-long string composed of "_") is
        instantiated as the new_O_cells. The "empty" dictionary (the dictionary
        of the coordinates pairs keys and values of "_") is instantiated as the
        new_O_dict - to set it apart from the "X" that will be placed first and
        plug it into the loop. The loop uses the counter variable to direct the
        player activity to the relevant blocks of the game's method. There, as
        that version of the game was created for the automatic building purposes,
        the player activity (typing in the coordinates) is simulated by the
        pseudorandom drawing of the coordinates from the list. The even counter
        values direct the player activity towards typing in the new "X" (the
        method X_field_without_checking(...)), the uneven counter values - towards
        typing in the new "O" (the method O_field_without_checking(...)). The
        values of the counter greater or equal to 4 are direct the activity
        towards the block with the checking of the game results. The string is
        then plugged into the checking_the_game_result method. The game results
        may be 4 - 3 of them result in the termination of the game and the
        printout of the game results - "X wins", "O wins" (if the player playing
        "X" or "O" - there simulated - has won, respectively) and "Draw" - when
        there is no win. When the string result resolves to the "Game not
        finished", the game continues.

        :param self: An instance of the class.
        :type self: An instance of the class.
        :returns: None. Example: None.
        :rtype: None
        """
        tic_tac_toe = TicTacToeRandom()

        # The helper coordinates printed:

        print("Coordinates of the cells:")
        print(tic_tac_toe.cell_coordinates())

        # The empty field printed:

        print(tic_tac_toe.empty_field()[0])

        # The counter variable set to 0.

        counter: int = 0

        new_X_dict: Dict[Tuple[int, int], str] = dict()
        new_X_cells: str = ''

        new_O_cells: str = tic_tac_toe.empty_field()[1]
        new_O_dict: Dict[Tuple[int, int], str] = tic_tac_toe.print_matrix_cells(new_O_cells)[1]

        while counter < 8:

            while counter < 4:

                if counter % 2 == 1:

                    new_O_field: str

                    new_O_field, new_O_dict, new_O_cells = \
                        TicTacToeRandom.O_field_without_checking_mid_level(
                            tic_tac_toe,
                            new_X_dict, new_X_cells)

                    print(new_O_field)

                    counter += 1

                elif counter % 2 == 0:

                    new_X_field: str

                    new_X_field, new_X_dict, new_X_cells = \
                        TicTacToeRandom.X_field_without_checking(
                            tic_tac_toe,
                            new_O_dict, new_O_cells)

                    print(new_X_field)

                    counter += 1

            while 4 <= counter < 9:

                if counter % 2 == 1:

                    new_O_field, new_O_dict, new_O_cells = \
                        TicTacToeRandom.O_field_without_checking_mid_level(
                            tic_tac_toe,
                            new_X_dict, new_X_cells)

                    print(new_O_field)

                    phrase = TicTacToeRandom.checking_the_game_result(
                        tic_tac_toe, new_O_cells)

                    if phrase == "X wins":

                        print("X wins")
                        print("Game over.")
                        return

                    elif phrase == "O wins":

                        print("O wins")
                        print("Game over.")
                        return

                    elif phrase == "Draw":

                        print("Draw")
                        print("Game over.")
                        return

                    elif phrase == "Game not finished":

                        pass

                    counter += 1

                elif counter % 2 == 0:

                    new_X_field, new_X_dict, new_X_cells = \
                        TicTacToeRandom.X_field_without_checking(
                            tic_tac_toe,
                            new_O_dict, new_O_cells)

                    print(new_X_field)

                    phrase = TicTacToeRandom.checking_the_game_result(
                        tic_tac_toe, new_X_cells)

                    if phrase == "X wins":

                        print("X wins")
                        print("Game over.")
                        return

                    elif phrase == "O wins":

                        print("O wins")
                        print("Game over.")
                        return

                    elif phrase == "Draw":

                        print("Draw")
                        print("Game over.")
                        return

                    elif phrase == "Game not finished":

                        pass

                    counter += 1
