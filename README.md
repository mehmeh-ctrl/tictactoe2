run.py - play the classic console 3 x 3 Tic-Tac-Toe
game with the choice of the starting symbol.

run_automatic_build.py - run the automatic
version of the previous gameplay with the pseudorandom
choice of the starting symbol and the pseudorandom choice of
the coordinates.

run_half_random.py - play against the computer. Pseudorandomly.

run_automatic_gameplay_4_choices.py - the simulation of the aforementioned
game for the testing purposes. 

run_testing.py - choose the number of the automatically building
games and a starting symbol. The summary of wins / draws is printed at the
end of the game and saved in the file. Textfiles have descriptive names:
testing_{starting_symbol}_game_{dimensions_of_the_matrix}_{number_of_the_games_played}_{result of calling of the
time.time() function trimmed to a integer format}.txt

run_testing_automatic.py - the automatic version of the
aforementioned game with the pseudorandom drawing of the number of 
the games played - from 1 000 to 5 000 001. For the automatic
building and testing purposes. The summary of wins / draws is printed at the
end of the game and saved in the file. Textfiles have descriptive names:
testing_{starting_symbol}_game_{dimensions_of_the_matrix}_{number_of_the_games_played}_{result of calling of the
time.time() function trimmed to a integer format}.txt.

run_X_game.py - player chooses between playing as a (and with a) human, "easy" (pseudorandom),
medium-hard agent or hard agent with starting X or O. Input command accepts four arguments
at the start level: start ([user/human]/easy/[medium/mid]/hard) ([user/human]/easy/[medium/mid]/hard) (starting_symbol). 
To exit the game type "exit"
