# File game_choice_half_random_gameplay_automatic.py The automatic
# gameplay to simulate the choice between "X" and "O" and to simulate the
# choice between playing first or the second for the automatic
# building and testing purposes.

from random import randint

from tictactoe import Check


class TheGame:
    """
    The game logic of choice between starting with "X" or starting with "O"
    and being the first or the second player (playing with the computer).
    """

    check: Check = Check()

    # The player is prompted to choose with which symbol they want to play.
    # Choose "0" to play with "X" first, choose "1" to play with "O" first.
    # There, this behaviour is simulated by the pseudorandom drawing of 0 or
    # 1.

    choice_1: int = randint(0, 1)

    # The player is required to choose an integer. Otherwise the player is
    # informed that they hadn't chosen an instance of an integer and they are
    # prompted to choose another time.

    while not Check.check_if_choice_int(check, choice_1):
        print("Choose an integer !!!")
        choice_1 = randint(0, 1)

    # If the player didn't choose an integer which is 0 or 1 they are informed
    # of it and prompted to choose once again.

    while not Check.check_if_choice_in_range(check, choice_1):
        print("Choose 0 or 1 !!! ")
        choice_1 = randint(0, 1)

    # The player is prompted to choose if they want to play first or the
    # second against the computer. There, this behaviour is simulated by the
    # pseudorandom drawing of 0 or 1.

    choice_2: int = randint(0, 1)

    # The player is required to choose an integer. Otherwise the player is
    # informed that they hadn't chosen an instance of an integer and they are
    # prompted to choose another time.

    while not Check.check_if_choice_int(check, choice_2):
        print("Choose an integer !!!")
        choice_2 = randint(0, 1)

    # If the player didn't choose an integer which is 0 or 1 they are informed
    # of it and prompted to choose once again.

    while not Check.check_if_choice_in_range(check, choice_2):
        print("Choose 0 or 1 !!! ")
        choice_2 = randint(0, 1)

    # If 0 is chosen the game with the logic with starting "X" is imported and
    # instantiated, and played. There this behaviour is simulated.

    if choice_1 == 0:

        # If 0 is chosen the game with the logic with the human playing first is
        # imported, instantiated, and played. There this behaviour is simulated.

        # with playing "X" first

        if choice_2 == 0:

            print(f"Choice 1: {choice_1}")
            print(f"Choice 2: {choice_2}")

            from gameX_automatic import GameXRandom
            game = GameXRandom()
            game.play()

        # If 1 is chosen the game with the logic with the human playing as the
        # second player is imported, instantiated, and played. There this
        # behaviour is simulated.

        # with "O" playing first

        elif choice_2 == 1:

            print(f"Choice 1: {choice_1}")
            print(f"Choice 2: {choice_2}")

            from gameO_automatic import GameORandom
            game = GameORandom()
            game.play()

    # If 1 is chosen the game with the logic with starting "O" is imported,
    # instantiated, and played. There this behaviour is simulated.

    elif choice_1 == 1:

        # If 0 is chosen the game with the logic with the human playing first is
        # imported, instantiated, and played. There this behaviour is simulated.

        # with "O" playing first

        if choice_2 == 0:

            print(f"Choice 1: {choice_1}")
            print(f"Choice 2: {choice_2}")

            from gameO_automatic import GameORandom
            game = GameORandom()
            game.play()

        # If 1 is chosen the game with the logic with the human playing as the
        # second player is imported, instantiated, and played. There, this
        # behaviour is simulated.

        # with "X" playing first

        elif choice_2 == 1:

            print(f"Choice 1: {choice_1}")
            print(f"Choice 2: {choice_2}")

            from gameX_automatic import GameXRandom
            game = GameXRandom()
            game.play()
