# File tests.py

import unittest
from typing import List, Tuple, Union, Dict, Any

from to_tests import (enter_the_coordinates,
                      empty_field,
                      checking_the_game_result,
                      O_fields_without_checking_cells,
                      O_fields_without_checking_dict,
                      X_field_without_checking_cells,
                      X_field_without_checking_dict,
                      print_matrix_cells,
                      print_matrix_cells_new_X_string,
                      print_matrix_cells_new_X_dict,
                      print_matrix_cells_new_O_string,
                      print_matrix_cells_new_O_dict,
                      check_if_coordinates_int,
                      check_if_coordinates_in_range,
                      check_if_cell_occupied,
                      check_if_choice_int,
                      check_if_choice_in_range,
                      checking_the_game_result_partial_win_put_O,
                      checking_the_game_result_partial_win_put_X)


class MyTest(unittest.TestCase):

    def test_empty(self):
        """
        Tests if the "empty string" is "empty" (composed of "_" only).
        """

        empty_string: str = 9 * '_'

        self.assertEqual(empty_field(), empty_string)

    def test_X_win(self):
        """
        Tests if the given alphanumeric combinations give the result of "X wins".
        """

        win_X_cells_list: List[str] = [
            "X__XOOX__",
            "X__XO_XO_",
            "__X_XOX_O",
            "XXXOX_OO_",
            "OOXXOXOXX"
        ]

        win_X: str

        for win_X in win_X_cells_list:
            self.assertEqual(checking_the_game_result(win_X),
                             "X wins")

    def test_O_win(self):
        """
        Tests if the given alphanumeric combinations give the result of "O wins".
        """

        win_O_cells_list: List[str] = [
            "OOO_X_XX_", "_XX_X_OOO",
            "OXXXO_OXO", "XX_OXXOOO"
        ]

        win_O: str

        for win_O in win_O_cells_list:
            self.assertEqual(checking_the_game_result(win_O),
                             "O wins")

    def test_draw(self):
        """
        Tests if the given alphanumeric combinations give the result of "Draw".
        """

        draws: List[str] = [
            "OXXXXOOOX", "OOXXXOOXX",
            "OXXXOOXOX", "XXOOXXXOO"
        ]

        draw: str

        for draw in draws:
            self.assertEqual(checking_the_game_result(draw),
                             "Draw")

    def test_enter_the_coordinates(self):
        """
        Tests if the given i-th string combination of digits ∈ <1, 3>
        separated by a whitespace in the list give the corresponding i-th tuple of
        2 integers in the second list after being subjected to the action of the
        tested function (enter_the_coordinates). It also checks if the function
        strips the strings from the whitespaces correctly."
        """

        coordinates_string_test: List[str] = [
            '1 1', '2 2',
            ' 2 3', '3 3',
            '2 3', ' 2 1 '
        ]

        coordinates_return: List[Tuple[int, int]] = [
            (1, 1), (2, 2),
            (2, 3), (3, 3),
            (2, 3), (2, 1)
        ]

        i: int

        for i in range(len(coordinates_string_test)):
            self.assertEqual(enter_the_coordinates(coordinates_string_test[i]),
                             coordinates_return[i])

    def test_O_fields_without_checking(self):
        """
        Tests if the i-th string of the X_string_list, after being plugged into
        the O_fields_without_checking_cells and O_fields_without_checking_dict
        functions along with the i-th pair of coordinates from the
        new_O_coordinates list will return the corresponding i-th new_O_string and
        dictionary, respectively.
        """

        X_strings_list: List[str] = [
            "___X_____",
            "_____X___",
            "_X_______",
            "_OXX_____",
            "__O__X__X",
            "_XO_X____",
            "_OXX__OX_",
            "__O_XXO_X",
            "XXO_X__O_",
            "_OXXOXOX_",
            "XXO_X_XOO"
        ]

        new_O_strings: List[str] = [
            "_O_X_____",
            "__O__X___",
            "_XO______",
            "_OXX__O__",
            "__O__XO_X",
            "_XO_X__O_",
            "_OXXO_OX_",
            "__O_XXOOX",
            "XXO_X__OO",
            "_OXXOXOXO",
            "XXOOX_XOO"
        ]

        new_O_coordinates: List[Tuple[int, int]] = [
            (2, 3),
            (3, 3),
            (3, 3),
            (1, 1),
            (1, 1),
            (2, 1),
            (2, 2),
            (2, 1),
            (3, 1),
            (3, 1),
            (1, 2)
        ]

        i: int

        for i in range(len(X_strings_list)):
            new_dict_X: Dict[Tuple[int, int], str] = dict()
            new_dict_0: Dict[Tuple[int, int], str] = dict()

            new_dict_X[(1, 3)] = X_strings_list[i][0]
            new_dict_X[(2, 3)] = X_strings_list[i][1]
            new_dict_X[(3, 3)] = X_strings_list[i][2]
            new_dict_X[(1, 2)] = X_strings_list[i][3]
            new_dict_X[(2, 2)] = X_strings_list[i][4]
            new_dict_X[(3, 2)] = X_strings_list[i][5]
            new_dict_X[(1, 1)] = X_strings_list[i][6]
            new_dict_X[(2, 1)] = X_strings_list[i][7]
            new_dict_X[(3, 1)] = X_strings_list[i][8]

            new_dict_0[(1, 3)] = new_O_strings[i][0]
            new_dict_0[(2, 3)] = new_O_strings[i][1]
            new_dict_0[(3, 3)] = new_O_strings[i][2]
            new_dict_0[(1, 2)] = new_O_strings[i][3]
            new_dict_0[(2, 2)] = new_O_strings[i][4]
            new_dict_0[(3, 2)] = new_O_strings[i][5]
            new_dict_0[(1, 1)] = new_O_strings[i][6]
            new_dict_0[(2, 1)] = new_O_strings[i][7]
            new_dict_0[(3, 1)] = new_O_strings[i][8]

            self.assertEqual(O_fields_without_checking_cells(
                new_O_coordinates[i],
                new_dict_X, X_strings_list[i]),
                new_O_strings[i])

            self.assertEqual(O_fields_without_checking_dict(
                new_O_coordinates[i],
                new_dict_X, X_strings_list[i]),
                new_dict_0)

    def test_X_fields_without_checking(self):
        """
        Tests if i-th string of the O_string_list, after being plugged into the
        X_fields_without_checking_cells and the X_fields_without_checking_dict
        functions, along with the i-th pair of coordinates from the
        new_X_coordinates list will return the corresponding i-th new_X_string and
        dictionary, respectively.
        """

        O_strings_list: List[str] = [
            "_O_X_____",
            "__O__X___",
            "_XO______",
            "_OXX__O__",
            "__O__XO_X",
            "_XO_X__O_",
            "_OXXO_OX_",
            "__O_XXOOX",
            "XXO_X__OO",
            "_OXXOXOXO",
            "XXOOX_XOO"
        ]

        new_X_strings: List[str] = [
            "_OXX_____",
            "__O__X__X",
            "_XO_X____",
            "_OXX__OX_",
            "__O_XXO_X",
            "XXO_X__O_",
            "_OXXOXOX_",
            "__OXXXOOX",
            "XXO_X_XOO",
            "XOXXOXOXO",
            "XXOOXXXOO"
        ]

        new_X_coordinates: List[Tuple[int, int]] = [
            (3, 3),
            (3, 1),
            (2, 2),
            (2, 1),
            (2, 2),
            (1, 3),
            (3, 2),
            (1, 2),
            (1, 1),
            (1, 3),
            (3, 2)
        ]

        i: int

        for i in range(len(O_strings_list)):
            new_dict_0: Dict[Tuple[int, int], str] = dict()
            new_dict_X: Dict[Tuple[int, int], str] = dict()

            new_dict_0[(1, 3)] = O_strings_list[i][0]
            new_dict_0[(2, 3)] = O_strings_list[i][1]
            new_dict_0[(3, 3)] = O_strings_list[i][2]
            new_dict_0[(1, 2)] = O_strings_list[i][3]
            new_dict_0[(2, 2)] = O_strings_list[i][4]
            new_dict_0[(3, 2)] = O_strings_list[i][5]
            new_dict_0[(1, 1)] = O_strings_list[i][6]
            new_dict_0[(2, 1)] = O_strings_list[i][7]
            new_dict_0[(3, 1)] = O_strings_list[i][8]

            new_dict_X[(1, 3)] = new_X_strings[i][0]
            new_dict_X[(2, 3)] = new_X_strings[i][1]
            new_dict_X[(3, 3)] = new_X_strings[i][2]
            new_dict_X[(1, 2)] = new_X_strings[i][3]
            new_dict_X[(2, 2)] = new_X_strings[i][4]
            new_dict_X[(3, 2)] = new_X_strings[i][5]
            new_dict_X[(1, 1)] = new_X_strings[i][6]
            new_dict_X[(2, 1)] = new_X_strings[i][7]
            new_dict_X[(3, 1)] = new_X_strings[i][8]

            self.assertEqual(
                X_field_without_checking_cells(
                    new_X_coordinates[i], new_dict_0, O_strings_list[i]),
                new_X_strings[i])

            self.assertEqual(
                X_field_without_checking_dict(
                    new_X_coordinates[i], new_dict_0, O_strings_list[i]),
                new_dict_X)

    def test_print_matrix_cells(self):
        """
        Checks the correctness of printing the dictionary by the function
        print_matrix_cells().
        """

        new_X_strings: List[str] = [
            "_OXX_____",
            "__O__X__X",
            "_XO_X____",
            "_OXX__OX_",
            "__O_XXO_X",
            "XXO_X__O_",
            "_OXXOXOX_",
            "__OXXXOOX",
            "XXO_X_XOO",
            "XOXXOXOXO",
            "XXOOXXXOO"
        ]

        X_string: str

        for X_string in new_X_strings:
            new_dict: Dict[Tuple[int, int], str] = dict()

            new_dict[(1, 3)] = X_string[0]
            new_dict[(2, 3)] = X_string[1]
            new_dict[(3, 3)] = X_string[2]
            new_dict[(1, 2)] = X_string[3]
            new_dict[(2, 2)] = X_string[4]
            new_dict[(3, 2)] = X_string[5]
            new_dict[(1, 1)] = X_string[6]
            new_dict[(2, 1)] = X_string[7]
            new_dict[(3, 1)] = X_string[8]

            self.assertEqual(print_matrix_cells(X_string), new_dict)

    def test_print_matrix_cells_new_X_string(self):
        """
        Checks the correctness of inserting the "X" from the raw coordinates
        ("0 0" format) and checks the resulting string.
        """

        O_strings_list: List[str] = [
            "_O_X_____",
            "__O__X___",
            "_XO______",
            "_OXX__O__",
            "__O__XO_X",
            "_XO_X__O_",
            "_OXXO_OX_",
            "__O_XXOOX",
            "XXO_X__OO",
            "_OXXOXOXO",
            "XXOOX_XOO"
        ]

        new_X_strings: List[str] = [
            "_OXX_____",
            "__O__X__X",
            "_XO_X____",
            "_OXX__OX_",
            "__O_XXO_X",
            "XXO_X__O_",
            "_OXXOXOX_",
            "__OXXXOOX",
            "XXO_X_XOO",
            "XOXXOXOXO",
            "XXOOXXXOO"
        ]

        new_X_coordinates: List[str] = [
            "3 3",
            "3 1",
            "2 2",
            "2 1",
            "2 2",
            "1 3",
            "3 2",
            "1 2",
            "1 1",
            "1 3",
            "3 2"
        ]

        i: int

        for i in range(len(O_strings_list)):
            self.assertEqual(print_matrix_cells_new_X_string(O_strings_list[i],
                                                             *enter_the_coordinates(new_X_coordinates[i])),
                             new_X_strings[i])

    def test_print_matrix_cells_new_X_dict(self):
        """
        Checks the correctness of inserting the "X" from the raw coordinates
        ("0 0" format) and checks the resulting dictionary.
        """

        O_strings_list: List[str] = [
            "_O_X_____",
            "__O__X___",
            "_XO______",
            "_OXX__O__",
            "__O__XO_X",
            "_XO_X__O_",
            "_OXXO_OX_",
            "__O_XXOOX",
            "XXO_X__OO",
            "_OXXOXOXO",
            "XXOOX_XOO"
        ]

        new_X_strings: List[str] = [
            "_OXX_____",
            "__O__X__X",
            "_XO_X____",
            "_OXX__OX_",
            "__O_XXO_X",
            "XXO_X__O_",
            "_OXXOXOX_",
            "__OXXXOOX",
            "XXO_X_XOO",
            "XOXXOXOXO",
            "XXOOXXXOO"
        ]

        new_X_coordinates: List[str] = [
            "3 3",
            "3 1",
            "2 2",
            "2 1",
            "2 2",
            "1 3",
            "3 2",
            "1 2",
            "1 1",
            "1 3",
            "3 2"
        ]

        i: int

        for i in range(len(O_strings_list)):
            new_coordinates: Tuple[int, int] = enter_the_coordinates(new_X_coordinates[i])

            new_dict: Dict[Tuple[int, int], str] = dict()

            new_dict[(1, 3)] = new_X_strings[i][0]
            new_dict[(2, 3)] = new_X_strings[i][1]
            new_dict[(3, 3)] = new_X_strings[i][2]
            new_dict[(1, 2)] = new_X_strings[i][3]
            new_dict[(2, 2)] = new_X_strings[i][4]
            new_dict[(3, 2)] = new_X_strings[i][5]
            new_dict[(1, 1)] = new_X_strings[i][6]
            new_dict[(2, 1)] = new_X_strings[i][7]
            new_dict[(3, 1)] = new_X_strings[i][8]

            self.assertEqual(print_matrix_cells_new_X_dict(
                O_strings_list[i],
                *new_coordinates),
                new_dict)

    def test_print_matrix_cells_new_O_string(self):
        """
        Checks the correctness of inserting the "O" from the raw coordinates
        ("0 0" format), and checks the resulting string.
        """

        X_strings_list: List[str] = [
            "___X_____",
            "_____X___",
            "_X_______",
            "_OXX_____",
            "__O__X__X",
            "_XO_X____",
            "_OXX__OX_",
            "__O_XXO_X",
            "XXO_X__O_",
            "_OXXOXOX_",
            "XXO_X_XOO"
        ]

        new_O_strings: List[str] = [
            "_O_X_____",
            "__O__X___",
            "_XO______",
            "_OXX__O__",
            "__O__XO_X",
            "_XO_X__O_",
            "_OXXO_OX_",
            "__O_XXOOX",
            "XXO_X__OO",
            "_OXXOXOXO",
            "XXOOX_XOO"
        ]

        new_O_coordinates: List[str] = [
            "2 3",
            "3 3",
            "3 3",
            "1 1",
            "1 1",
            "2 1",
            "2 2",
            "2 1",
            "3 1",
            "3 1",
            "1 2"
        ]

        i: int

        for i in range(len(X_strings_list)):
            self.assertEqual(
                print_matrix_cells_new_O_string(
                    X_strings_list[i], *enter_the_coordinates(new_O_coordinates[i])),
                new_O_strings[i])

    def test_print_matrix_cells_new_O_dict(self):
        """
        Checks the correctness of inserting the "O" from the raw coordinates
        ("0 0" format) and checks the resulting dictionary.
        """

        X_strings_list: List[str] = [
            "___X_____",
            "_____X___",
            "_X_______",
            "_OXX_____",
            "__O__X__X",
            "_XO_X____",
            "_OXX__OX_",
            "__O_XXO_X",
            "XXO_X__O_",
            "_OXXOXOX_",
            "XXO_X_XOO"
        ]

        new_O_strings: List[str] = [
            "_O_X_____",
            "__O__X___",
            "_XO______",
            "_OXX__O__",
            "__O__XO_X",
            "_XO_X__O_",
            "_OXXO_OX_",
            "__O_XXOOX",
            "XXO_X__OO",
            "_OXXOXOXO",
            "XXOOX_XOO"
        ]

        new_O_coordinates: List[str] = [
            "2 3",
            "3 3",
            "3 3",
            "1 1",
            "1 1",
            "2 1",
            "2 2",
            "2 1",
            "3 1",
            "3 1",
            "1 2"
        ]

        i: int

        for i in range(len(X_strings_list)):
            new_coordinates: Tuple[int, int] = enter_the_coordinates(new_O_coordinates[i])

            new_dict: Dict[Tuple[int, int], str] = dict()

            new_dict[(1, 3)] = new_O_strings[i][0]
            new_dict[(2, 3)] = new_O_strings[i][1]
            new_dict[(3, 3)] = new_O_strings[i][2]
            new_dict[(1, 2)] = new_O_strings[i][3]
            new_dict[(2, 2)] = new_O_strings[i][4]
            new_dict[(3, 2)] = new_O_strings[i][5]
            new_dict[(1, 1)] = new_O_strings[i][6]
            new_dict[(2, 1)] = new_O_strings[i][7]
            new_dict[(3, 1)] = new_O_strings[i][8]

            self.assertEqual(
                print_matrix_cells_new_O_dict(
                    X_strings_list[i], *new_coordinates),
                new_dict)

    def test_check_if_coordinates_int_true(self):
        """
        Asserts that's the function (check_if_coordinates_int(..)) -
        checking if the symbol passed in is an instance of an integer
        - returns True on receiving pairs of digits in the strings.
        """

        new_O_coordinates: List[str] = [
            "2 3",
            "3 3",
            "3 3",
            "1 1",
            "1 1",
            "2 1",
            "2 2",
            "2 1",
            "3 1",
            "3 1",
            "1 2"
        ]

        coordinate: str

        for coordinate in new_O_coordinates:
            self.assertTrue(
                check_if_coordinates_int(
                    *enter_the_coordinates(coordinate)))

    def test_check_if_coordinates_int_false(self):
        """
        Asserts that's the function (check_if_coordinates_int(...))
        - checking if the symbol passed in is an instance of an integer
        - returns False on receiving an instance of the non-integer or the
        non-digit in a pair.
        """

        new_O_coordinates: List[Union[Tuple[float, int], Tuple[str, str],
                                      Tuple[int, float], Tuple[float, float]]] = [
            (2.0, 3),
            ("two", "3"),
            ('3', "two"),
            ('three', 'three'),
            (1, 1.0),
            (2.3, 1.0),
            ("half", "big")
        ]

        coordinate: Union[Tuple[float, int], Tuple[str, str],
                          Tuple[int, float], Tuple[float, float]]

        for coordinate in new_O_coordinates:
            self.assertFalse(check_if_coordinates_int(*coordinate))

    def test_check_if_coordinates_in_range_true(self):
        """
        Checks, if the function returns True on receiving pairs of integers
        ∈ <1, 3> in a string format.
        """

        new_O_coordinates: List[str] = [
            "2 3",
            "3 3",
            "3 3",
            "1 1",
            "1 1",
            "2 1",
            "2 2",
            "2 1",
            "3 1",
            "3 1",
            "1 2"
        ]

        coordinate: str

        for coordinate in new_O_coordinates:
            self.assertTrue(
                check_if_coordinates_in_range(*enter_the_coordinates(coordinate)))

    def test_check_if_coordinates_in_range_false(self):
        """
        Checks, if the function returns False if the one or two of the passed-in
        coordinates ∉ <1, 3>.
        """

        new_O_coordinates: List[str] = [
            "4 3",
            "10 3",
            "3 0",
            "21 1",
            "4 1",
            "5 1",
            "2 6",
            "2 7",
            "3 9",
            "4 5",
            "5 2"
        ]

        coordinate: str

        for coordinate in new_O_coordinates:
            self.assertFalse(
                check_if_coordinates_in_range(*enter_the_coordinates(coordinate)))

    def test_check_if_cell_occupied_assert_false(self):
        """
        Checks for the range of strings and for the range of coordinates assigned
        to the i-th string, if function returns False if asked if
        the new_dict[(first_coordinate, second_coordinate)] != "_", so it checks
        if the function correctly checks if cell is occupied.
        """

        strings: List[str] = [
            '______X__', '____O_X__', '__X_O_X__',
            '__X_O_XO_', '__X_OXXO_', 'O_X_OXXO_',
            'OXX_OXXO_', 'OXXOOXXO_', 'OXXOOXXOX'
        ]

        coordinates: List[List[Tuple[int, int]]] = [
            [(1, 1)],
            [(1, 1), (2, 2)],
            [(1, 1), (2, 2), (3, 3)],
            [(1, 1), (2, 2), (3, 3), (2, 1)],
            [(1, 1), (2, 2), (3, 3), (2, 1), (3, 2)],
            [(1, 1), (2, 2), (3, 3), (2, 1), (3, 2), (1, 3)],
            [(1, 1), (2, 2), (3, 3), (2, 1), (3, 2), (1, 3), (2, 3)],
            [(1, 1), (2, 2), (3, 3), (2, 1), (3, 2), (1, 3), (2, 3), (1, 2)],
            [(1, 1), (2, 2), (3, 3), (2, 1), (3, 2), (1, 3), (2, 3), (1, 2), (3, 1)]
        ]

        i: int

        for i in range(len(strings)):

            new_dict: Dict[Tuple[int, int], str] = dict()

            new_dict[(1, 3)] = strings[i][0]
            new_dict[(2, 3)] = strings[i][1]
            new_dict[(3, 3)] = strings[i][2]
            new_dict[(1, 2)] = strings[i][3]
            new_dict[(2, 2)] = strings[i][4]
            new_dict[(3, 2)] = strings[i][5]
            new_dict[(1, 1)] = strings[i][6]
            new_dict[(2, 1)] = strings[i][7]
            new_dict[(3, 1)] = strings[i][8]

            coordinate: Tuple[int, int]

            for coordinate in coordinates[i]:
                self.assertFalse(check_if_cell_occupied(new_dict, *coordinate))

    def test_check_if_cell_occupied_assert_true(self):
        """
        Checks for the range of strings and for the range of coordinates assigned
        to the i-th string, if the function returns True if asked if
        the new_dict[(first_coordinate, second_coordinate)] != '_', so it checks
        if the function correctly checks if cell is not occupied.
        """

        strings: List[str] = [
            '______X__', '____O_X__',
            '__X_O_X__', '__X_O_XO_',
            '__X_OXXO_', 'O_X_OXXO_',
            'OXX_OXXO_', 'OXXOOXXO_'
        ]

        coordinates: List[List[Tuple[int, int]]] = [
            [(1, 2), (1, 3), (2, 1), (2, 2), (2, 3), (3, 1), (3, 2), (3, 3)],
            [(1, 2), (1, 3), (2, 1), (2, 3), (3, 1), (3, 2), (3, 3)],
            [(1, 2), (1, 3), (2, 1), (2, 3), (3, 1), (3, 2)],
            [(1, 2), (1, 3), (2, 3), (3, 1), (3, 2)],
            [(1, 2), (1, 3), (2, 3), (3, 1)],
            [(1, 2), (2, 3), (3, 1)],
            [(1, 2), (3, 1)],
            [(3, 1)]
        ]

        i: int

        for i in range(len(strings)):

            new_dict: Dict[Tuple[int, int], str] = dict()

            new_dict[(1, 3)] = strings[i][0]
            new_dict[(2, 3)] = strings[i][1]
            new_dict[(3, 3)] = strings[i][2]
            new_dict[(1, 2)] = strings[i][3]
            new_dict[(2, 2)] = strings[i][4]
            new_dict[(3, 2)] = strings[i][5]
            new_dict[(1, 1)] = strings[i][6]
            new_dict[(2, 1)] = strings[i][7]
            new_dict[(3, 1)] = strings[i][8]

            coordinate: Tuple[int, int]

            for coordinate in coordinates[i]:
                self.assertTrue(check_if_cell_occupied(new_dict, *coordinate))

    def test_check_if_choice_int_assert_true(self):
        """
        Asserts that the tested function on receiving the range of
        integers returns True.
        """

        int_list: List[int] = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]

        num: int

        for num in int_list:
            self.assertTrue(check_if_choice_int(num))

    def test_check_if_choice_int_assert_false(self):

        """
        Asserts that the tested function on receiving the range of
        non-integers returns False.
        """

        rubbish_list: List[Union[float, str]] = [1.2, 'f', 13.3, 1.0]

        presumed_not_int: Union[float, str]

        for presumed_not_int in rubbish_list:
            self.assertFalse(check_if_choice_int(presumed_not_int))

    def test_check_if_choice_in_range_assert_true(self):
        """
        Asserts, that the tested function on receiving 0 and 1
        returns True.
        """

        num_list: List[int] = [0, 1, 2, 3, 4, 5, 6, 7, 8]

        num: int

        for num in num_list:
            self.assertTrue(check_if_choice_in_range(num))

    def test_check_if_choice_in_range_assert_false(self):
        """
        Asserts, that the tested function on receiving the range of integers
        that are not 0 or 1 returns False.
        """

        num_list: List[int] = [ -1, 9, 10, -2, 12]

        num: int

        for num in num_list:
            self.assertFalse(check_if_choice_in_range(num))

    def test_checking_the_game_result_partial_win_put_O(self):

        put_O_input_list: List[str] = ["__XXOOXXO", "__X__OXXO", "_____OXX_", "OOX_XO__X",
                            "OOX_____X", "OX__OXXOX", "_XX_OXO__", "X_OXOX__O",
                            "__OX_X__O", "__O_XXX_O", "____X_X_O"]

        put_O_output_list: List[Tuple[int, int]] = [(1, 3), (2, 2), (3, 1), (1, 1), (3, 2), (3, 3),
                             (1, 3), (1, 1), (2, 2), (1, 2), (3, 3)]

        _: int
        field: str

        for _, field in enumerate(put_O_input_list):
            self.assertEqual(checking_the_game_result_partial_win_put_O(field), put_O_output_list[_])

    def test__checking_the_game_result_partial_win_put_X(self):

        put_X_input_list: List[str] = ["__X_OOXXO", "_____OXXO", "X_O__X_OO", "X_O_____O",
                            "OOX_XOO_X", "OO______X", "OX__O_X__", "XOX_OOOX_",
                            "_OX_O____", "OXX_OXO__", "_X__OXO__", "__OXOX__O",
                            "__OX____O", "__O_X_X_O"]

        put_X_output_list: List[Tuple[int, int]] = [(1, 2), (3, 3), (1, 1), (3, 2), (1, 2),
                             (3, 3), (3, 1), (1, 2), (2, 1), (3, 1),
                             (3, 3), (1, 3), (3, 2), (3, 2)]

        _: int
        field: str

        for _, field in enumerate(put_X_input_list):
            self.assertEqual(checking_the_game_result_partial_win_put_X(field), put_X_output_list[_])


if __name__ == '__main__':
    unittest.main()
