# File choice_testing.py: The choice logic of the "testing mode" gameplay
# where you can choose the number of the classic 3x3 tic-tac-toe games which
# you want to play in the automatic build mode and a starting symbol. The
# summaries for a range of games played are printed at the end and written to
# a file.

import time
from typing import List, TextIO

from tictactoe import Check, Check2


class TheGame:
    """
    The game logic of choice between starting with "X" or starting with "O".
    The number of games played in the automatic building mode can be chosen
    by the player.
    """

    # Let's instantiate a Check class.

    flag: str = "y"

    check: Check = Check()
    check2: Check2 = Check2()



    while flag != "n":

        try:

            # The player is asked for the number of games they want to
            # play.

            number_of_the_games: int = int(input("How many games do you want to play? > "))

        except ValueError:

            # If the player had a ValueError during processing of the number of
            # games they want to play, they are informed of it and asked for
            # the number of games once again.

            print("You had ValueError. Try again.")
            number_of_the_games = int(input("How many games do you want to play? > "))

        # The player is required to choose an integer. Otherwise the player is
        # informed that they hadn't chosen an instance of an integer and they are
        # prompted to choose once again.

        while not Check.check_if_choice_int(check, number_of_the_games):
            print("Choose an integer !!!")
            number_of_the_games = int(input("How many games do you want to play? > "))

        # The player is prompted to choose with which symbol they want to play.
        # Choose "0" to play with "X" first, choose "1" to play with "O" first.

        try:

            choice: int = int(input("Do you want to play with \"X\" first (choose 0) or "
                                    "with \"O\" (choose 1)? "))

        except ValueError:

            # If the player had a ValueError during choosing of
            # the symbol they want to play, they are informed of
            # it and asked for the number of games once again.

            print("You had ValueError. Try again.")
            choice: int = int(input("Do you want to play with \"X\" first (choose 0) or "
                                    "with \"O\" (choose 1)? "))

        # The player is required to choose an integer. Otherwise the player is
        # informed that they hadn't chosen an instance of an integer and they are
        # prompted to choose once again.

        while not Check.check_if_choice_int(check, choice):
            print("Choose an integer !!!")
            choice: int = int(input("Do you want to play with \"X\" first (choose 0) or "
                                    "with \"O\" (choose 1)? "))

        # If the player didn't chose an integer which is 0 or 1 they are informed
        # of it and prompted to choose once again.

        while not Check.check_if_choice_in_range(check, choice):
            print("Choose 0 or 1 !!! ")
            choice: int = int(input("Do you want to play with \"X\" first (choose 0) or "
                                    "with \"O\" (choose 1)? "))

        # If 0 is chosen the game with the logic with starting "X" is imported,
        # instantiated, and played the preselected number of times. The outcomes
        # of playing the number of games the player had chosen are summarized at
        # the end and written to a file.+++++

        try:

            choice_O: int = int(input("Do you want to play easy games (choose 0) or \n"
                                      "the medium-level hard games (choose 1) or to \n"
                                      "set an easy agent against a medium-level agent (choose 2) \n"
                                      " or the medium against an easy agent (choose 3) \n"
                                      " or set easy agent against hard agent (choose 4) \n"
                                      " or set hard agent against easy agent (choose 5) \n"
                                      "or set medium agent against hard agent (choose 6) \n"
                                      "or set hard agent against medium gent (choose 7) \n"
                                      "or set hard agent against hard agent (choose 8) ? \n"
                                      "> "))

        except ValueError:

            # If the player had a ValueError during choosing of
            # the symbol they want to play, they are informed of
            # it and asked for the number of games once again.

            print("You had ValueError. Try again.")
            choice_O: int = int(input("Do you want to play easy games (choose 0) or \n"
                                      "the medium-level hard games (choose 1) or to \n"
                                      "set an easy agent against a medium-level agent (choose 2) \n"
                                      " or the medium against an easy agent (choose 3) \n"
                                      " or set easy agent against hard agent (choose 4) \n"
                                      " or set hard agent against easy agent (choose 5) \n"
                                      "or set medium agent against hard agent (choose 6) \n"
                                      "or set hard agent against medium gent (choose 7) \n"
                                      "or set hard agent against hard agent (choose 8) ? \n"
                                      "> "))

        # The player is required to choose an integer. Otherwise the player is
        # informed that they hadn't chosen an instance of an integer and they are
        # prompted to choose once again.

        while not Check2.check_if_choice_int(check2, choice_O):
            print("Choose an integer !!!")
            choice_O: int = int(input("Do you want to play easy games (choose 0) or \n"
                                      "the medium-level hard games (choose 1) or to \n"
                                      "set an easy agent against a medium-level agent (choose 2) \n"
                                      " or the medium against an easy agent (choose 3) \n"
                                      " or set easy agent against hard agent (choose 4) \n"
                                      " or set hard agent against easy agent (choose 5) \n"
                                      "or set medium agent against hard agent (choose 6) \n"
                                      "or set hard agent against medium gent (choose 7) \n"
                                      "or set hard agent against hard agent (choose 8) ? \n"
                                      "> "))

        # If the player didn't chose an integer which is 0 or 1 they are informed
        # of it and prompted to choose once again.

        while not Check2.check_if_choice_in_range(check2, choice_O):
            print("Choose 0, 1, 2 or 3 !!! ")
            choice_O: int = int(input("Do you want to play easy games (choose 0) or \n"
                                      "the medium-level hard games (choose 1) or to \n"
                                      "set an easy agent against a medium-level agent (choose 2) \n"
                                      " or the medium against an easy agent (choose 3) \n"
                                      " or set easy agent against hard agent (choose 4) \n"
                                      " or set hard agent against easy agent (choose 5) \n"
                                      "or set medium agent against hard agent (choose 6) \n"
                                      "or set hard agent against medium gent (choose 7) \n"
                                      "or set hard agent against hard agent (choose 8) ? \n"
                                      "> "))

        # If 0 is chosen the game with the logic with starting "X" is imported,
        # instantiated, and played the preselected number of times. The outcomes
        # of playing the number of games the player had chosen are summarized at
        # the end.

        if choice == 0 and choice_O == 0:

            from game_X_testing import GameXRandom
            game = GameXRandom()

            i: int

            for i in range(0, number_of_the_games):
                game.play()

            print("---------------------------------------")
            symbol_string: str = "You have played easy games with X first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won {game.X_wins} times. That is {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won {game.O_wins} times. That is {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played {number_of_games_played} games."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_X_easy_game_3D_{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        # If 1 is chosen the game with the logic with starting "O" is imported,
        # instantiated, and played the preselected number of times. The outcomes
        # of playing the number of games the player had chosen are summarized at
        # the end.

        if choice == 0 and choice_O == 2:

            from gameXeasyOmidXfirst_testing import GameXRandom
            game = GameXRandom()

            i: int

            for i in range(0, number_of_the_games):
                game.play()

            print("---------------------------------------")
            symbol_string: str = "You have played easy-medium games with X first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X (easy player) won {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O (medium player) won {game.O_wins} times." \
                                f" That is {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played {number_of_games_played} games."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_X_easy_medium_game_3D_{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        # If 1 is chosen the game with the logic with starting "O" is imported,
        # instantiated, and played the preselected number of times. The outcomes
        # of playing the number of games the player had chosen are summarized at
        # the end.

        if choice == 0 and choice_O == 5:

            from gameXhardOeasyXfirst_testing import GameXHard
            game = GameXHard()

            i: int

            for i in range(0, number_of_the_games):
                game.play()

            print("---------------------------------------")
            symbol_string: str = "You have played hard-easy games with X first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X (hard player) won {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O (easy player) won {game.O_wins} times." \
                                f" That is {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played {number_of_games_played} games."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_X_hard_easy_game_3D_{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 1 and choice_O == 0:

            from game_O_testing import GameORandom
            game = GameORandom()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played easy games with O first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won {game.X_wins} times. That is {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won {game.O_wins} times. That is {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played {number_of_games_played} easy (pseudorandom) games."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_O_easy_games_3D_{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 0 and choice_O == 1:

            from game_X_MidMid import GameXMid
            game = GameXMid()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played medium-hard games with X first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won {game.X_wins} times. That is {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won {game.O_wins} times. That is {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played {number_of_games_played}" \
                                                 f" medium-hard games (both agents used the same" \
                                                 f" strategy)."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_X_medium_games_3D_{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 1 and choice_O == 1:

            from game_O_MidMid import GameOMid
            game = GameOMid()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played medium-hard games with O first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won {game.X_wins} times. That is {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won {game.O_wins} times. That is {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played {number_of_games_played}" \
                                                 f" medium-hard games (both agents used the same" \
                                                 f" strategy."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_O_medium_games_3D_{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 1 and choice_O == 4:

            from gameOeasyXhard_Ofirst_testing import GameORandom
            game = GameORandom()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played easy-hard games with O first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (hard player) {game.X_wins} times." \
                                f" That is {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (easy player) {game.O_wins} times." \
                                f" That is {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws. That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played {number_of_games_played} easy-medium-hard games ."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_O_easy_hard_games_3D_{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 1 and choice_O == 3:

            from gameXeasyOmid_Ofirst_testing import GameORandom
            game = GameORandom()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played easy-medium games with O first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (easy player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (medium player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} easy-medium games ."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_O_medium_easy_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")


        elif choice == 1 and choice_O == 2:

            from game_O_easy_X_mid_O_first import GameORandom
            game = GameORandom()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played easy-medium games with O first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (easy player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (medium player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} easy-medium games ."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_O_medium_easy_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 0 and choice_O == 3:
            # First X, medium first

            from game_X_mid_O_easy_X_first import GameXMid
            game = GameXMid()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played medium-easy games with X first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (medium player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (easy player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} medium-easy games ."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_X_medium_easy_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 0 and choice_O == 4:
            # First X, easy first 0, 4

            from game_X_easy_O_hard_X_first import GameOHardXEasy
            game = GameOHardXEasy()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played easy-hard games with X playing first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (easy player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} easy-hard games ."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_X_easy_hard_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 1 and choice_O == 5:
            # First O, hard first

            from game_O_hard_X_easy_O_first import GameORandom
            game = GameORandom()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played hard-easy games with O playing first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (easy player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} hard-easy games ."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_O_hard_easy_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 0 and choice_O == 6:
            # First X, medium first

            from game_X_mid_O_hard_X_first import GameXMidOHard
            game = GameXMidOHard()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played medium-hard games with X playing first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (medium player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} medium-hard games ."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_X_medium_hard_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 1 and choice_O == 6:
            # First O, medium first

            from game_Orandom_testing_O_mid_X_hard import GameORandom
            game = GameORandom()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played medium-hard games with O playing first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (hard player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (medium player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} medium-hard games ."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_O_medium_hard_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 0 and choice_O == 7:
            # First X, hard first

            from game_Xrandom_testing_Omid_Xhard import GameXRandom
            game = GameXRandom()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played hard-medium games with X playing first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (hard player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (medium player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} hard-medium games ."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_X_hard_medium_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 1 and choice_O == 7:
            # First O, hard first

            from game_O_hard_X_mid_O_first import GameOHardXMid
            game = GameOHardXMid()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played hard-medium games with O playing first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (medium player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} hard-medium games ."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_O_hard_medium_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")


        elif choice == 0 and choice_O == 8:
            # X

            from game_X_HardHard import GameXHard
            game = GameXHard()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played hard games with X playing first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (hard player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} hard games (both" \
                                                 f" agents used the same strategy)."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_X_hard_hard_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")

        elif choice == 1 and choice_O == 8:
            # X

            from game_O_HardHard import GameOHard
            game = GameOHard()
            for i in range(number_of_the_games):
                game.play()

            print("-------------------------------------------")
            symbol_string: str = "You have played hard games with O playing first."
            print(symbol_string)
            number_of_games_played: int = game.X_wins + game.O_wins + game.draws
            percent_of_X_won: float = (game.X_wins / number_of_the_games) * 100
            percent_of_O_won: float = (game.O_wins / number_of_the_games) * 100
            percent_of_draws: float = (game.draws / number_of_the_games) * 100
            X_won_string: str = f"X won (hard player) {game.X_wins} times. That is" \
                                f" {round(percent_of_X_won, 3)} % of the time."
            print(X_won_string)
            O_won_string: str = f"O won (hard player) {game.O_wins} times. That is" \
                                f" {round(percent_of_O_won, 3)} % of the time."
            print(O_won_string)
            draws_string: str = f"There were {game.draws} draws." \
                                f" That is {round(percent_of_draws, 3)} % of the time."
            print(draws_string)
            number_of_games_played_string: str = f"You have played" \
                                                 f" {number_of_games_played} hard games (both" \
                                                 f" agents used the same strategy)."
            print(number_of_games_played_string)
            current_time: int = int(time.time())
            file_name: str = f"testing_O_hard_hard_games_3D_" \
                             f"{number_of_games_played}_{current_time}.txt"
            string_list: List[str] = [symbol_string, "\n",
                                      X_won_string, "\n", O_won_string, "\n", draws_string, "\n",
                                      number_of_games_played_string, "\n"]

            f: TextIO

            with open(file_name, "a+") as f:
                f.writelines(string_list)

            flag = input("Do you want to play again? [y]/n > ")
